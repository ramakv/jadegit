#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("ImplicitInverseRef.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class klass(&schema, nullptr, "TestClass");

	{
		ImplicitInverseRef ref(&klass, nullptr, "testRef");

		// Check name has been set as expected
		CHECK("testRef" == ref.GetName());
		CHECK("testRef" == ref.GetValue("name").Get<string>());

		// Check it's been added to collection
		CHECK(&ref == klass.properties.Get("testRef"));
	}

	CHECK(nullptr == klass.properties.Get("testRef"));
}

TEST_CASE("ImplicitInverseRef.TypeInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class klass(&schema, nullptr, "TestClass");

	ImplicitInverseRef ref(&klass, nullptr, "testRef");

	{
		Class refType1(&schema, nullptr, "RefType1");
		ref.type = &refType1;

		// Setting type should add property to types property refs
		CHECK(refType1.propertyRefs.Includes(&ref));

		Class refType2(&schema, nullptr, "RefType2");
		ref.type = &refType2;

		// Changing type should remove from previous type property refs
		CHECK(!refType1.propertyRefs.Includes(&ref));
		CHECK(refType2.propertyRefs.Includes(&ref));
	}

	// Deletion of current type should reset property type to null
	CHECK(nullptr == static_cast<Type*>(ref.type));
}