#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Attribute.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityFactory.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ApplicationMeta.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("IntegerArray.Instantiation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	// Setup test schema
	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	// Setup test class
	Class klass(&schema, nullptr, "TestClass");

	// Add primitive array property
	CompAttribute integerArray(&klass, nullptr, "integerArray");
	integerArray.type = EntityFactory::Get().Resolve<Type>(&schema, QualifiedName("IntegerArray"));
	
	// Create instance of the test class
	Object testObject(nullptr, &klass);

	// Check collection is instantiated as expected
	Any value = testObject.GetValue(&integerArray, true);
	REQUIRE(value.Get<Collection*>());
	REQUIRE(value.Get<Array<int>*>());
}

TEST_CASE("StringArray.Instantiation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	// Setup test schema
	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	// Setup test class
	Class klass(&schema, nullptr, "TestClass");

	// Add primitive array property
	CompAttribute stringArray(&klass, nullptr, "stringArray");
	stringArray.type = EntityFactory::Get().Resolve<Type>(&schema, QualifiedName("StringArray"));

	// Create instance of the test class
	Object testObject(nullptr, &klass);

	// Check collection is instantiated
	Any value = testObject.GetValue(&stringArray, true);
	REQUIRE(value.Get<Collection*>());
	REQUIRE(value.Get<Array<string>*>());
}

TEST_CASE("ExclusiveCollection.ModifiesOwner", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	// Setup test schema
	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();
	schema.Modified();

	SECTION("Static")
	{
		SECTION("Explicit")
		{
			// Setup test interface
			JadeInterface interface(&schema, nullptr, "TestInterface");
			interface.Modified();

			// Perform initial save
			assembly.save();

			// Setup test class
			Class klass(&schema, nullptr, "TestClass");

			// Clearing empty collection shouldn't modify
			klass.implementedInterfaces.Clear();

			// Check parent object is not in a modified state
			REQUIRE(!klass.isModified());

			SECTION("Add")
			{
				// Add to exclusive collection
				klass.implementedInterfaces.Add(&interface);
			}
			SECTION("Update")
			{
				// Perform initial add/save
				klass.implementedInterfaces.Add(&interface);
				klass.Modified();
				assembly.save();

				// Check parent object is not in a modified state
				REQUIRE(!klass.isModified());

				SECTION("Clear")
				{
					// Clear exclusive collection
					klass.implementedInterfaces.Clear();
				}
				SECTION("Remove")
				{
					// Remove from exclusive collection
					klass.implementedInterfaces.Remove(&interface);
				}
			}

			// Check parent object is now in a modified state
			REQUIRE(klass.isModified());
		}
		SECTION("Implicit")
		{
			// Setup test class
			Class klass(&schema, nullptr, "TestClass");
			klass.Modified();

			// Perform initial save
			assembly.save();

			// Setup test application
			Application app(&schema, nullptr, "TestApplication");
			
			// Get collection
			// NOTE: We don't currently have an any implicit references bound to proxy class
			Collection* coll = app.GetValue(schema.GetRootSchema().application->threeDControls, true).Get<Collection*>();

			// Clearing empty collection shouldn't modify
			coll->Clear();

			// Check parent object is not in a modified state
			REQUIRE(!klass.isModified());

			SECTION("Add")
			{
				// Add to collection
				coll->Add(Value(&klass));
			}
			SECTION("Update")
			{
				// Perform initial add/save
				coll->Add(Value(&klass));
				app.Modified();
				assembly.save();

				// Check parent object is not in a modified state
				REQUIRE(!app.isModified());

				SECTION("Clear")
				{
					// Clear collection
					coll->Clear();
				}
				SECTION("Remove")
				{
					// Remove from collection
					coll->Remove(Value(&klass));
				}
			}

			// Check parent object is now in a modified state
			REQUIRE(app.isModified());
		}
		SECTION("Primitive")
		{
			// Perform initial save
			assembly.save();

			// Setup test local format
			DateFormat format(&schema, nullptr, "TestFormat");

			// Clearing empty collection shouldn't modify
			format.shortDayNames.Clear();

			// Check parent object is not in a modified state
			REQUIRE(!format.isModified());

			SECTION("Add")
			{
				// Add to primitive array
				format.shortDayNames.Add("Mon");
			}
			SECTION("Update")
			{
				// Perform initial add/save
				format.shortDayNames.Add("Mon");
				format.Modified();
				assembly.save();

				// Check parent object is not in a modified state
				REQUIRE(!format.isModified());

				SECTION("Clear")
				{
					// Clear primitive array
					format.shortDayNames.Clear();
				}
				SECTION("Remove")
				{
					// Remove from primitive array
					format.shortDayNames.Remove("Mon");
				}
			}

			// Check parent object is now in a modified state
			REQUIRE(format.isModified());
		}
	}
	SECTION("Dynamic")
	{
		// Setup test class
		Class klass(&schema, nullptr, "TestObject");
		klass.superclass = EntityFactory::Get().Resolve<Class>(&schema, QualifiedName("Object"));
		klass.Modified();

		SECTION("Explicit")
		{
			// Define reference
			ExplicitInverseRef prop(&klass, nullptr, "prop");
			prop.inverseNotRequired = true;
			prop.type = EntityFactory::Get().Resolve<Type>(&schema, QualifiedName("JadeInterfaceColl"));
			prop.Modified();

			// Setup test interface
			JadeInterface interface(&schema, nullptr, "TestInterface");
			interface.Modified();

			// Perform initial save
			assembly.save();

			// Setup test instance (using schema as the parent object)
			Object testObject(&schema, &klass);

			// Get collection
			Collection* coll = testObject.GetValue(&prop, true).Get<Collection*>();

			// Clearing empty collection shouldn't modify
			coll->Clear();

			// Check parent object is not in a modified state
			REQUIRE(!testObject.isModified());

			SECTION("Add")
			{
				// Add to collection
				coll->Add(Value(&interface));
			}
			SECTION("Update")
			{
				// Perform initial add/save
				coll->Add(Value(&interface));
				testObject.Modified();
				assembly.save();

				// Check parent object is not in a modified state
				REQUIRE(!testObject.isModified());

				SECTION("Clear")
				{
					// Clear collection
					coll->Clear();
				}
				SECTION("Remove")
				{
					// Remove from collection
					coll->Remove(Value(&interface));
				}
			}

			// Check parent object is now in a modified state
			REQUIRE(testObject.isModified());
		}
		SECTION("Implicit")
		{
			// Define reference
			ImplicitInverseRef prop(&klass, nullptr, "prop");
			prop.type = EntityFactory::Get().Resolve<Type>(&schema, QualifiedName("JadeInterfaceColl"));
			prop.Modified();

			// Setup test interface
			JadeInterface interface(&schema, nullptr, "TestInterface");
			interface.Modified();

			// Perform initial save
			assembly.save();

			// Setup test instance (using schema as the parent object)
			Object testObject(&schema, &klass);

			// Get collection
			Collection* coll = testObject.GetValue(&prop, true).Get<Collection*>();

			// Clearing empty collection shouldn't modify
			coll->Clear();

			// Check parent object is not in a modified state
			REQUIRE(!testObject.isModified());

			SECTION("Add")
			{
				// Add to collection
				coll->Add(Value(&interface));
			}
			SECTION("Update")
			{
				// Perform initial add/save
				coll->Add(Value(&interface));
				testObject.Modified();
				assembly.save();

				// Check parent object is not in a modified state
				REQUIRE(!testObject.isModified());

				SECTION("Clear")
				{
					// Clear collection
					coll->Clear();
				}
				SECTION("Remove")
				{
					// Remove from collection
					coll->Remove(Value(&interface));
				}
			}

			// Check parent object is now in a modified state
			REQUIRE(testObject.isModified());
		}
		SECTION("Primitive")
		{
			// Define property
			CompAttribute prop(&klass, nullptr, "prop");
			prop.type = EntityFactory::Get().Resolve<Type>(&schema, QualifiedName("IntegerArray"));
			prop.Modified();

			// Perform initial save
			assembly.save();

			// Setup test instance (using schema as the parent object)
			Object testObject(&schema, &klass);

			// Get collection
			Collection* coll = testObject.GetValue(&prop, true).Get<Collection*>();

			// Clearing empty collection shouldn't modify
			coll->Clear();

			// Check parent object is not in a modified state
			REQUIRE(!testObject.isModified());

			SECTION("Add")
			{
				// Add to primitive array
				coll->Add(Value(1));
			}
			SECTION("Update")
			{
				// Perform initial add/save
				coll->Add(Value(1));
				testObject.Modified();
				assembly.save();

				// Check parent object is not in a modified state
				REQUIRE(!testObject.isModified());

				SECTION("Clear")
				{
					// Clear primitive array
					coll->Clear();
				}
				SECTION("Remove")
				{
					// Remove from primitive array
					coll->Remove(Value(1));
				}
			}

			// Check parent object is now in a modified state
			REQUIRE(testObject.isModified());
		}
	}
}