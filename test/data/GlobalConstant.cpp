#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/ConstantCategory.h>
#include <jadegit/data/GlobalConstant.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;
using namespace tinyxml2;

TEST_CASE("GlobalConstant.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	{
		GlobalConstant constant(&schema, nullptr, "TestConstant");

		// Check name has been set as expected
		CHECK("TestConstant" == constant.GetName());
		CHECK("TestConstant" == constant.GetValue("name").Get<string>());

		// Check constant has been added to collection
		CHECK(&constant == schema.constants.Get("TestConstant"));
	}

	// Check constant has been removed from collection
	CHECK(nullptr == schema.constants.Get("TestConstant"));
}

TEST_CASE("GlobalConstant.CategoryInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	ConstantCategory categoryA(&schema, nullptr, "TestCategoryA");
	ConstantCategory categoryB(&schema, nullptr, "TestCategoryB");

	{
		GlobalConstant constant(&schema, nullptr, "TestConstant");

		// Setting catgory should add to inverse collection
		constant.category = &categoryA;
		CHECK(categoryA.constants.Includes(&constant));

		// Switching to new category should remove from previous and add to new
		constant.category = &categoryB;
		CHECK(!categoryA.constants.Includes(&constant));
		CHECK(categoryB.constants.Includes(&constant));
	}

	// Check constant has been implicitly removed from last category
	CHECK(categoryB.constants.Empty());
}

TEST_CASE("GlobalConstant.Write", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.Created("47183823-2574-4bfd-b411-99ed177d3e43");
	schema.superschema = assembly.GetRootSchema();

	ConstantCategory category(&schema, nullptr, "TestCategory");
	category.Created("47183823-2574-4bfd-b411-99ed177d3e43");

	GlobalConstant constant(&schema, nullptr, "TestConstant");
	constant.Created("47183823-2574-4bfd-b411-99ed177d3e43");
	constant.category = &category;

	XMLDocument document;
	schema.Write(document);

	XMLPrinter printer;
	document.Print(&printer);

	CHECK(string(printer.CStr()) ==
		"<Schema name=\"TestSchema\" id=\"47183823-2574-4bfd-b411-99ed177d3e43\">\n"
		"    <superschema name=\"RootSchema\"/>\n"
		"    <ConstantCategory name=\"TestCategory\" id=\"47183823-2574-4bfd-b411-99ed177d3e43\"/>\n"
		"    <GlobalConstant name=\"TestConstant\" id=\"47183823-2574-4bfd-b411-99ed177d3e43\">\n"
		"        <category name=\"TestCategory\"/>\n"
		"    </GlobalConstant>\n"
		"</Schema>\n");
}

TEST_CASE("GlobalConstant.Resolution", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "BaseSchema");
	GlobalConstant constant(&schema, nullptr, "TestConstant");

	// Check local constant can be resolved irrespective of inherit option
	CHECK(Entity::resolve<GlobalConstant>(schema, "TestConstant", false, false) == &constant);
	CHECK(Entity::resolve<GlobalConstant>(schema, "TestConstant", false, true) == &constant);

	Schema subschema(assembly, nullptr, "SubSchema");
	subschema.superschema = &schema;

	// Check inherited constant can be resolved when inherit option is used
	CHECK(Entity::resolve<GlobalConstant>(subschema, "TestConstant", false, false) == nullptr);
	CHECK(Entity::resolve<GlobalConstant>(subschema, "TestConstant", false, true) == &constant);
}