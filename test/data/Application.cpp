#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Application.Resolution", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "BaseSchema");
	Application app(&schema, nullptr, "BaseSchemaApp");

	// Check local application can be resolved irrespective of inherit option
	CHECK(Entity::resolve<Application>(schema, "BaseSchemaApp", false, false) == &app);
	CHECK(Entity::resolve<Application>(schema, "BaseSchemaApp", false, true) == &app);

	Schema subschema(assembly, nullptr, "SubSchema");
	subschema.superschema = &schema;

	// Check inherited application can be resolved when inherit option is used
	CHECK(Entity::resolve<Application>(subschema, "BaseSchemaApp", false, false) == nullptr);
	CHECK(Entity::resolve<Application>(subschema, "BaseSchemaApp", false, true) == &app);
}