#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ObjectMeta.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <jadegit/vfs/RecursiveFileIterator.h>
#include <data/storage/ObjectFileStorage.h>
#include <data/storage/ObjectLoader.h>
#include <SourceFileSystem.h>

using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Assembly.Creation", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	CHECK(assembly.GetRootSchema() != nullptr);
}

TEST_CASE("Assembly.Creation2018", "[data]")
{
	SourceFileSystem fs("resources/Assembly/Creation2018");
	Assembly assembly(fs);

	CHECK(assembly.GetRootSchema().version == Version(18,0,1));
}

TEST_CASE("Assembly.Creation2020", "[data]")
{
	SourceFileSystem fs("resources/Assembly/Creation2020");
	Assembly assembly(fs);

	CHECK(assembly.GetRootSchema().version == Version(20,0,1));
}

TEST_CASE("Assembly.LoadEntityWithBadNameErrs", "[data]")
{
	SourceFileSystem fs("resources/Class/BadName");
	Assembly assembly(fs);

	CHECK_THROWS_AS(assembly.Load("schemas/TestSchema/types/TestClass.jox", true), jadegit_exception);
}

TEST_CASE("Assembly.RootSchemaValid", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	const RootSchema& rootSchema = assembly.GetRootSchema();

	for (auto klass : rootSchema->classes)
	{
		if (klass.second == static_cast<const Class*>(*rootSchema.object))
		{
			UNSCOPED_INFO("Object class shouldn't have superclass");
			CHECK(!klass.second->superclass);
		}
		else
		{
			UNSCOPED_INFO(klass.second->name << " class doesn't have a superclass");
			CHECK(klass.second->superclass);
		}
	}
}

TEST_CASE("Assembly.CircularLoad", "[data]")
{
	// Using repository with circular references which prompt entity retrieval before being explicitly loaded
	SourceFileSystem fs("resources/Assembly/Circular");

	Assembly assembly(fs);

	// Queue load for all files
	for (auto& file : RecursiveFileIterator(fs.open("")))
	{
		if (!file.isDirectory())
			assembly.getStorage().getLoader().queue(file);
	}
	// Load entities
	REQUIRE(assembly.getStorage().getLoader().load());

	// Get schema
	Schema* schema = assembly.schemas.Get("TestSchema");
	REQUIRE(schema);

	// Get classes
	Class* a = schema->classes.Get("A");
	CHECK(a);

	Class* b = schema->classes.Get("B");
	CHECK(b);

	// Get references
	ExplicitInverseRef* left = a->properties.Get<ExplicitInverseRef>("b");
	CHECK(left);

	ExplicitInverseRef* right = b->properties.Get<ExplicitInverseRef>("a");
	CHECK(right);

	// Check there is an inverse between references
	Inverse* inverse = left->inverses[right];
	CHECK(inverse);
	CHECK(inverse->counterpart);
	CHECK(right->inverses[left] == inverse->counterpart);
}