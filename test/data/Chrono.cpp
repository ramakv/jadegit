#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Chrono.h>

using namespace std;
using namespace std::chrono;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Chrono.NullDateToString", "[data]")
{
	Value<Date> value;
	CHECK(value.ToString() == "");
}

TEST_CASE("Chrono.DateToString", "[data]")
{
	Value<Date> value(day(12) / September / year(2022));
	CHECK(value.ToString() == "2022-09-12");
}

TEST_CASE("Chrono.NegativeDateToString", "[data]")
{
	Value<Date> value(day(25) / November / year(-4713));
	CHECK(value.ToString() == "-4713-11-25");
}

TEST_CASE("Chrono.InvalidDateToString", "[data]")
{
	Value<Date> value(nullopt);
	CHECK(value.ToString() == "invalid");
}

TEST_CASE("Chrono.TimeToString", "[data]")
{
	Value<Time> value(Time(hours(13) + minutes(36) + seconds(59) + milliseconds(899)));
	CHECK(value.ToString() == "13:36:59.899");
}

TEST_CASE("Chrono.InvalidTimeToString", "[data]")
{
	Value<Time> value(nullopt);
	CHECK(value.ToString() == "invalid");
}

TEST_CASE("Chrono.TimeStampToString", "[data]")
{
	Date date(day(11) / December / year(2021));
	Time time(hours(13) + minutes(36) + seconds(59) + milliseconds(456));
	Value<TimeStamp> value(TimeStamp(date, time));
	CHECK(value.ToString() == "2021-12-11 13:36:59.456");
}

TEST_CASE("Chrono.InvalidTimeStampToString", "[data]")
{
	Value<TimeStamp> value(nullopt);
	CHECK(value.ToString() == "invalid");
}

TEST_CASE("Chrono.StringToNullDate", "[data]")
{
	Any value(Value<string>(""));
	CHECK(Date() == value.Get<Date>());
}

TEST_CASE("Chrono.StringToDate", "[data]")
{
	Any value(Value<string>("1983-06-10"));
	CHECK(Date(day(10) / June / year(1983)) == value.Get<Date>());
}

TEST_CASE("Chrono.StringToNegativeDate", "[data]")
{
	Any value(Value<string>("-4713-11-25"));
	CHECK(Date(day(25) / November / year(-4713)) == value.Get<Date>());
}

TEST_CASE("Chrono.StringToInvalidDate", "[data]")
{
	Any value(Value<string>("invalid"));
	CHECK(Date(nullopt) == value.Get<Date>());
}

TEST_CASE("Chrono.StringToTime", "[data]")
{
	Any value(Value<string>("20:15:35.123"));
	CHECK(Time(hours(20) + minutes(15) + seconds(35) + milliseconds(123)) == value.Get<Time>());
}

TEST_CASE("Chrono.StringToInvalidTime", "[data]")
{
	Any value(Value<string>("invalid"));
	CHECK(Time(nullopt) == value.Get<Time>());
}

TEST_CASE("Chrono.StringToTimeStamp", "[data]")
{
	Any value(Value<string>("1969-01-02 20:15:35.123"));
	CHECK(TimeStamp(Date(day(2)/January/year(1969)), Time(hours(20) + minutes(15) + seconds(35) + milliseconds(123))) == value.Get<TimeStamp>());
}

TEST_CASE("Chrono.StringToInvalidTimeStamp", "[data]")
{
	Any value(Value<string>("invalid"));
	CHECK(TimeStamp(nullopt) == value.Get<TimeStamp>());
}