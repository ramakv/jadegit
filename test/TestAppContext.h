#pragma once
#include <jade/AppContext.h>
#include "TestDbContext.h"

namespace JadeGit
{
	class TestAppContext : Jade::AppContext
	{
	public:
		TestAppContext(const TestDbContext& db);
		TestAppContext(const TestDbContext& db, const Character* schema, const Character* app);
	};
}