#include <catch2/catch_test_macros.hpp>
#include <Graph.h>

using namespace std;
using namespace JadeGit;

namespace JadeGit
{
	class TestVertex
	{
	};

	using TestGraph = Graph<TestVertex>;
}

TEST_CASE("Graph.isDependent")
{
	TestGraph graph;
	TestVertex a;
	TestVertex b;
	TestVertex c;

	graph.AddVertex(&a);
	graph.AddVertex(&b);
	graph.AddVertex(&c);

	CHECK(!graph.isDependent(&a, &b));
	CHECK(!graph.isDependent(&a, &c));
	CHECK(!graph.isDependent(&b, &c));

	graph.AddEdge(&b, &a);
	CHECK(graph.isDependent(&a, &b));

	graph.AddEdge(&c, &b);
	CHECK(graph.isDependent(&b, &c));

	// Indirect dependency
	CHECK(graph.isDependent(&a, &c));
}

TEST_CASE("Graph.tryAddEdge")
{
	TestGraph graph;
	TestVertex a;
	TestVertex b;
	TestVertex c;

	graph.AddVertex(&a);
	graph.AddVertex(&b);
	graph.AddVertex(&c);

	CHECK(graph.tryAddEdge(&b, &a));
	CHECK(graph.tryAddEdge(&c, &b));
	CHECK(!graph.tryAddEdge(&a, &c));	// Cyclic dependency should be prevented
}