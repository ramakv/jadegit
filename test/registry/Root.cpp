#include <catch2/catch_test_macros.hpp>
#include <registry/Root.h>
#include <registry/Commit.h>
#include <registry/Repository.h>
#include <registry/Schema.h>

#if USE_JADE
#include <TestAppContext.h>
#endif

using namespace JadeGit;
using namespace JadeGit::Registry;

TEST_CASE("Root.Compare", "[registry]")
{
	Root empty;
	Root registry;

	SECTION("Experimental")
	{
		registry.experimental = true;

		CHECK(registry == registry);
		CHECK(registry != empty);
	}
	SECTION("Platform")
	{
		registry.platform = make_unique<PlatformT>();
		registry.platform->version = "99.0.00";
		registry.platform->charset = Charset_ANSI;

		CHECK(registry == registry);
		CHECK(registry != empty);
	}
	SECTION("Repositories")
	{
		RepositoryT repo;
		repo.name = "test1";
		repo.origin = "http://gitmock.com/mockrepo.git";
		registry.add(repo);

		repo.name = "test2";
		repo.origin = "";
		registry.add(repo);

		CHECK(registry == registry);
		CHECK(registry != empty);
	}
}

TEST_CASE("Root.Update", "[registry]")
{
	Root registry;
	
	RepositoryT foo;
	foo.name = "foo";
	foo.origin = "http://gitmock.com/foo.git";
	foo.latest.push_back(make_commit("aaaa"));
	foo.schemas.push_back(make_schema("FooSchema"));

	registry.add(foo);

	SECTION("Initialise")
	{
		Root updated;

		RepositoryT bar;
		bar.name = "bar";
		bar.origin = "http://gitmock.com/bar.git";
		bar.latest.push_back(make_commit("bbbb"));
		bar.schemas.push_back(make_schema("BarSchema"));

		updated.add(bar);

		registry.update(updated, false);

		REQUIRE(registry.repos.size() == 2);

		auto repo = registry.find_repo_by_name("foo");
		REQUIRE(repo);
		REQUIRE(*repo == foo);

		repo = registry.find_repo_by_name("bar");
		REQUIRE(repo);
		REQUIRE(*repo == bar);
	}
	SECTION("Update")
	{
		Root updated;
		
		RepositoryT foo;
		foo.name = "foo";
		foo.origin = "http://gitmock.com/foo.git";
		foo.latest.push_back(make_commit("bbbb"));
		foo.previous.push_back(make_commit("aaaa"));
		foo.schemas.push_back(make_schema("FooSchema"));
		foo.schemas.push_back(make_schema("FooTestSchema"));

		updated.add(foo);

		registry.update(updated, false);

		REQUIRE(registry.repos.size() == 1);

		auto repo = registry.find_repo_by_name("foo");
		REQUIRE(repo);
		REQUIRE(*repo == foo);
	}
}
