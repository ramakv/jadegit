if (-Not (Test-Path $env:JADE_SYSTEM/*.dat))
{
	& $PSScriptRoot\initialize.ps1
}
else
{
	& $PSScriptRoot\upgrade.ps1
}

jadegit -p $env:JADE_SYSTEM -i $env:JADE_INI install