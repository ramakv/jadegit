## 0.11.1 (2023-05-24)

### Fixed (1 change)

- [fix(scm): update schemas submodule location](jadelab/jadegit@0d800812f38fdfdb5bc900571f7c458206b2e077)

## 0.11.0 (2023-03-18)

### Added (9 changes)

- [feat(deploy): add pwsh deployment format](jadelab/jadegit@98045866faee80b4cbb3f7582a3b882ad763a020) ([merge request](jadelab/jadegit!158))
- [feat(console): add option to log progress using format specified](jadelab/jadegit@e22e35d0dd28d1803a407c9da319f62aa06e6069) ([merge request](jadelab/jadegit!157))
- [feat: support for deferred collection maintenance](jadelab/jadegit@a3aabea633a397e7192774ae95e8900cff223e15) ([merge request](jadelab/jadegit!155))
- [feat: add JADE 2022](jadelab/jadegit@8ce999a0f9146609fb9169a8d893f7da638a570d) ([merge request](jadelab/jadegit!150))
- [feat(data): support for timestamp attributes](jadelab/jadegit@dff53e250c598e2c38c712538ce1ab09bc7cf541) ([merge request](jadelab/jadegit!145))
- [feat(scm): add diff proxy classes](jadelab/jadegit@51cc2009e583aeb44af35238d9fc57927e041266) ([merge request](jadelab/jadegit!142))
- [feat(scm): add history form](jadelab/jadegit@48ad051240d504106fc75c8d1c8766dbe3e18971) ([merge request](jadelab/jadegit!138))
- [feat(config): deployment scripts](jadelab/jadegit@9fb3c460e78af7c5e6b5e8d599678c7b85f458f7) ([merge request](jadelab/jadegit!133))
- [Add generic xml deployment format](jadelab/jadegit@a80bbdbb7f789adbd24aca5fc9a7d3dedbe1c78d) ([merge request](jadelab/jadegit!132))

### Fixed (21 changes)

- [fix(scm): handle entity removal while parent still being added](jadelab/jadegit@3251160e7b23e73bc4e3541c5f23f68e003d2f6b)
- [fix(build): unit test method options](jadelab/jadegit@08f87e152f57a96f709b7a9e95bd07cee49eb334) ([merge request](jadelab/jadegit!163))
- [fix(scm): commit timestamp conversion](jadelab/jadegit@7f620b7a97e3a8832450fd48465f23cc5cf61101) ([merge request](jadelab/jadegit!162))
- [fix(build): web application properties](jadelab/jadegit@26e7c14d3bb00513c57b7341fbe35098dc5d7262) ([merge request](jadelab/jadegit!161))
- [fix(data): menu item shortcut keys](jadelab/jadegit@e1d08343f320a19d509d7c5270787479fbcc4410) ([merge request](jadelab/jadegit!161))
- [fix(build): web service header wsdl name](jadelab/jadegit@612de700515e4b1f8f87a381b5f615773e63f040) ([merge request](jadelab/jadegit!161))
- [fix(extract): add collection member type wsdl name](jadelab/jadegit@4da47e8e4cc95a847da78758e527878c2f3733fc) ([merge request](jadelab/jadegit!161))
- [fix(extract): add property xml nillable flag](jadelab/jadegit@fd1e9d6dc12dabb12b134a9f7251e820024cb577) ([merge request](jadelab/jadegit!161))
- [fix(build): exposed class auto added flag](jadelab/jadegit@4c7e7a3591d6d8c01a2e6b6c3b4ca4a99a00a3d4) ([merge request](jadelab/jadegit!161))
- [fix(extract): add menu item command id](jadelab/jadegit@e0a6e5989b3ca0358d40f2395b2c024f577e5ac9) ([merge request](jadelab/jadegit!161))
- [fix(extract): add printer/web form flags](jadelab/jadegit@041069ae5e9798ffb4f5f5a6a309667a3f95f780) ([merge request](jadelab/jadegit!161))
- [fix(dev): handle debugging scripts](jadelab/jadegit@f99225403d489aeda5e592bafaf386e7f2dda463) ([merge request](jadelab/jadegit!160))
- [fix(console): suppress progress bar during pipeline](jadelab/jadegit@e26e97eca9f9f89cdc89742bb9591fc6d5f6bbbf) ([merge request](jadelab/jadegit!157))
- [fix(data): add property name to runtime error during write](jadelab/jadegit@a48e519da1a921f85a6a6753801182b40e194d40)
- [fix(build): package documentation text](jadelab/jadegit@67939eb374d1d8e6f58a829d9880eecd5a0f4bc3) by @TangataRereke ([merge request](jadelab/jadegit!156))
- [fix(build): use control specific ddx format for primitive arrays](jadelab/jadegit@a8afd0098f0f0d74dece25f85dbba2787e143244) ([merge request](jadelab/jadegit!151))
- [fix(scm): identify indirect child dependencies during rename](jadelab/jadegit@58abbb93679aa6022c00f81140eda1eebb9562b0) ([merge request](jadelab/jadegit!149))
- [fix(build): infer update mode for implied references from inverse](jadelab/jadegit@be598dc13e2110258f64e9490de1d0fa7e08660c)
- [fix(dev): handle interface renames](jadelab/jadegit@8c9b9a7e5a4ff30afca0fe1100cfd422d0e3f40b) ([merge request](jadelab/jadegit!142))
- [fix(devenv): add auth library to thin client files](jadelab/jadegit@7434a04276c374870382223d64c9164996ed7550) ([merge request](jadelab/jadegit!140))
- [fix(data): load menu items](jadelab/jadegit@f66e6c66419fd9bb042b8809c006eafa24d437e4) ([merge request](jadelab/jadegit!135))

### Changed (10 changes)

- [refactor(scm): config backend](jadelab/jadegit@d6b6dcb23826a1bd30bfa5bba2277b77a546d0f9) ([merge request](jadelab/jadegit!153))
- [fix(auth): approve/reject credentials](jadelab/jadegit@4e90a4a1b6e1ae8e1db13c9b1b6bdb90bd128dc7) ([merge request](jadelab/jadegit!147))
- [fix: disable strict http parser mode](jadelab/jadegit@75323dda4b72946313278168e6888c1e24f2b0e8) ([merge request](jadelab/jadegit!147))
- [refactor(scm): remote operations](jadelab/jadegit@d4d81fdb2337b7e0381bbb4332ebcef05db35259) ([merge request](jadelab/jadegit!146))
- [Merge branch 'ksaul/101-references' into 'main'](jadelab/jadegit@872322a6539310edbae18e69818c4deebaf7764a) ([merge request](jadelab/jadegit!144))
- [refactor(scm): store index data in common map file](jadelab/jadegit@b16a5a8a3ee1ba03d273a7cb244adb1fc214ad05) ([merge request](jadelab/jadegit!141))
- [refactor(scm): use worktree for change tracking](jadelab/jadegit@8e7d4e0b9d8f32a7f17b16da4191f85802fc29ea) ([merge request](jadelab/jadegit!141))
- [feat(dev): allow changes to JadeGitSchema when repo has been cloned](jadelab/jadegit@919945879ef64b54f8b987eab7ee82cafe884f3f) ([merge request](jadelab/jadegit!137))
- [refactor(scm): install schema using submodule to build deployment](jadelab/jadegit@6c63bd57f3d50629f2369ebf882e42daa1f9368b) ([merge request](jadelab/jadegit!134))
- [feat(schema): run scripts during internal deployment](jadelab/jadegit@cde492b86397b7abe8bbf703163286e00b12d425) ([merge request](jadelab/jadegit!133))

### Removed (1 change)

- [refactor(console): remove option to force install](jadelab/jadegit@953a75296f3fe1b9c8e615bdf06d892357244d96) ([merge request](jadelab/jadegit!134))

## 0.10.0 (2022-08-23)
### Added
- Support for imported classes with local features.
- Console command for cloning repository.
- Support for .jadegit configuration file using toml++.
- Console deploy manifest commands to store/retrieve origin/commit during deployment.

### Changed
- Prevent redundant subschema class copy extracts.
- Use 'jox' file extension for xml format.
- Use subfolders for schemas & forms, and lowercase all entity subfolder names.
- Use system configuration file.
- Removed option to set target JADE version during build (now defined by config).

### Fixed
- Renamed/split library into jadegitdev/jadegitscm so entry points are found by JADE when commands invoked via console.
- Derive logs directory from executable path (rather than relative to current working directory).

## 0.9.0 (2022-04-09)
### Added
- Support for HTML documents.
- Support for dynamic properties.
- Support for JADE 2020 SP1.

### Changed
- Refactored inverse maintenance within proxy data model using metadata.
- Use 'main' as default branch name.
- Resolve current session using process instead of username.
- Refactored id allocation & storage using stduuid.

### Fixed
- Use integer input type for helpContextId properties during extract.
- Ignore interface references during extract.
- Handle resolving inherited/cloned locale entities.
- Handle deleting subschema subclasses before base class.
- Define static schema properties for subschema member key dictionary usage.
- Ignore duplicated system files during extract.
- Confirm order during initial extract when objects may be created out of order.
- Mark entities as inferred when they may not be extracted.
- Reset data items when ignored during extract.
- Ignore attribute precision unless decimal type during extract.
- Clean up non-static schemas when unloading during save.
- Handle encoding accent characters within documentation text.
- Handle defining custom event methods before any element of dependent classes.
- Handle resolving inherited applications.
- Handle loading circular package references.

### Removed
- Support for JADE 2018.
- Support for JADE 2020.

## 0.8.0 (2021-04-26)
### Added
- Support for JADE 2020.
- Support for unicode environments.
- Ability for console to open JADE database connection.
- Console commands to open bare repository, switch branch, extract schemas & commit.
- CardSchema definition (now excluded from source control by default).

### Changed
- Refactored console to support shell mode for series of commands while connected to JADE database.
- Pushing new branches will now default to only or oldest remote when 'origin' doesn't exist.
- Embedded database definition within schema file to support names which don't match default.

### Fixed
- ActiveXControl properties which cannot be accessed without creating ActiveX object will now be skipped during extract.
- Allow text boxes on remote dialog to scroll horizontally.
- Support for adding/deleting remotes.
- Support for deleting translatable strings.

# 0.7.0 (2020-11-05)
### Added
- Support for showing progress.
- Support for ActiveX libraries.
- Support for .NET libraries.
- Support for type methods.

### Changed
- Evaluate prior/next version when schema selected.
- Install process to allow re-orgs when required due to control changes.

### Fixed
- Handling of schema deregistration and resetting branch state when unloading repositories.
- Handling of form & control renames.
- Handling of form deletions.
- Handling of copying forms.
- Support for moving classes (better handling for new classes being inserted above existing).
- Staging renames with updates to related entities.
- Handling of super-interface mapping dependencies.

## 0.6.0 (2020-06-07)
### Added
- Support for relational views.
- Support for tracking changes to latest schema version.

### Changed
- Updated internal deployments to use JadeSchemaLoader & JadeReorgApp.
- Refactored build process to generate DDX data files.
- Improved support for locales (changing schema default & converting base locale to/from clone).
- Simplified DbClassMap format (now written as single element within class file).
- Refactored authentication using presentation client library to retrieve git credentials (jadegitauth).
- Refactored RootSchema metadata generation to simplify future updates.

### Fixed
- Handling of duplicates allowed option for collection classes.
- Handling of collection class option to map all instances to selected map file.
- Handling of dependency between form control methods and custom control event methods.
- Handling of cross-dependencies between global constants spanning multiple categories.

### Removed
- Support for JADE 2016.

## 0.5.0 (2020-02-05)
### Added
- Support for C# exposures.
- Support for web services.
- Support for schema views (local use only).

### Changed
- Refactored how references are stored to support branch folders.
- Refactored schema file generation to include modified timestamps.
- Refactored console build to load data from git repository.
- Removed use of default 'JADE' root folder.

### Fixed
- Support for cloning repositories from local filesystem.
- Support for cloned locale definitions.
- Support for references to imported types & imported interface mappings.
- Removal of local branches (config backend refactored to support removals).
- Schema deletion now handled via separate command files (avoids problems when internal deployment is being restarted).

## 0.4.0 (2019-11-02)
### Added
- Ability to restart & abort internal deployments (used to load/unload/reset branches & repositories).
- Ability to stage & commit changes selectively.
- Ability to reset index with changes being kept (unstage all).

### Changed
- Refactored internal deployment build process to store files persistently.
- Schema versioning & re-org functions are now allowed, provided there is no internal deployment outstanding.
- Building internal deployments are now prevented when there's an outstanding re-org.
- Major entity file path now used to resolve parent during load, removing need to store qualified name within file.

## 0.3.0 (2019-07-04)
### Added
- Support for adding existing schemas to source control.
- Support for custom control subclasses.
- Support for building schema updates using command line utility with previous/latest source folders.
- Support for imported packages.
- Support for locale formats.
- Implemented branch reset functions to discard changes.
- Implemented repository reset function to unload all branches with complete schema reload.

### Changed
- Deep extract now performed when new entity is added, removing the need to track child changes.
- Consolidated into one shared library and/or command line executable (jadegit.dll & jadegit.exe).
- Suppress extract of shallow/proxy entities which cannot be loaded (i.e. subschema copies of an interface).

### Fixed
- Support for external methods.
- Support for removing class interface mappings.
- Support for libraries & external functions.

## 0.2.0 (2019-04-16)
### Added
- Support for JADE 2018.
- Support for translatable strings.
- Support for schema/repository associations.
- Support for unloading repositories.
- Support for removing schemas.
- Support for renaming schema entities.
- Support for conditional collections.

### Changed
- Improved extract process using data mappings which can be explicitly defined or automatically generated as required.
- Improved support for subschemas.
- Improved support for forms (renames, event methods, menu items).
- Improved support for adding/removing inverses between references.
- Jade-Git Explorer now uses notifications to refresh/show changes (possibly caused by another user).

### Fixed
- Replaced use of paramSetString with paramSetCString to explicitly initialize DskParam format.
- Suppress 'unknown property' errors with warnings being logged instead (identified during automatic data mapping).
- Suppress Jade-Git Explorer when starting IDE in administration mode.

## 0.1.0 (2018-12-15)
### Added
- This CHANGELOG file to document notable changes going forward.
- jadegit UI explorer, which runs alongside the JADE IDE (refer to README for configuration steps), provides:
  - Ability to create/clone repositories.
  - Ability to manage branches, with push & pull support for remote repositories.
  - Ability to track changes to basic schema entities (supporting add/update/delete operations).
  - Ability to commit changes, during which affected entities are re-extracted from JADE and converted to source control friendly format.
- Ability to build schemas from jadegit repository using command line utility (independently of JADE IDE).
