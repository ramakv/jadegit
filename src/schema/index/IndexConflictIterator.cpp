#include "IndexEntry.h"
#include <schema/Exception.h>
#include <schema/ObjectRegistration.h>

using namespace std;

namespace JadeGit::Schema
{
	class IndexConflictIterator : public GitObject<git_index_conflict_iterator>
	{
	public:
		using GitObject::GitObject;
		IndexConflictIterator(const Index& index);

		bool next(IndexEntry& base_out, IndexEntry& ours_out, IndexEntry& theirs_out) const
		{
			const git_index_entry* base = nullptr;
			const git_index_entry* ours = nullptr;
			const git_index_entry* theirs = nullptr;

			int result = git_index_conflict_next(&base, &ours, &theirs, *this);
			if (result == GIT_ITEROVER)
				return false;
			git_throw(result);

			Index index = getProperty<Index>(TEXT("parent"));

			base_out = IndexEntry(index, base);
			ours_out = IndexEntry(index, ours);
			theirs_out = IndexEntry(index, theirs);
			return true;
		}
	};
	static GitObjectRegistration<IndexConflictIterator> registration(TEXT("IndexConflictIterator"));

	IndexConflictIterator::IndexConflictIterator(const Index& index) : GitObject(registration, nullptr)
	{
		jade_throw(setProperty(TEXT("parent"), index));
		git_throw(git_index_conflict_iterator_new(git_ptr(*this), index));
	}

	int JOMAPI jadegit_index_conflict_iterator_new(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Index index;
				JADE_RETURN(paramGetOid(*pParams, index.oid));

				IndexConflictIterator iter(index);
				return paramSetOid(pReturn, iter.oid);
			});
	}

	int JOMAPI jadegit_index_conflict_iterator_next(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pBase;
				DskParam* pOurs;
				DskParam* pTheirs;
				JADE_RETURN(paramGetParameter(*pParams, 1, pBase));
				JADE_RETURN(paramGetParameter(*pParams, 2, pOurs));
				JADE_RETURN(paramGetParameter(*pParams, 3, pTheirs));

				IndexEntry base;
				IndexEntry ours;
				IndexEntry theirs;
				bool result = IndexConflictIterator(pBuffer).next(base, ours, theirs);

				JADE_RETURN(paramSetOid(*pBase, base.oid));
				JADE_RETURN(paramSetOid(*pOurs, ours.oid));
				JADE_RETURN(paramSetOid(*pTheirs, theirs.oid));
				return paramSetBoolean(*pReturn, result);
			});
	}
}