#include "Repository.h"
#include "Branch.h"
#include "Exception.h"
#include "ObjectRegistration.h"
#include "Progress.h"
#include "Remote.h"
#include "Task.h"
#include "config/Config.h"
#include "data/Worktree.h"
#include "index/Index.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Repository> registration(TEXT("Repository"));

	Repository::Repository(unique_ptr<git_repository> ptr) : GitObject(registration, move(ptr))
	{
	}

	void Repository::abort() const
	{
		return worktree().abort(*this);
	}

	void Repository::commit(const std::string& message) const
	{
		worktree().commit(*this, message);
	}

	bool Repository::continue_(const string& message, IProgress* progress) const
	{
		return worktree().continue_(*this, message, progress);
	}

	Config Repository::config() const
	{
		unique_ptr<git_config> config;
		git_throw(git_repository_config(git_ptr(config), *this));
		return Config(*this, move(config));
	}

	unique_ptr<git_reference> Repository::head() const
	{
		unique_ptr<git_reference> head;

		auto result = git_repository_head(git_ptr(head), *this);
		if (result == GIT_ENOTFOUND || result == GIT_EUNBORNBRANCH)
			return nullptr;
		git_throw(result);

		return head;
	}

	Index Repository::index() const
	{
		// Return existing
		Index index = getProperty<Index>(TEXT("index_"));
		if (!index.isNull())
			return index;

		// Open index when required
		return Index::open(*this);
	}

	bool Repository::merge(const git_reference* theirs, IProgress* progress) const
	{
		unique_ptr<git_annotated_commit> commit;
		git_throw(git_annotated_commit_from_ref(git_ptr(commit), *this, theirs));
		return worktree().merge(*this, commit.get(), progress);
	}

	string Repository::message() const
	{
		return worktree().message();
	}

	bool Repository::reset(const git_commit* commit, bool hard, IProgress* progress) const
	{
		return worktree().reset(*this, commit, hard, progress);
	}

	git_repository_state_t Repository::state() const
	{
		return worktree().state();
	}

	WorktreeData Repository::worktree() const
	{
		auto worktree = getProperty<WorktreeData>(TEXT("worktree"));
		if (worktree.isNull())
			throw jadegit_exception("No worktree");

		return worktree;
	}

	int JOMAPI jadegit_repo_abort(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Repository(pBuffer).abort();
				return J_OK;
			});
	}

	int JOMAPI jadegit_repo_commit(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				string message;
				JADE_RETURN(paramGetString(*pParams, message));

				Repository(pBuffer).commit(message);
				return J_OK;
			});
	}

	int JOMAPI jadegit_repo_config(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Repository repo(pBuffer);
				return paramSetOid(pReturn, repo.config().oid);
			});
	}

	int JOMAPI jadegit_repo_continue(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		string message;
		JADE_RETURN(paramGetString(*pParams, message));

		Repository repo(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress)
			{
				return repo.continue_(message, progress);
			});
	}

	int JOMAPI jadegit_repo_head(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Repository repo(pBuffer);
				return paramSetOid(pReturn, Reference::make(repo, repo.head()).oid);
			});
	}

	int JOMAPI jadegit_repo_index(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Repository repo(pBuffer);
				return paramSetOid(pReturn, repo.index().oid);
			});
	}

	int JOMAPI jadegit_repo_message(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetString(*pReturn, widen(Repository(pBuffer).message()));
			});
	}

	int JOMAPI jadegit_repo_state(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetInteger(*pReturn, Repository(pBuffer).state());
			});
	}
}