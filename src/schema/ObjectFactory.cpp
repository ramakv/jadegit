#include "ObjectFactory.h"
#include <jadegit/Exception.h>
#include <jomobj.hpp>

using namespace Jade;

namespace JadeGit::Schema
{
	void GitObjectFactory::dispose(const DskObjectId& oid) const
	{
		lookup(oid.classNo).dispose(oid);
	}

	GitObjectFactory& GitObjectFactory::get()
	{
		static GitObjectFactory f; return f;
	}

	void GitObjectFactory::registration(const Character* key, const GitObjectFactory::Registration* registrar)
	{
		registryByName[key] = registrar;
	}

	void GitObjectFactory::initialize() const
	{
		static bool initialized = false;
		if (initialized)
			return;

		initialized = true;

		/* Resolve schema */
		DskSchema rootSchema(&RootSchemaOid);
		DskSchema schema;
		rootSchema.getSubschema(TEXT("JadeGitSchema"), schema);

		if (schema.isNull())
			throw jadegit_exception("Schema has not been installed");

		/* Resolve each class */
		for (auto& registration : registryByName)
		{
			DskClass cls;
			schema.getLocalClass(registration.first.c_str(), cls);

			if (cls.isNull())
				throw jadegit_exception("Schema missing class [" + narrow(registration.first) + "]");

			ClassNumber number = 0;
			jade_throw(cls.getNumber(&number));

			/* Associate registration with class number */
			registryByNumber[number] = registration.second;
			registration.second->initialize(number);
		}
	}

	const GitObjectFactory::Registration& GitObjectFactory::lookup(const ClassNumber& key) const
	{
		initialize();

		/* Attempt basic find without needing to resolve class */
		auto iter = registryByNumber.find(actualClass(key));
		if (iter != registryByNumber.end())
			return *(iter->second);

		throw jadegit_exception("Unhandled schema object");
	}
}