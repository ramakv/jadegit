target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Config.cpp
	${CMAKE_CURRENT_LIST_DIR}/ConfigEntry.cpp
	${CMAKE_CURRENT_LIST_DIR}/ConfigIterator.cpp
)