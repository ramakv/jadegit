#include "Branch.h"
#include "Exception.h"
#include "Remote.h"
#include "ObjectRegistration.h"
#include "Task.h"
#include <jade/Transaction.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<LocalBranch> local_branch(TEXT("LocalBranch"));
	static GitObjectRegistration<RemoteBranch> remote_branch(TEXT("RemoteBranch"));

	Branch Branch::lookup(const Repository& repo, const string& name, git_branch_t type)
	{
		// Lookup reference
		unique_ptr<git_reference> ptr;
		git_throw(git_branch_lookup(git_ptr(ptr), repo, name.c_str(), type));

		// Return new
		return Branch::make(repo, move(ptr));
	}

	Branch Branch::make(const Repository& repo, unique_ptr<git_reference> ptr)
	{
		git_branch_t type = git_reference_is_remote(ptr.get()) ? git_branch_t::GIT_BRANCH_REMOTE : git_branch_t::GIT_BRANCH_LOCAL;
		return make(repo, move(ptr), type);
	}

	Branch Branch::make(const Repository& repo, unique_ptr<git_reference> ptr, git_branch_t type)
	{
		switch (type)
		{
		case git_branch_t::GIT_BRANCH_LOCAL:
			return LocalBranch(repo, move(ptr));
		case git_branch_t::GIT_BRANCH_REMOTE:
			return RemoteBranch(repo, move(ptr));
		default:
			return Branch(repo, move(ptr));
		}
	}

	LocalBranch Branch::branch(string name, bool track_upstream) const
	{
		// Resolve commit
		unique_ptr<git_commit> source_commit;
		git_throw(git_commit_lookup(git_ptr(source_commit), git_reference_owner(*this), git_reference_target(*this)));

		// Start transaction
		Transaction transaction;

		// Create new branch 
		unique_ptr<git_reference> branch_ref;
		git_throw(git_branch_create(git_ptr(branch_ref), git_reference_owner(*this), name.c_str(), source_commit.get(), FALSE));

		// Set upstream for tracking branches
		if (track_upstream)
			git_throw(git_branch_set_upstream(branch_ref.get(), git_reference_shorthand(*this)));

		// Commit transaction
		transaction.commit();

		// Return new branch
		return LocalBranch(repo(), move(branch_ref));
	}
	
	LocalBranch::LocalBranch(const Repository& repo, std::unique_ptr<git_reference> ptr) : Branch(local_branch, repo, move(ptr))
	{
	}

	bool LocalBranch::push(const Remote& remote, IProgress* progress) const
	{
		if (remote.isNull())
			throw jadegit_exception("Remote required");

		return remote.push(*this, progress);
	}

	void LocalBranch::set_upstream(const string& branch_name) const
	{
		git_throw(git_branch_set_upstream(*this, branch_name.empty() ? nullptr : branch_name.c_str()));
	}

	template <auto function>
	string upstream_config(const LocalBranch& branch)
	{
		unique_ptr<git_buf> name = make_unique<git_buf>();
		auto result = function(name.get(), git_reference_owner(branch), git_reference_name(branch));
		if (result == GIT_ENOTFOUND)
			return string();
		git_throw(result);

		return string(name->ptr, name->size);
	}

	string LocalBranch::upstream_merge() const
	{
		return upstream_config<git_branch_upstream_merge>(*this);
	}

	string LocalBranch::upstream_name() const
	{
		return upstream_config<git_branch_upstream_name>(*this);
	}

	string LocalBranch::upstream_remote() const
	{
		return upstream_config<git_branch_upstream_remote>(*this);
	}

	RemoteBranch::RemoteBranch(const Repository& repo, unique_ptr<git_reference> ptr) : Branch(remote_branch, repo, move(ptr))
	{
	}

	string RemoteBranch::remote_name() const
	{
		unique_ptr<git_buf> name = make_unique<git_buf>();
		git_branch_remote_name(name.get(), git_reference_owner(*this), git_reference_name(*this));
		return string(name->ptr, name->size);
	}

	int JOMAPI jadegit_branch_branch(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pName = nullptr;
				DskParam* pTrackUpstream = nullptr;

				JADE_RETURN(paramGetParameter(*pParams, 1, pName));
				JADE_RETURN(paramGetParameter(*pParams, 2, pTrackUpstream));

				String name;
				JADE_RETURN(paramGetString(*pName, name));

				bool track_upstream = false;
				JADE_RETURN(paramGetBoolean(*pTrackUpstream, track_upstream));

				LocalBranch branch = Branch(pBuffer).branch(narrow(name), track_upstream);
				return paramSetOid(pReturn, branch.oid);
			});
	}
	
	int JOMAPI jadegit_branch_lookup(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo = nullptr;
				DskParam* pName = nullptr;
				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pName));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				string name;
				JADE_RETURN(paramGetString(*pName, name));

				// Determine branch type to lookup based on subclass type method has been invoked for
				DskClass klass(&pBuffer->oid);
				ClassNumber classNo;
				JADE_RETURN(klass.getNumber(&classNo));

				git_branch_t type = git_branch_t::GIT_BRANCH_ALL;
				if (classNo == local_branch)
					type = git_branch_t::GIT_BRANCH_LOCAL;
				else if (classNo == remote_branch)
					type = git_branch_t::GIT_BRANCH_REMOTE;
				
				// Lookup & return branch
				auto branch = Branch::lookup(repo, name, type);
				return paramSetOid(pReturn, branch.oid);
			});
	}

	int JOMAPI jadegit_branch_merge(DskBuffer *pBuffer, DskParam *pParams, DskParam *pReturn)
	{
		Branch branch(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress)
			{
				return branch.repo().merge(branch, progress);
			});
	}

	int JOMAPI jadegit_branch_push(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		Remote remote;
		JADE_RETURN(paramGetOid(*pParams, remote.oid));

		LocalBranch branch(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress) { return branch.push(remote, progress); });
	}

	int JOMAPI jadegit_branch_remote_name(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetString(*pReturn, widen(RemoteBranch(pBuffer).remote_name()));
			});
	}
	
	int JOMAPI jadegit_branch_remove(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				// Delete branch
				Transaction transaction;
				git_throw(git_branch_delete(LocalBranch(pBuffer)));
				transaction.commit();

				return J_OK;
			});
	}

	int JOMAPI jadegit_branch_set_upstream(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				string branch_name;
				jade_throw(paramGetString(*pParams, branch_name));

				LocalBranch(pBuffer).set_upstream(branch_name);
				return J_OK;
			});
	}

	int JOMAPI jadegit_branch_upstream_name(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetString(*pReturn, widen(LocalBranch(pBuffer).upstream_name()));
			});
	}

	int JOMAPI jadegit_branch_upstream_remote(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetString(*pReturn, widen(LocalBranch(pBuffer).upstream_remote()));
			});
	}
}