#include "DiffLine.h"
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<DiffLine> registration(TEXT("DiffLine"));

	DiffLine::DiffLine(const git_diff_line* line) : GitObject(registration, line)
	{
		jade_throw(setProperty(TEXT("content_offset"), line->content_offset));
		jade_throw(setProperty(TEXT("new_lineno"), line->new_lineno));
		jade_throw(setProperty(TEXT("num_lines"), line->num_lines));
		jade_throw(setProperty(TEXT("old_lineno"), line->old_lineno));
	}

	int JOMAPI jadegit_diff_line_content(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_method<DiffLine>(pBuffer, pParams, [&](git_diff_line* p, DskParam& value)
			{
				return paramSetString(value, widen(string(p->content, p->content_len)));
			});
	}
}