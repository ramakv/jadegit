#include "DiffHunk.h"
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<DiffHunk> registration(TEXT("DiffHunk"));

	DiffHunk::DiffHunk(const git_diff_hunk* hunk) : GitObject(registration, hunk)
	{
		jade_throw(setProperty(TEXT("old_start"), hunk->old_start));
		jade_throw(setProperty(TEXT("old_lines"), hunk->old_lines));
		jade_throw(setProperty(TEXT("new_start"), hunk->new_start));
		jade_throw(setProperty(TEXT("new_lines"), hunk->new_lines));
	}

	int JOMAPI jadegit_diff_hunk_header(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_method<DiffHunk>(pBuffer, pParams, [&](git_diff_hunk* p, DskParam& value)
			{
				return paramSetString(value, widen(string(p->header, p->header_len)));
			});
	}
}