#pragma once
#include "Proxy.h"

namespace JadeGit
{
	class IProgress;
}

namespace JadeGit::Schema
{
	class Config;
	class Index;
	class WorktreeData;

	class Repository : public GitObject<git_repository>
	{
	public:
		using GitObject::GitObject;
		Repository(std::unique_ptr<git_repository> ptr);
		
		Config config() const;
		std::unique_ptr<git_reference> head() const;
		Index index() const;
		std::string message() const;
		git_repository_state_t state() const;

		// Operations that may be invoked
		void commit(const std::string& message) const;
		bool merge(const git_reference* theirs, IProgress* progress) const;
		bool reset(const git_commit* commit, bool hard, IProgress* progress) const;
		
		// Handle aborting/continuing current operation
		void abort() const;
		bool continue_(const std::string& message, IProgress* progress) const;

	private:
		friend Index;
		WorktreeData worktree() const;
	};
}