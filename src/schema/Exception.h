#pragma once
#include "Object.h"
#include <jadegit/git2.h>

namespace JadeGit::Schema
{
	class GitException : public Object
	{
	public:
		GitException(const char* message, const int code = 64000);
		GitException(std::runtime_error &ex, ExceptionCause cause = EC_PRECONDITION);
		GitException(git_exception& ex, ExceptionCause cause = EC_PRECONDITION);
		~GitException() {}

        template <typename T>
        static int wrapper(T func)
        {
			try
			{
				return func();
			}
			catch (git_exception& e)
			{
				GitException ex(e);
			}
			catch (std::runtime_error& e)
			{
				GitException ex(e);
			}

            return 0;
        }
	};
}