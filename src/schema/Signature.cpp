#include "Signature.h"
#include "Exception.h"
#include "ObjectRegistration.h"
#include <jade/Chrono.h>
#include <functional>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Signature> registration(TEXT("Signature"));
	
	Signature::Signature(const Commit& parent, const Character* prop, const git_signature* ptr) : GitObject(registration, ptr, parent, prop)
	{
	}

	int JOMAPI jadegit_signature_email(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<Signature>(pBuffer, pParams, &git_signature::email);
	}

	int JOMAPI jadegit_signature_name(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<Signature>(pBuffer, pParams, &git_signature::name);
	}

	int JOMAPI jadegit_signature_when(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_method<Signature>(pBuffer, pParams, [&](git_signature* signature, DskParam& value)
			{
				using namespace chrono;
				return paramSetTimeStampOffset(value, system_clock::time_point{ seconds{ signature->when.time } }, signature->when.offset * -1);
			});
	}
}