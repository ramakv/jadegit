#pragma once
#include <jade/Lock.h>

namespace JadeGit::Schema
{
	class DeploymentBuilder;

	class DeploymentLock : public Jade::Lock
	{
	public:
		DeploymentLock(LockDuration duration = TRANSACTION_DURATION, LockWaitTime wait = LOCK_TIMEOUT_SERVER_DEFINED);

	protected:
		friend DeploymentBuilder;
		DeploymentLock(const char* error_message, LockType type, LockDuration duration = TRANSACTION_DURATION, LockWaitTime wait = LOCK_TIMEOUT_SERVER_DEFINED);
	};
}