#include "DeploymentBuilder.h"
#include "Deployment.h"
#include "DeploymentLock.h"
#include "DeploymentReorg.h"
#include "DeploymentScript.h"
#include "SchemaFile.h"
#include <jade/Iterator.h>
#include <schema/data/Root.h>
#include <schema/data/Schema.h>
#include <schema/data/User.h>
#include <schema/Session.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	DeploymentLock::DeploymentLock(LockDuration duration, LockWaitTime wait) : DeploymentLock("Deployment in progress", SHARED_LOCK, duration, wait) {}

	DeploymentLock::DeploymentLock(const char* error_message, LockType type, LockDuration duration, LockWaitTime wait)
	{
		const Root& root = Root::get();

		if (root.deploying())
			throw jadegit_exception(error_message);
		
		lock(root.oid, type, duration, wait);

		if (root.deploying())
			throw jadegit_exception(error_message);
	}

	DeploymentBuilder::DeploymentBuilder(const IGitRepositoryData& data) : DeploymentBuilder(make_shared<GitDeployment>(), data) {}

	DeploymentBuilder::DeploymentBuilder(shared_ptr<GitDeployment> deployment, const IGitRepositoryData& data) : deployment(deployment), jade_repo(data)
	{
		DeploymentLock lock("Deployment cannot be initiated while there's another already in progress.\n\nExisting deployment must be completed or aborted.", EXCLUSIVE_LOCK);

		// Check schemas aren't already in a re-org state
		DskSchema rootSchema(&RootSchemaOid);
		DskSchema schema;
		Iterator<DskSchema> iter(rootSchema, PRP_Schema__schemasByNumber);
		while (iter.next(schema))
		{
			if(schema.getVersionState() != ObjectVersionState::OBJVERSTATE_CURRENT_ONLY)
				throw jadegit_exception("Deployment cannot be initiated while there's a pending reorg");
		}

		jade_throw(deployment->createObject());
		jade_throw(deployment->setProperty(TEXT("root"), Root::get()));
		jade_throw(deployment->setProperty(TEXT("repo"), jade_repo));
		jade_throw(deployment->setProperty(TEXT("user"), Session::get().user()));
	}

	void DeploymentBuilder::RegisterSchema(const std::string& name, bool registering)
	{
		// Check existing schema associations
		GitSchema schema;
		if (jade_repo.GetSchema(name, schema) == registering)
			return;

		// Safeguard against attempts to re-register known schema
		if (registering && GitSchema::isRegistered(name))
			throw jadegit_exception("Schema [" + name + "] is already registered to another repository (or excluded)");

		// Setup schema association
		// NOTE: For schemas being deregistered a duplicate schema is created for the repository
		// again which signifies the need to remove the original when changes are being finalized
		jade_throw(schema.createObject());
		jade_throw(schema.setProperty(TEXT("kind"), DSKSCHEMA));
		schema.setProperty(TEXT("name"), name);
		jade_throw(schema.setProperty(TEXT("deployment"), *deployment));
		jade_throw(schema.setProperty(TEXT("repo"), jade_repo));
		jade_throw(schema.setProperty(TEXT("root"), Root::get()));
	}

	std::unique_ptr<std::ostream> DeploymentBuilder::AddCommandFile(bool latestVersion)
	{
		// Set flag indicating reorg is needed
		if (latestVersion)
			needsReorg = true;

		// Cause re-org before loading into current schema version
		else if (needsReorg)
			Reorg();

		GitSchemaFile file(*deployment, latestVersion, GitSchemaFile::Type::JCF);
		return file.CreateOutputStream();
	}

	std::unique_ptr<std::ostream> DeploymentBuilder::AddSchemaFile(const std::string& schema, bool latestVersion)
	{
		// Set flag indicating reorg is needed
		if (latestVersion)
			needsReorg = true;

		// Cause re-org before loading into current schema version
		else if (needsReorg)
			Reorg();

		GitSchemaFile file(*deployment, latestVersion, GitSchemaFile::Type::SCM, schema);
		this->schemaFile = file.oid;
		return file.CreateOutputStream();
	}

	std::unique_ptr<std::ostream> DeploymentBuilder::AddSchemaDataFile(const std::string& schema, bool latestVersion)
	{
		// Set flag indicating reorg is needed
		if (latestVersion)
			needsReorg = true;

		// Cause re-org before loading into current schema version
		else if (needsReorg)
			Reorg();

		GitSchemaFile schemaFile(this->schemaFile);
		if (schemaFile.isNull() || schemaFile.isLatest() != latestVersion)
			return GitSchemaFile(*deployment, latestVersion, GitSchemaFile::Type::DDX, schema).CreateOutputStream();
		else
			return GitSchemaFile(schemaFile, latestVersion, GitSchemaFile::Type::DDX, schema).CreateOutputStream();
	}

	void DeploymentBuilder::addScript(const Build::Script& script)
	{
		// Cause re-org before running any scripts
		if (needsReorg)
			Reorg();

		GitDeploymentScript command(*deployment, script);
	}

	void DeploymentBuilder::Reorg()
	{
		if (reorging)
			return;

		GitDeploymentReorg reorg(*deployment);

		// Set flag indicating we've just created a re-org command
		reorging = true;

		// Reset flag indicating reorg is needed
		needsReorg = false;
	}

	void DeploymentBuilder::Flush(bool final)
	{
		// Add final re-org command if needed
		if (final && needsReorg)
			Reorg();

		// Reset reorging flag
		reorging = false;

		// Reset last schema file
		schemaFile = NullDskObjectId;
	}
}