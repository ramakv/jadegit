#include "WorktreeDeployment.h"
#include <jade/JadeBytes.h>
#include <schema/Exception.h>
#include <schema/ObjectRegistration.h>
#include <schema/data/Index.h>
#include <schema/data/Worktree.h>
#include <schema/data/WorktreeState.h>
#include <schema/data/refdb/NodeDict.h>

using namespace std;

namespace JadeGit::Schema
{
	static GitObjectRegistration<WorktreeDeployment> registration(TEXT("WorktreeDeployment"));

	WorktreeDeployment::WorktreeDeployment() : GitDeployment(registration) {}

	WorktreeDeployment::WorktreeDeployment(const GitDeployment& parent, const WorktreeData& worktree, Action action) : GitDeployment(parent, registration)
	{
		setAction(action);
		setWorktree(worktree);
	}

	WorktreeDeployment::Action WorktreeDeployment::action() const
	{
		return static_cast<Action>(getProperty<Byte>(TEXT("action")));
	}

	unique_ptr<git_commit> WorktreeDeployment::commit(git_repository* repo) const
	{
		// Get OID
		git_oid oid = getProperty<git_oid>(TEXT("commit"));

		// Lookup commit
		unique_ptr<git_commit> commit;
		git_throw(git_commit_lookup(git_ptr(commit), repo, &oid));
		return commit;
	}

	unique_ptr<git_reference> WorktreeDeployment::reference(git_repository* repo) const
	{
		// Get name
		auto name = getProperty<string>(TEXT("reference"));
		if (name.empty())
			return nullptr;

		// Lookup reference
		unique_ptr<git_reference> reference;
		auto error = git_reference_lookup(git_ptr(reference), repo, name.c_str());
		if (error == GIT_ENOTFOUND)
			return nullptr;
		git_throw(error);

		return reference;
	}

	void WorktreeDeployment::setAction(Action action) const
	{
		jade_throw(setProperty(TEXT("action"), (BYTE)action));
	}

	void WorktreeDeployment::setCommit(const unique_ptr<git_commit>& commit) const
	{
		setProperty(TEXT("commit"), git_commit_id(commit.get()));
	}
	
	void WorktreeDeployment::setReference(const git_reference* ref) const
	{
		setProperty(TEXT("reference"), string(git_reference_name(ref)));
	}

	void WorktreeDeployment::setWorktree(const WorktreeData& worktree) const
	{
		jade_throw(setProperty(TEXT("worktree"), worktree));
	}

	void WorktreeDeployment::executeExit() const
	{
		GitDeployment::executeExit();

		auto worktree = this->worktree();
		worktree.getState().deployed(worktree, *this);
	}

	WorktreeData WorktreeDeployment::worktree() const
	{
		return getProperty<WorktreeData>(TEXT("worktree"));
	}
}