#include "SchemaFile.h"
#include <jade/Loader.h>
#include <schema/ObjectRegistration.h>
#include <Environment.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitSchemaFile> registration(TEXT("GitSchemaFile"));

	GitSchemaFile::GitSchemaFile(const GitDeploymentCommand& parent, bool latest, Type type, const std::string& schema) : GitDeploymentFile(parent, registration)
	{
		jade_throw(setProperty(TEXT("latest"), latest));
		jade_throw(setProperty(TEXT("type"), (BYTE)type));
		setProperty(TEXT("schema"), schema);
	}

	GitSchemaFile::Type GitSchemaFile::getType() const
	{
		BYTE type = 0;
		getProperty(TEXT("type"), type);
		return (Type)type;
	}

	const char* GitSchemaFile::getExtension() const
	{
		switch (getType())
		{
		case Type::SCM:
			return "scm";
		case Type::DDX:
			return "ddx";
		case Type::JCF:
			return "jcf";
		default:
			throw jadegit_exception("Unhandled deployment schema file type");
		}
	}

	bool GitSchemaFile::isLatest() const
	{
		bool latest = false;
		getProperty(TEXT("latest"), latest);
		return latest;
	}

	void GitSchemaFile::abort(Transaction& transaction, Loader& loader, bool& done) const
	{
		// Cannot abort current schema version loads
		if (isCompleted() && !isLatest())
		{
			done = true;
			return;
		}
		
		// TODO: Remember/verify schemas that are being unversioned?

		// Commit current progress
		transaction.commit();

		// Unversion
		loader.unversion();

		// Restart transaction
		transaction.begin();

		// Nothing further to be aborted (unversion currently applies to all schemas)
		done = true;
	}

	void GitSchemaFile::execute(Transaction& transaction, Loader& loader) const
	{
		// Commit current progress
		transaction.commit();

		// Get type
		auto type = getType();

		// Retrieve schema name
		string schema = (type == Type::JCF ? "Commands" : getProperty<string>(TEXT("schema")));

		// Extract to temporary file
		auto path = makeTempDirectory() / (schema + "." + getExtension());
		ExtractToFile(path.string());

		filesystem::path dataPath;

		// Load file
		switch (type)
		{
		case Type::SCM:
		{
			GitSchemaFile dataFile;
			getProperty(TEXT("data"), dataFile);

			if (dataFile.isNull())
				loader.loadSchemaFile(path, filesystem::path(), isLatest());
			else
			{
				// Extract data file
				dataPath = (makeTempDirectory() / (schema + "." + dataFile.getExtension())).string();
				dataFile.ExtractToFile(dataPath.string());

				// Load schema & data file together
				loader.loadSchemaFile(path, dataPath, isLatest());
			}
			break;
		}
		case Type::DDX:
			loader.loadSchemaFile(filesystem::path(), path, isLatest());
			break;

		case Type::JCF:
			loader.loadCommandFile(path, isLatest());
			break;

		default:
			throw jadegit_exception("Unhandled deployment schema file type");
		}

		// Mark as completed immediately, and repository as unstable if current schema load
		transaction.begin();
		markAsCompleted();
		if (!isLatest())
			markRepositoryAsUnstable();
		transaction.commit();

		// Clean-up file
		filesystem::remove(path);

		// Clean-up data file
		if (!dataPath.empty())
			filesystem::remove(dataPath);
			
		// Restart transaction
		transaction.begin();
	}
}