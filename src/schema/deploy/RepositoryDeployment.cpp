#include "RepositoryDeployment.h"
#include <schema/data/Repository.h>
#include <schema/ObjectRegistration.h>

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitRepositoryDeployment> registration(TEXT("RepositoryDeployment"));

	GitRepositoryDeployment::GitRepositoryDeployment() : GitDeployment(registration) {}

	void GitRepositoryDeployment::SetAction(Action action)
	{
		jade_throw(setProperty(TEXT("action"), (BYTE)action));
	}

	void GitRepositoryDeployment::SetRepository(const RepositoryData& repo)
	{
		jade_throw(setProperty(TEXT("repo"), repo));
	}

	void GitRepositoryDeployment::executeExit() const
	{
		GitDeployment::executeExit();

		RepositoryData repo;
		getProperty(TEXT("repo"), repo);

		Byte action;
		getProperty(TEXT("action"), &action);

		switch ((Action)action)
		{
		case Load:
			executeLoad(repo);
			break;
		case Reset:
			executeReset(repo);
			break;
		case Unload:
			executeUnload(repo);
			break;
		default:
			throw jadegit_exception("Invalid action");
		}
	}

	void GitRepositoryDeployment::executeLoad(RepositoryData& repo) const
	{
		// Set active state
		repo.SetState(repo.GetState() | RepositoryData::Active);

		// Reset
		executeReset(repo);
	}

	void GitRepositoryDeployment::executeReset(RepositoryData& repo) const
	{
		// Reset unstable state
		repo.SetState(repo.GetState() & ~RepositoryData::Unstable);

		// TODO: Purge changes and/or entities?
	}

	void GitRepositoryDeployment::executeUnload(RepositoryData& repo) const
	{
		// Reset active state
		repo.SetState(repo.GetState() & ~RepositoryData::Active);

		// TODO: Purge changes and/or schemas?
	}
}