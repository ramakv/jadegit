#include "DeploymentScript.h"
#include <jade/AppContext.h>
#include <jade/Transient.h>
#include <schema/ObjectRegistration.h>
#include <future>
#include <thread>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitDeploymentScript> registration(TEXT("GitDeploymentScript"));

	GitDeploymentScript::GitDeploymentScript(const GitDeploymentCommand& parent, const Build::Script& script) : GitDeploymentCommand(parent, registration)
	{
		setProperty(TEXT("schema"), script.schema);
		setProperty(TEXT("app_"), script.app);
		setProperty(TEXT("executeSchema"), script.executeSchema);
		setProperty(TEXT("executeClass"), script.executeClass);
		setProperty(TEXT("executeMethod"), script.executeMethod);
		setProperty(TEXT("executeParam"), script.executeParam);
		jade_throw(setProperty(TEXT("executeTransient"), script.executeTransient));
		jade_throw(setProperty(TEXT("executeTypeMethod"), script.executeTypeMethod));
	}

	void GitDeploymentScript::execute(Transaction& transaction, Loader& loader) const
	{
		// Commit current progress
		transaction.commit();

		DskHandle node = { 0 };
		jade_throw(jomGetNodeContextHandle(nullptr, &node));

		auto schema = getProperty<String>(TEXT("schema"));
		auto app = getProperty<String>(TEXT("app_"));
		auto executeSchema = getProperty<String>(TEXT("executeSchema"));
		auto executeClass = getProperty<String>(TEXT("executeClass"));
		auto executeMethod = getProperty<String>(TEXT("executeMethod"));
		auto executeParam = getProperty<String>(TEXT("executeParam"));
		bool executeTransient = getProperty<bool>(TEXT("executeTransient"));
		bool executeTypeMethod = getProperty<bool>(TEXT("executeTypeMethod"));

		// Resolve schema
		DskSchema rootSchema(&RootSchemaOid);
		DskSchema targetSchema;
		jade_throw(rootSchema.findSchema((executeSchema.empty() ? schema : executeSchema).c_str(), targetSchema));

		if (targetSchema.isNull())
			throw jadegit_exception("Invalid schema");

		// Resolve class
		DskClass targetClass;
		jade_throw(targetSchema.getSchemaClass(executeClass.c_str(), targetClass));

		if (targetClass.isNull())
			throw jadegit_exception("Invalid class");

		ClassNumber targetClassNumber = 0;
		jade_throw(targetClass.getNumber(&targetClassNumber));

		// Resolve method
		DskMethod targetMethod;
		jade_throw(targetClass.getMethodInHTree(executeMethod.c_str(), targetMethod));

		if (targetMethod.isNull())
			throw jadegit_exception("Invalid method");

		// Define lamda to execute script
		auto runScript = [&]()
		{
			AppContext appContext(&node, schema.c_str(), app.c_str());

			// Define param
			unique_ptr<DskParamString> param;
			if (!executeParam.empty())
				param = make_unique<DskParamString>(executeParam);

			// Handle invoking type method
			if (executeTypeMethod)
			{
				jade_throw(jomInvokeTypeMethod(nullptr, targetClassNumber, nullptr, &targetMethod.oid, param.get(), nullptr, __LINE__));
				return;
			}
			
			// Handle invoking method on first instance
			if (!executeTransient)
			{
				DskObjectId targetId = { 0 };
				jade_throw(targetClass.firstInstance(&targetId));

				if (!targetId.isNull())
				{
					jade_throw(jomInvokeMethod(nullptr, &targetId, nullptr, &targetMethod.oid, param.get(), nullptr, __LINE__));
					return;
				}
			}

			// Handle invoking method on transient
			Transient<DskObject> target(targetClassNumber);
			jade_throw(jomInvokeMethod(nullptr, &target.oid, nullptr, &targetMethod.oid, param.get(), nullptr, __LINE__));
		};

		// Execute script within another thread
		std::packaged_task<void()> task(runScript);
		std::future<void> f = task.get_future();
		std::thread t(std::move(task));

		while (f.wait_for(std::chrono::seconds(1)) != std::future_status::ready)
		{
			DskParam pInterval;
			paramSetInteger(pInterval);
			jade_throw(DskObject(AppContext::GetApp()).sendMsg(TEXT("doWindowEvents"), &pInterval));
		}

		t.join();
		f.get();

		// Restart transaction
		transaction.begin();
	}
}