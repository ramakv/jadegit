#include "Repository.h"
#include <jadegit/build.h>
#include <Environment.h>

using namespace std;

namespace JadeGit::Schema::Install
{
	void fetch(unique_ptr<git_repository>& repo, unique_ptr<git_commit>& commit)
	{
		// Use temp folder
		auto path = makeTempDirectory("install") / "repo";

		// Try to open existing
		int result = git_repository_open_bare(git_ptr(repo), path.string().c_str());
		if (result == GIT_ENOTFOUND)
		{
			// Clone repository
			git_clone_options opts = GIT_CLONE_OPTIONS_INIT;
			opts.bare = 1;
			opts.fetch_opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;
			git_throw(git_clone(git_ptr(repo), build_repo, path.string().c_str(), &opts));
		}
		else
			git_throw(result);

		// Determine build commit id
		git_oid id = { 0 };
		git_throw(git_oid_fromstrp(&id, build_commit));
		
		// Lookup commit
		result = git_commit_lookup(git_ptr(commit), repo.get(), &id);
		if (result == GIT_ENOTFOUND)
		{
			// Lookup remote
			unique_ptr<git_remote> remote;
			git_throw(git_remote_lookup(git_ptr(remote), repo.get(), "origin"));

			// Define refspec to fetch specific commit
			char* refspec[] = { const_cast<char*>(build_commit) };
			git_strarray refspecs = { refspec, 1 };

			// Fetch commit
			git_fetch_options opts = GIT_FETCH_OPTIONS_INIT;
			opts.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;
			result = git_remote_fetch(remote.get(), &refspecs, &opts, nullptr);
			if (result != GIT_EEOF)
				git_throw(result);
			
			// Lookup commit
			git_throw(git_commit_lookup(git_ptr(commit), repo.get(), &id));
		}
		else
			git_throw(result);
	}
}