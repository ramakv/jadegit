#include "Data.h"
#include <jade/Binary.h>
#include <jadegit/git2.h>
#include <git2/sys/odb_backend.h>
#include <schema/data/Repository.h>
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	ObjectData ObjectData::lookup(const RepositoryData& repo, const git_oid* oid)
	{
		auto objects = repo.getProperty<DskMemberKeyDictionary>(TEXT("objects"));
		ObjectData object;
		char id[GIT_OID_HEXSZ + 1] = "";
		jade_throw(objects.getAtKey(widen(git_oid_tostr(id, GIT_OID_HEXSZ + 1, oid)).c_str(), object));
		return object;
	}

	void ObjectData::write(const RepositoryData& repo, const git_oid* oid, const void* data, size_t len, git_object_t type)
	{
		ObjectData object(repo, oid, data, len, type);
	}

	static GitObjectRegistration<ObjectData> object(TEXT("ObjectData"));
	static GitObjectRegistration<ObjectData> commit(TEXT("CommitData"));
	static GitObjectRegistration<ObjectData> tree(TEXT("TreeData"));

	ClassNumber getDataClass(git_object_t type)
	{
		switch (type)
		{
		case git_object_t::GIT_OBJECT_COMMIT:
			return commit;
		case git_object_t::GIT_OBJECT_TREE:
			return tree;
		default:
			return object;
		}
	}

	ObjectData::ObjectData(const RepositoryData& repo, const git_oid* oid, const void* data, size_t len, git_object_t type) : Object(getDataClass(type))
	{
		jade_throw(createObject());

		char id[GIT_OID_HEXSZ + 1] = "";
		setProperty(TEXT("id"), string(git_oid_tostr(id, GIT_OID_HEXSZ + 1, oid)));
		jade_throw(setProperty(TEXT("type"), type));
		jade_throw(setBinaryProperty(TEXT("data"), (BYTE*)data, len));
		jade_throw(setProperty(TEXT("repo"), repo));
	}

	git_object_t ObjectData::getType() const
	{
		return (git_object_t)getProperty<int>(TEXT("type"));
	}

	git_error_code ObjectData::getData(void **data_p, size_t *len_p, git_odb_backend* _backend) const
	{
		// TODO: Long term, subclassing GitData so we can utilise JadeBytes for large blobs may be more efficient
		// Potentially also hooks for reading data in chunks

		/* Get data from JADE */
		DskParamBinary pData;
		getProperty(TEXT("data"), pData);

		BYTE* buffer = NULL;
		Size len = NULL;
		jade_throw(paramGetBinary(pData, buffer, len));

		/* Allocate libgit2 buffer */
		*len_p = len;
		*data_p = git_odb_backend_data_alloc(_backend, *len_p);
		if (!*data_p)
			return GIT_ERROR;

		/* Copy data into buffer */
		memcpy(*data_p, buffer, len);
		return GIT_OK;
	}

	git_error_code ObjectData::read(void** data_p, size_t* len_p, git_object_t* type_p, git_odb_backend* backend) const
	{
		*type_p = getType();
		return getData(data_p, len_p, backend);
	}
}