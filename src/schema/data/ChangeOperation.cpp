#include "ChangeOperation.h"
#include "ChangeSet.h"
#include "EntitySet.h"
#include <extract/Assembly.h>
#include <extract/EntityFactory.h>
#include <jade/Iterator.h>
#include <Singleton.h>
#include <Platform.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	// Determine version required to delete entity (if change were deployed)
	const AnnotatedVersion& requiredVersionToDelete(const Extract::Entity& entity)
	{
		switch (entity.GetKind())
		{
		case DSKAPPLICATION:
		case DSKLIBRARY:
			return jade2022SP1;
		default:
			return nullVersion;
		}
	}

	// Determine version required to rename entity (if change were deployed)
	const AnnotatedVersion& requiredVersionToRename(const Extract::Entity& entity)
	{
		switch (entity.GetKind())
		{
		case DSKDBFILE:
			return jedi102;
		case DSKCONSTANTCATEGORY:
		case DSKLIBRARY:
			return jedi457;
		case DSKAPPLICATION:
		case DSKJADEPACKAGE:
		case DSKJADEEXPORTEDPACKAGE:
		case DSKJADEIMPORTEDPACKAGE:
			return jade2022SP1;
		default:
			return nullVersion;
		}
	}

	GitChange ChangeOperation::add(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const
	{
		// TODO: Check entity exists in current worktree

		// Register change
		return GitChange(worktree, nullptr, &entity, &current).setOperation(GitChange::Operation::Add);
	}

	GitChange ChangeOperation::update(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const
	{
		// TODO: Check entity exists in current worktree

		// Register change
		return GitChange(worktree, nullptr, &entity, &current).setOperation(GitChange::Operation::Update);
	}

	GitChange ChangeOperation::rename(const WorktreeData& worktree, const GitChange& current, const GitEntity& previous, const GitEntity& latest, const Extract::Entity& actual) const
	{
		// TODO: Check entity exists in current worktree

		// Prevent rename if deploying change isn't supported
		auto requiredVersion = requiredVersionToRename(actual);
		if (jadeVersion < requiredVersion)
			throw jadegit_unsupported_feature("Renaming existing entities of this type", requiredVersion);

		// Setup collection for predecessors
		set<DskObjectId> predecessors;

		// Add current change for previous entity to predecessors
		if (!current.isNull())
			predecessors.insert(current.oid);

		// Add last change to predecessors for latest entity if it was a deletion/rename making this rename possible
		auto latest_change = latest.getChange();
		if (!latest_change.isNull())
		{
			// TODO: Handle case where latest change may be under different branch

			if (latest_change.isPrevious(latest))
				predecessors.insert(latest_change.oid);
			else
				throw jadegit_exception("Unhandled predecessor operation");
		}

		// Register change
		GitChange change(worktree, &previous, &latest, predecessors);
		change.setOperation(GitChange::Operation::Rename);

		// Get previous child changes
		vector<GitChange> changes;
		previous.getChildChanges(worktree, changes);

		// Migrate children & changes if required
		if (!changes.empty())
		{
			// Clone children
			map<DskObjectId, DskObjectId> entities_dir;
			previous.cloneChildren(latest, entities_dir);

			// Clone changes
			map<DskObjectId, DskObjectId> changes_dir;
			latest.cloneChanges(worktree, changes, entities_dir, changes_dir);

			// Finalize migration by transferring the operation
			for (GitChange src : changes)
			{
				GitChange dst(&changes_dir[src.oid]);
				dst.setOperation(src);
				src.setVoid();

				// Make child changes dependent on parent rename
				// NOTE: This includes potentially staged changes to cater for scenario where they're unstaged post-rename
				dst.successorOf(change);
			}
		}

		// Retrieve all dependent entities affected by rename
		set<DskObjectId> dependents;
		actual.dependentsAll(dependents);

		// Register implied update for dependent entities affected by rename
		for (auto& dependent : dependents)
		{
			auto actual = Extract::EntityFactory::Get().Create(dependent);
			if (auto entity = GitEntity::resolve(*actual))
			{
				auto dependent = GitChange::update(*entity).successorOf(change);
				if (!dependent.isAdd())
					dependent.predecessorOf(change);	// Updates must be progressed together
			}
		}
		
		return change;
	}

	GitChange ChangeOperation::dele(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity, const Extract::Entity* actual) const
	{	
		// TODO: Ignore/allow deletion if entity doesn't exist in any active worktree (probably because we couldn't delete automatically when switching branches)

		// TODO: Check entity exists in current worktree

		// Prevent deletion if deploying change isn't supported
		if (actual)
		{
			auto requiredVersion = requiredVersionToDelete(*actual);
			if (jadeVersion < requiredVersion)
				throw jadegit_unsupported_feature("Deleting existing entities of this type", requiredVersion);
		}

		// Void any child changes
		vector<GitChange> changes;
		entity.getChildChanges(worktree, changes);
		for (GitChange change : changes)
			change.setVoid();

		// Register change
		GitChange change(worktree, &entity, nullptr, &current);
		change.setOperation(GitChange::Operation::Delete);

		// Register implied deletion of associates
		if (actual)
		{
			set<DskObjectId> associates;
			actual->getAssociates(associates);

			// Register implied deletion of associate
			for (auto& associate : associates)
			{
				auto actual = Extract::EntityFactory::Get().Create(associate);
				if (auto entity = GitEntity::resolve(*actual))
					GitChange::dele(*entity, *actual).peerOf(change);
			}
		}

		// Register deletion for associates stored just prior to removal
		GitEntity associate;
		GitEntitySet associates(entity);
		Iterator<GitEntity> iter(associates);
		while (iter.next(associate))
		{
			GitChange::dele(associate, nullptr).peerOf(change);
		}

		return change;
	}

	// Preload entities affected
	// Used to avoid resolution errors while staging, where loading a previous entity may be affected by other changes already staged (delete/rename)
	void ChangeOperation::stagePreload(const GitChange& change, Extract::Assembly& assembly, std::set<DskObjectId>& loaded) const
	{
		// Preload predecessors
		GitChange predecessor;
		Iterator<GitChange> predecessors(change, TEXT("predecessors"));
		while (predecessors.next(predecessor))
			predecessor.stagePreload(assembly, loaded);

		// Attempt to load both targets
		// Generally, previous should exist and latest shouldn't (but not always true)
		// If successfully resolved, entities are loaded in full upfront to cater for related entities being renamed
		stagePreload(change.getPrevious(), assembly);
		stagePreload(change.getLatest(), assembly);
	}

	void ChangeOperation::stagePreload(const GitEntity& entity, Extract::Assembly& assembly) const
	{
		if(!entity.isNull())
			if (auto target = entity.GetTarget(assembly))
				target->Load();
	}

	void ChangeOperation::stage(GitChange& change, Extract::Assembly& assembly, GitChangeSet& extract) const
	{
		// Set staged flag
		// NOTE: This is set prior to staging predecessors to support peer relationships
		change.SetStaged(true);

		// Stage predecessors
		GitChange predecessor;
		Iterator<GitChange> predecessors(change, TEXT("predecessors"));
		while (predecessors.next(predecessor))
			predecessor.stage(assembly, extract);
	}

	void ChangeOperation::Compact(GitChange& change) const
	{
		GitEntity entity = change.getPrevious();
		if (entity.isNull())
			entity = change.getLatest();

		bool staged = change.isStaged();

		GitChange predecessor;
		Iterator<GitChange> predecessors(change, TEXT("predecessors"));
		while (predecessors.next(predecessor))
		{
			if (staged != predecessor.isStaged())
				continue;

			if (predecessor.isLatest(entity))
			{
				Compact(predecessor, change, predecessor.getOperation());
				break;	// May have just merged/deleted change supplied
			}
		}
	}

	void ChangeOperation::Merge(GitChange& lhs, GitChange& rhs) const
	{
		lhs.Merge(rhs);
	}

	void ChangeOperation::Merge(GitChange& lhs, GitChange& rhs, GitChange::Operation operation) const
	{
		Merge(lhs, rhs);
		lhs.setOperation(operation);
	}

	class ExtractOperation : public ChangeOperation
	{
	public:
		ExtractOperation(bool deep = false) : ChangeOperation(), deep(deep) {}

		GitChange add(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const override
		{
			// Unexpected scenario
			if (current.isLatest(entity))
				throw jadegit_exception("Unexpected add operation");

			return ChangeOperation::add(worktree, current, entity);
		}

		GitChange update(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const override
		{
			// Unstaged add/update/rename has already been registered
			if (!current.isStaged())
				return current;

			return ChangeOperation::update(worktree, current, entity);
		}

	protected:
		void stage(GitChange& change, Extract::Assembly& assembly, GitChangeSet& extract) const override
		{
			ChangeOperation::stage(change, assembly, extract);

			// Change needs to be extracted
			extract.add(&change);
		}

	private:
		bool deep = false;

		void stageExtract(GitChange& change, Extract::Assembly& assembly) const override final
		{
			GitEntity latest = change.getLatest();
			assert(!latest.isNull());

			// Handle extract, assuming we're still able to find source entity to extract (currently ignored if not)
			// NOTE: Deep extract is performed for new entities added without needing to track all children
			if (std::unique_ptr<Extract::Entity> source = latest.GetSource(false))
				source->Extract(assembly, deep);
		}
	};

	class AddOperation : public ExtractOperation, public Singleton<AddOperation>
	{
	public:
		AddOperation() : ExtractOperation(true) {}

		GitChange add(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const override
		{
			// Ignore case where the 'A' operation is repeated/duplicate, which the interface mapper is known to do
			// NOTE: Schema creation also invokes the 'A' operation multiple times, with the second call invoked in a new
			// transaction, hence we can't check if change is exclusively locked to confirm it's the same transaction
			
			// Latest entity for current change record should match entity supplied
			assert(current.isLatest(entity));
			return current;
		}

		GitChange rename(const WorktreeData& worktree, const GitChange& current, const GitEntity& previous, const GitEntity& latest, const Extract::Entity& actual) const override
		{
			// Replace previous add operation if we can
			if (!current.isStaged())
			{
				current.setVoid();
				return ChangeOperation::add(worktree, GitChange(), latest);
			}

			return ExtractOperation::rename(worktree, current, previous, latest, actual);
		}

		GitChange dele(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity, const Extract::Entity* actual) const override
		{
			// Void creation if we can
			if (!current.isStaged())
			{
				current.setVoid();
				return GitChange();
			}

			return ExtractOperation::dele(worktree, current, entity, actual);
		}

	protected:
		void Compact(GitChange& lhs, GitChange& rhs, const ChangeOperation& operation) const override
		{
			operation.Combine(lhs, rhs, *this);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const AddOperation& operation) const override
		{
			throw jadegit_exception("Unexpected operation sequence (add->add)");
		}

		void Combine(GitChange& lhs, GitChange& rhs, const UpdateOperation& operation) const override
		{
			Merge(lhs, rhs);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const RenameOperation& operation) const override
		{
			lhs.setLatest(rhs.getLatest());
			Merge(lhs, rhs);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const DeleteOperation& operation) const override
		{
			lhs.deleteObject();
			rhs.deleteObject();
		}
	};

	class UpdateOperation : public ExtractOperation, public Singleton<UpdateOperation>
	{
	public:
		GitChange rename(const WorktreeData& worktree, const GitChange& current, const GitEntity& previous, const GitEntity& latest, const Extract::Entity& actual) const override
		{
			// Void update if we can
			if (!current.isStaged())
			{
				current.setVoid();
				return ExtractOperation::rename(worktree, GitChange(), previous, latest, actual);
			}

			return ExtractOperation::rename(worktree, current, previous, latest, actual);
		}

		GitChange dele(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity, const Extract::Entity* actual) const override
		{
			// Void update if we can
			if (!current.isStaged())
			{
				current.setVoid();
				return ExtractOperation::dele(worktree, GitChange(), entity, actual);
			}

			return ExtractOperation::dele(worktree, current, entity, actual);
		}

	protected:
		void Compact(GitChange& lhs, GitChange& rhs, const ChangeOperation& operation) const override
		{
			operation.Combine(lhs, rhs, *this);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const AddOperation& operation) const override
		{
			throw jadegit_exception("Unexpected operation sequence (update->add)");
		}

		void Combine(GitChange& lhs, GitChange& rhs, const UpdateOperation& operation) const override
		{
			Merge(lhs, rhs);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const RenameOperation& operation) const override
		{
			lhs.setPrevious(lhs.getLatest());
			lhs.setLatest(rhs.getLatest());
			Merge(lhs, rhs, GitChange::Operation::Rename);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const DeleteOperation& operation) const override
		{
			lhs.deleteObject();
		}
	};

	class RenameOperation : public ExtractOperation, public Singleton<RenameOperation>
	{
	public:
		GitChange update(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const override
		{
			// Redirect to add operation on assumption previous entity has been re-added outside of source control
			if (!current.isLatest(entity))
				return ExtractOperation::add(worktree, current, entity);

			return ExtractOperation::update(worktree, current, entity);
		}

		GitChange rename(const WorktreeData& worktree, const GitChange& current, const GitEntity& previous, const GitEntity& latest, const Extract::Entity& actual) const override
		{
			// Ignore duplicate rename operation, which is known to occur when form class is renamed implicitly
			if (current.isPrevious(previous) && current.isLatest(latest))
				return current;

			// Ensure previous is latest for current rename
			if (!current.isLatest(previous))
				throw jadegit_exception("Invalid rename operation");

			// Replace existing rename if we can
			if (!current.isStaged())
			{
				// Revert to update if current rename has been reversed
				if (current.isPrevious(latest))
				{
					current.setVoid();
					return ChangeOperation::update(worktree, GitChange(), latest);
				}
				else
				{
					// TODO: Further work needed to determine if current rename can be replaced,
					// or if it's still needed as an interim step during a name swap
				}
			}

			return ExtractOperation::rename(worktree, current, previous, latest, actual);
		}

		GitChange dele(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity, const Extract::Entity* actual) const override
		{
			// Ensure entity being deleted is latest
			if (!current.isLatest(entity))
				throw jadegit_exception("Unexpected delete operation");

			// Replace existing rename if we can
			if (!current.isStaged())
			{
				current.setVoid();
				return ExtractOperation::dele(worktree, GitChange(), current.getPrevious(), actual);
			}

			return ExtractOperation::dele(worktree, current, entity, actual);
		}

	protected:
		void stage(GitChange& change, Extract::Assembly& assembly, GitChangeSet& extract) const override
		{
			ExtractOperation::stage(change, assembly, extract);

			// Dereference previous/latest entities
			GitEntity previous = change.getPrevious();
			GitEntity latest = change.getLatest();
			assert(!previous.isNull());
			assert(!latest.isNull());

			// Resolve previous target
			Data::Entity* target = previous.GetTarget(assembly);
			assert(target);	// TODO: Log warning?

			// Rename to become latest
			if (target)
				target->Rename(latest.GetName().c_str());
		}

		void Compact(GitChange& lhs, GitChange& rhs, const ChangeOperation& operation) const override
		{
			operation.Combine(lhs, rhs, *this);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const AddOperation& operation) const override
		{
			throw jadegit_exception("Unexpected operation sequence (rename->add)");
		}

		void Combine(GitChange& lhs, GitChange& rhs, const UpdateOperation& operation) const override
		{
			Merge(lhs, rhs);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const RenameOperation& operation) const override
		{
			// Revert to update operation if entity has been renamed back to original
			if (lhs.isPrevious(rhs.getLatest()))
			{
				lhs.setLatest(lhs.getPrevious());
				lhs.setPrevious(GitEntity());
				Merge(lhs, rhs, GitChange::Operation::Update);
			}
			else
			{
				// TODO: Further work required to determine when rename operations can be merged without
				// causing a rename deadlock which prevents entities swapping names
			}
		}

		void Combine(GitChange& lhs, GitChange& rhs, const DeleteOperation& operation) const override
		{
			lhs.setLatest(GitEntity());
			Merge(lhs, rhs, GitChange::Operation::Delete);
		}
	};

	class DeleteOperation : public ChangeOperation, public Singleton<DeleteOperation>
	{
	public:
		GitChange update(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity) const override
		{
			// Redirect to add operation on assumption previous entity has been re-added outside of source control
			return ChangeOperation::add(worktree, current, entity);
		}

		GitChange rename(const WorktreeData& worktree, const GitChange& current, const GitEntity& previous, const GitEntity& latest, const Extract::Entity& actual) const override
		{
			// Should never be able to rename something after it's been deleted!
			throw jadegit_exception("Cannot rename deleted entity");
		}

		GitChange dele(const WorktreeData& worktree, const GitChange& current, const GitEntity& entity, const Extract::Entity* actual) const override
		{
			// Ignore case where the delete operation is repeated, which is known to occur when deleting a group of associates,
			// during which associate deletions are registered recursively, hence deletion for first entity deleted occurs twice.

			// Previous entity for current change record should match entity supplied
			assert(current.isPrevious(entity));
			return current;
		}

	private:
		void stage(GitChange& change, Extract::Assembly& assembly, GitChangeSet& extract) const override
		{
			ChangeOperation::stage(change, assembly, extract);

			// Dereference previous entity
			GitEntity previous = change.getPrevious();
			assert(!previous.isNull());

			// Resolve previous target
			Data::Entity* target = previous.GetTarget(assembly);
			assert(target);	// TODO: Log warning?

			// Delete
			if (target)
				target->Delete();
		}

		void stageExtract(GitChange& change, Extract::Assembly& assembly) const override final
		{
			throw jadegit_exception("Invalid extract operation");
		}

		void Compact(GitChange& lhs, GitChange& rhs, const ChangeOperation& operation) const override
		{
			operation.Combine(lhs, rhs, *this);
		}

		void Combine(GitChange& lhs, GitChange& rhs, const AddOperation& operation) const override
		{
			throw jadegit_exception("Unexpected operation sequence (delete->add)");
		}

		void Combine(GitChange& lhs, GitChange& rhs, const UpdateOperation& operation) const override
		{
			throw jadegit_exception("Unexpected operation sequence (delete->update)");
		}

		void Combine(GitChange& lhs, GitChange& rhs, const RenameOperation& operation) const override
		{
			throw jadegit_exception("Unexpected operation sequence (delete->rename)");
		}

		void Combine(GitChange& lhs, GitChange& rhs, const DeleteOperation& operation) const override
		{
			throw jadegit_exception("Unexpected operation sequence (delete->delete)");
		}
	};

	class VoidOperation : public ChangeOperation, public Singleton<VoidOperation>
	{
	protected:
		void stagePreload(const GitChange& change, Extract::Assembly& assembly, std::set<DskObjectId>& loaded) const override {}
		void stage(GitChange& change, Extract::Assembly& assembly, GitChangeSet& extract) const override {}
		void stageExtract(GitChange& change, Extract::Assembly& assembly) const override {}
		void Compact(GitChange& change) const override {}
		void Compact(GitChange& lhs, GitChange& rhs, const ChangeOperation& operation) const override {};
		void Combine(GitChange& lhs, GitChange& rhs, const AddOperation& operation) const override {};
		void Combine(GitChange& lhs, GitChange& rhs, const UpdateOperation& operation) const override {};
		void Combine(GitChange& lhs, GitChange& rhs, const RenameOperation& operation) const override {};
		void Combine(GitChange& lhs, GitChange& rhs, const DeleteOperation& operation) const override {};
	};

	const ChangeOperation& ChangeOperation::Get(GitChange::Operation operation)
	{
		switch (operation)
		{
		case GitChange::Operation::Void:
			return *VoidOperation::Instance();
		case GitChange::Operation::Add:
			return *AddOperation::Instance();
		case GitChange::Operation::Delete:
			return *DeleteOperation::Instance();
		case GitChange::Operation::Rename:
			return *RenameOperation::Instance();
		case GitChange::Operation::Update:
			return *UpdateOperation::Instance();
		}

		throw jadegit_exception("Invalid operation");
	}
}