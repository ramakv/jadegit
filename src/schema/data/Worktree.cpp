#include "Worktree.h"
#include "ChangeSet.h"
#include "Index.h"
#include "Repository.h"
#include "WorktreeState.h"
#include <jade/Transaction.h>
#include <registry/Commit.h>
#include <schema/Branch.h>
#include <schema/Exception.h>
#include <schema/ObjectRegistration.h>
#include <schema/Repository.h>
#include <schema/Task.h>
#include <schema/config/Config.h>
#include <schema/data/refdb/NodeDict.h>
#include <schema/index/Index.h>
#include <vfs/GitFileSystem.h>

#include <Epilog.h>
#include <git2/sys/repository.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<WorktreeData> registration(TEXT("WorktreeData"));

	WorktreeData::WorktreeData() : Object(registration) {}

	WorktreeData::WorktreeData(const RepositoryData& repo) : WorktreeData(repo, nullptr) {}

	WorktreeData::WorktreeData(const RepositoryData& repo, const Registry::CommitT& commit) : WorktreeData(repo, &commit) {}

	WorktreeData::WorktreeData(const RepositoryData& repo, const Registry::CommitT* commit) : WorktreeData()
	{
		// Create worktree
		createObject();
		setProperty(TEXT("repo"), repo);
		setProperty(TEXT("state"), (Byte)State::Active);

		// Initialise HEAD
		ReferenceData head;
		head.createObject();
		head.setProperty(TEXT("name"), string("HEAD"));
		head.setProperty(TEXT("parent"), *this);

		// Put HEAD into detached state using commit supplied, or use initial/default branch otherwise
		if (commit)
		{
			ostringstream commit_id;
			commit_id << *commit;

			head.setProperty(TEXT("type"), GIT_REFERENCE_DIRECT);
			head.setProperty(TEXT("target"), commit_id.str());
		}
		else
		{
			Transient<Repository> repo = open();
			Transient<Config> config = repo.config();

			head.setProperty(TEXT("type"), GIT_REFERENCE_SYMBOLIC);
			head.setProperty(TEXT("target"), "refs/heads/" + config.default_branch());
		}
	}

	void WorktreeData::abort(const Repository& repo) const
	{
		verify(repo);
		return getState().abort(*this, repo);
	}

	void WorktreeData::addSelf(git_repository* repo, GitFileSystem& fs) const
	{
		// Add unmodified commit
		if (!isModified())
			return fs.add(refs().head<git_commit>(repo));

		// Get repository index
		unique_ptr<git_index> repo_index;
		git_throw(git_repository_index(git_ptr(repo_index), repo));

		// Open worktree index
		unique_ptr<git_index> index = this->index().open();

		// Swap repo index temporarily
		git_throw(git_repository_set_index(repo, index.get()));
		Epilog epilog([&]() { git_repository_set_index(repo, repo_index.get()); });

		// Stage all changes transiently
		changes().stage(GitFileSystem(repo, index.get()), true);

		// Write index tree
		// TODO: Make this temporary using memcached repo so interim changes aren't persisted unncessarily
		Transaction transaction;
		git_oid tree_id;
		git_throw(git_index_write_tree_to(&tree_id, index.get(), repo));
		transaction.commit();

		// Add modified index
		fs.add("<uncommitted>", tree_id, refs().head<git_commit>(repo));
	}

	GitChangeSet WorktreeData::changes() const
	{
		return getProperty<GitChangeSet>(TEXT("changes"));
	}

	void WorktreeData::commit(const Repository& repo, const std::string& message) const
	{
		verify(repo);
		return getState().commit(*this, repo, message);
	}

	bool WorktreeData::continue_(const Repository& repo, const string& message, IProgress* progress) const
	{
		verify(repo);
		return getState().continue_(*this, repo, message, progress);
	}

	void WorktreeData::GetRepository(RepositoryData& repo) const
	{
		repo = getProperty<RepositoryData>(TEXT("repo"));
	}

	const WorktreeState& WorktreeData::getState() const
	{
		return WorktreeState::get(static_cast<State>(getProperty<Byte>(TEXT("state"))));
	}

	IndexData WorktreeData::index() const
	{
		return getProperty<IndexData>(TEXT("index"));
	}

	bool WorktreeData::isActive() const
	{
		return getState().isActive();
	}

	bool WorktreeData::isModified() const
	{
		GitChange change;
		Iterator<GitChange> iter(*this, TEXT("changes"));
		while (iter.next(change))
		{
			if (!change.isVoid())
				return true;
		}

		return false;
	}

	bool WorktreeData::isStaged() const
	{
		GitChange change;
		Iterator<GitChange> iter(*this, TEXT("changes"));
		while (iter.next(change))
		{
			if (change.isStaged())
				return true;
		}

		return false;
	}

	bool WorktreeData::load(IProgress* progress) const
	{
		return getState().load(*this, progress);
	}

	bool WorktreeData::merge(const Repository& repo, const git_annotated_commit* theirs, IProgress* progress) const
	{
		verify(repo);
		return getState().merge(*this, repo, theirs, progress);
	}

	string WorktreeData::message() const
	{
		return getProperty<string>(TEXT("message"));
	}

	Repository WorktreeData::open() const
	{
		Repository repo(RepositoryData(*this).open(*this));
		jade_throw(repo.setProperty(TEXT("data"), getProperty<DskObjectId>(TEXT("repo"))));
		jade_throw(repo.setProperty(TEXT("worktree"), *this));
		return repo;
	}

	ReferenceNodeDict WorktreeData::refs() const
	{
		return ReferenceNodeDict(*this, TEXT("references"));
	}

	bool WorktreeData::reset(const Repository& repo, const git_commit* commit, bool hard, IProgress* progress) const
	{
		verify(repo);
		return getState().reset(*this, repo, commit, hard, progress);
	}

	void WorktreeData::setMessage(const string& message) const
	{
		setProperty(TEXT("message"), message);
	}

	void WorktreeData::setState(State value) const
	{
		jade_throw(setProperty(TEXT("state"), (Byte)value));
	}

	void WorktreeData::stage(const GitChangeSet& changes) const
	{
		// Open repository
		Transient<Repository> repo = open();

		// Open index
		auto index = repo.index().read(true);

		// Start transaction
		Transaction transaction;

		// Stage changes
		changes.stage(GitFileSystem(repo, index));

		// Save index changes
		index.write();

		// Success
		transaction.commit();
	}

	git_repository_state_t WorktreeData::state() const
	{
		return getState().state(*this);
	}

	bool WorktreeData::switch_(const LocalBranch& branch, IProgress* progress) const
	{
		return getState().switch_(*this, branch, progress);
	}

	bool WorktreeData::unload(IProgress* progress) const
	{
		return getState().unload(*this, progress);
	}

	void WorktreeData::verify(const Repository& repo) const
	{
		assert(oid == repo.getProperty<DskObjectId>(TEXT("worktree")));
	}

	int JOMAPI jadegit_worktree_load(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		WorktreeData worktree(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress) { return worktree.load(progress); });
	}

	int JOMAPI jadegit_worktree_open(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetOid(*pReturn, WorktreeData(pBuffer).open().oid);
			});
	}

	int JOMAPI jadegit_worktree_stage(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				GitChangeSet changes;
				JADE_RETURN(paramGetOid(*pParams, changes.oid));

				WorktreeData(pBuffer).stage(changes);
				return J_OK;
			});
	}

	int JOMAPI jadegit_worktree_switch(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		LocalBranch branch;
		JADE_RETURN(paramGetOid(*pParams, branch.oid));

		WorktreeData worktree(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress) { return worktree.switch_(branch, progress); });
	}

	int JOMAPI jadegit_worktree_unload(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		WorktreeData worktree(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress) { return worktree.unload(progress); });
	}
}