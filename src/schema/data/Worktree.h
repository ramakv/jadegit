#pragma once
#include <schema/Object.h>
#include <jadegit/git2.h>
#include "RepositoryData.h"

namespace JadeGit
{
	class GitFileSystem;
	class IProgress;
}

namespace JadeGit::Registry
{
	class CommitT;
}

namespace JadeGit::Schema
{
	// TODO: Need to rename
	class GitChangeSet;

	class Branch;
	class IndexData;
	class LocalBranch;
	class ReferenceNodeDict;
	class Remote;
	class Repository;

	class WorktreeData : public Object, public IGitRepositoryData
	{
	public:
		enum class State : Byte
		{
			Inactive = 0,
			Active = 1,
			Merge = 2
		};

		using Object::Object;
		WorktreeData();
		WorktreeData(const RepositoryData& repo);
		WorktreeData(const RepositoryData& repo, const Registry::CommitT& commit);
		
		bool isActive() const;
		bool isModified() const;
		bool isStaged() const;

		GitChangeSet changes() const;
		IndexData index() const;
		ReferenceNodeDict refs() const;

		Repository open() const;

		// Operations that may be invoked
		void commit(const Repository& repo, const std::string& message) const;
		bool load(IProgress* progress) const;
		bool merge(const Repository& repo, const git_annotated_commit* theirs, IProgress* progress) const;
		bool reset(const Repository& repo, const git_commit* commit, bool hard, IProgress* progress) const;
		void stage(const GitChangeSet& changes) const;
		git_repository_state_t state() const;
		bool switch_(const LocalBranch& branch, IProgress* progress) const;
		bool unload(IProgress* progress) const;

		// Handle aborting/continuing current operation
		void abort(const Repository& repo) const;
		bool continue_(const Repository& repo, const std::string& message, IProgress* progress) const;
		
		std::string message() const;
		void setMessage(const std::string& message) const;

		void addSelf(git_repository* repo, GitFileSystem& fs) const;

	protected:
		void GetRepository(RepositoryData& repo) const final;

	private:
		friend class GitChange;
		friend class WorktreeDeployment;
		friend class WorktreeState;

		WorktreeData(const RepositoryData& repo, const Registry::CommitT* commit);

		const WorktreeState& getState() const;
		void setState(State value) const;

		void verify(const Repository& repo) const;
	};
}