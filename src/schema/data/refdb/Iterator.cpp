#include "Iterator.h"
#include "Directory.h"

namespace JadeGit::Schema::Backend
{
	bool jadegit_refdb_iterator::next(ReferenceData& reference)
	{
		while (!iters.empty())
		{
			ReferenceNode member;
			if (iters.top().next(member))
			{
				if (member.isDirectory())
				{
					iters.emplace(ReferenceDirectory(member.oid).children());
				}
				else
				{
					reference.oid = member.oid;
					if (reference.valid())
						return true;
				}
			}
			else
			{
				iters.pop();
			}
		}

		return false;
	}

	template <typename T>
	static int wrapper(git_reference_iterator* iter, T func)
	{
		try
		{
			return func(*static_cast<jadegit_refdb_iterator*>(iter));
		}
		catch (std::runtime_error& e)
		{
			git_error_set_str(GIT_ERROR_REFERENCE, e.what());
		}

		return GIT_ERROR;
	}

	int jadegit_refdb_iterator__next(git_reference** ref, git_reference_iterator* iter)
	{
		return wrapper(iter, [&](jadegit_refdb_iterator& iter)
			{
				ReferenceData reference;
				if (!iter.next(reference))
					return GIT_ITEROVER;

				reference.read(ref);
				return GIT_OK;
			});
	}

	int jadegit_refdb_iterator__next_name(const char** ref_name, git_reference_iterator* iter)
	{
		return wrapper(iter, [&](jadegit_refdb_iterator& iter)
			{
				ReferenceData reference;
				if (!iter.next(reference))
					return GIT_ITEROVER;

				*ref_name = _strdup(reference.path().c_str());
				return GIT_OK;
			});
	}

	void jadegit_refdb_iterator__free(git_reference_iterator* iter)
	{		
		delete static_cast<jadegit_refdb_iterator*>(iter);
	}

	jadegit_refdb_iterator::jadegit_refdb_iterator(const jadegit_refdb_backend& backend, const char* glob)
	{
		git_reference_iterator::next = &jadegit_refdb_iterator__next;
		git_reference_iterator::next_name = &jadegit_refdb_iterator__next_name;
		git_reference_iterator::free = &jadegit_refdb_iterator__free;

		// TODO: Implement glob pattern matching
		if (glob && std::string(glob) != "refs/*")
			throw jadegit_unimplemented_feature();

		auto refs = backend.repo.refs().getAtKey<ReferenceDirectory>("refs");
		if (!refs.isNull())
			iters.emplace(refs.children());
	}
}