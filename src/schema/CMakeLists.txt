# Suppress building shared schema library if not required
if(NOT WITH_SCHEMA)
	target_compile_definitions(jadegit PUBLIC JADEGIT_STATIC)
	return()
endif()

# Add schema functionality to common object library
target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Blob.cpp
	${CMAKE_CURRENT_LIST_DIR}/Branch.cpp
	${CMAKE_CURRENT_LIST_DIR}/BranchIterator.cpp
	${CMAKE_CURRENT_LIST_DIR}/Commit.cpp
	${CMAKE_CURRENT_LIST_DIR}/Committish.cpp
	${CMAKE_CURRENT_LIST_DIR}/Exception.cpp
	${CMAKE_CURRENT_LIST_DIR}/Main.cpp
	${CMAKE_CURRENT_LIST_DIR}/MergeFileResult.cpp
	${CMAKE_CURRENT_LIST_DIR}/Object.cpp
	${CMAKE_CURRENT_LIST_DIR}/ObjectFactory.cpp
	${CMAKE_CURRENT_LIST_DIR}/PatchBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/Progress.cpp
	${CMAKE_CURRENT_LIST_DIR}/Reference.cpp
	${CMAKE_CURRENT_LIST_DIR}/ReferenceIterator.cpp
	${CMAKE_CURRENT_LIST_DIR}/Remote.cpp
	${CMAKE_CURRENT_LIST_DIR}/RemoteCallbacks.cpp
	${CMAKE_CURRENT_LIST_DIR}/RemoteCallbacksPayload.cpp
	${CMAKE_CURRENT_LIST_DIR}/Repository.cpp
	${CMAKE_CURRENT_LIST_DIR}/RevisionWalker.cpp
	${CMAKE_CURRENT_LIST_DIR}/Session.cpp
	${CMAKE_CURRENT_LIST_DIR}/Signature.cpp
	${CMAKE_CURRENT_LIST_DIR}/Task.cpp
	${CMAKE_CURRENT_LIST_DIR}/Tree.cpp
	${CMAKE_CURRENT_LIST_DIR}/Workers.cpp
)
include(${CMAKE_CURRENT_LIST_DIR}/config/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/data/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/deploy/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/diff/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/index/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/install/CMakeLists.txt)

# Setup schema library
add_library(jadegitscm SHARED ${jadegit_SOURCE_DIR}/src/jadegit.rc)
target_compile_definitions(jadegitscm PRIVATE JADEGIT_FILEDESC="jadegit schema library" JADEGIT_FILENAME="jadegitscm.dll" GIT_VERSION="${GIT_VERSION}")
target_compile_definitions(jadegitscm PRIVATE jadegit_EXPORTS)
target_include_directories(jadegitscm PRIVATE ${jadegit_SOURCE_DIR}/src)
target_link_libraries(jadegitscm PUBLIC jadegit)

install(TARGETS jadegitscm RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
install(FILES $<TARGET_PDB_FILE:jadegitscm> DESTINATION ${CMAKE_INSTALL_BINDIR} OPTIONAL)

# Add schema library specific source files
target_sources(jadegitscm PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/jadegitscm.def
)