#include "Commit.h"
#include "ObjectRegistration.h"
#include "Task.h"
#include <deploy/build/Director.h>
#include <deploy/build/MultiExtractBuilder.h>
#include <jadegit/vfs/FileSignature.h>
#include <jadegit/vfs/NativeFileSystem.h>

using namespace std;

namespace JadeGit::Schema
{
	class PatchBuilder : public Object
	{
	public:
		using Object::Object;

		bool build(const Commit& previous, const Commit& latest, IProgress* progress = nullptr) const
		{
			// Setup output filesystem
			NativeFileSystem fs(getProperty<string>(TEXT("path")), true);

			// Setup patch deployment builder
			Deploy::MultiExtractBuilder builder(fs, "Patch", true);

			// Setup deployment director
			Deploy::Director director(*static_cast<git_repository*>(latest.repo()), builder, progress);

			// Setup signature with username/patch number to use
			FileSignature signature(getProperty<string>(TEXT("username")), getProperty<int>(TEXT("number")));

			// Build patch for difference between commits
			return director.build(previous, latest, signature.empty() ? nullptr : &signature);
		}
	};

	int JOMAPI jadegit_patch_build(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		DskParam* pOld = nullptr;
		DskParam* pNew = nullptr;
		JADE_RETURN(paramGetParameter(*pParams, 1, pOld));
		JADE_RETURN(paramGetParameter(*pParams, 2, pNew));

		Commit previous;
		JADE_RETURN(paramGetOid(*pOld, previous.oid));

		Commit latest;
		JADE_RETURN(paramGetOid(*pNew, latest.oid));

		PatchBuilder builder(pBuffer);
		return Task::make(pReturn, [=](IProgress* progress) { return builder.build(previous, latest, progress); });
	}
}