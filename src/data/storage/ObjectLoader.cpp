#include "ObjectLoader.h"
#include "ObjectFileFormat.h"
#include "ObjectFileStorage.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/vfs/FileSystem.h>
#include <Epilog.h>

using namespace std;
using namespace std::filesystem;

namespace JadeGit::Data
{
	ObjectLoader::ObjectLoader(const ObjectFileStorage& storage) : storage(storage)
	{
		// Set dogfood flag if loading schemas from own repository
		dogfood = storage.fs.open(path("schemas/JadeGitSchema").replace_extension(storage.format->extension())).exists();
	}

	Entity* ObjectLoader::load(std::filesystem::path path, bool shallow)
	{
		path.replace_extension(storage.format->extension());

		// Handle deferred load
		if (!shallow && !deferred.empty())
		{
			auto it = deferred.find(path);
			if (it != deferred.end())
				return it->second.get();
		}

		// Throw exception for requests to load files outside scope of assembly
		if (!path.is_relative())
			throw jadegit_exception("Invalid path");

		File file = storage.fs.open(path);
		if (file.exists())
			return load(file, shallow, nullptr);

		return nullptr;
	}

	bool ObjectLoader::load(IProgress* progress)
	{
		if (deferred.empty())
			return true;

		for (auto& future : deferred)
		{
			future.second.get();

			if (progress && !progress->step())
				return false;
		}

		return true;
	}

	Entity* ObjectLoader::load(const File& file, bool shallow, const std::function<void(Entity* entity)>& callback)
	{
		auto path = file.path();

		try
		{
			depth++;
			Epilog epilog([&]() { depth--; });

			// Reset task queue
			if (depth == 1)
			{
				tasks = {};
			}

			// Load major entity
			if (Entity* result = storage.format->load(storage.assembly, file, shallow, tasks))
			{
				// Verify entity name matches filename (without extension)
				if (path.stem() != result->GetName())
					throw jadegit_exception("Entity name doesn't match filename");

				// Invoke callback for entity loaded
				if (callback) tasks.push(async(launch::deferred, callback, result));

				// Flush task queue
				if (depth == 1)
				{
					while (!tasks.empty())
					{
						tasks.front().get();
						tasks.pop();
					}
				}

				return result;
			}
		}
		catch (const jadegit_exception& e)
		{
			throw jadegit_exception(e.code, std::string(e.what()) + ", while loading [" + path.generic_string() + "]");
		}

		return nullptr;
	}

	void ObjectLoader::queue(const File& file, std::function<void(Entity* entity)> callback)
	{
		assert(file.vfs() == &storage.fs);

		// Suppress attempts to load test files from own repository
		if (dogfood)
		{
			auto path = file.path();
			while (path.has_parent_path())
				path = path.parent_path();

			// Ignore test files, and for performance include/src files
			if (path == "test" || path == "include" || path == "src")
				return;
		}

		// Setup deferred load
		deferred.insert(make_pair(file.path(), async(std::launch::deferred, [=]() { return load(file, false, callback); }).share()));
	}
}