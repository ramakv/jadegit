#pragma once
#include <string>
#include <vector>

namespace JadeGit::Data
{
	class Assembly;
	class Schema;
	class Application;
	class Method;

	class DeployConfig
	{
	public:
		DeployConfig(const Assembly& assembly);

		class Script
		{
		public:
			const Schema* schema = nullptr;
			const Application* app = nullptr;
			const Method* method = nullptr;
			std::string param;
			bool transient = false;
		};

		std::vector<Script> scripts;

		void load();

	private:
		const Assembly& assembly;
	};
}