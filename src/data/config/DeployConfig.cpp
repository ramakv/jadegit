#include "Config.h"
#include "DeployConfig.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Method.h>
#include <format>

using namespace std;

namespace JadeGit::Data
{
	DeployConfig::DeployConfig(const Assembly& assembly) : assembly(assembly)
	{
	}

	string position(const toml::node* node)
	{
		if (!node)
			return string();

		stringstream ss;
		ss << node->source().begin;
		return ss.str();
	}

	template <class T>
	T get_value(const toml::table& tbl, const char* key, bool required = true)
	{
		auto node_view = tbl[key];

		if (node_view && !node_view.is<T>())
			throw jadegit_exception(format("Type of configuration setting '{}' is unexpected ({})", key, position(node_view.node())));

		T value = node_view.value_or<T>(T());

		if (required && value == T())
			throw jadegit_exception(format("Configuration setting '{}' is missing ({})", key, position(&tbl)));

		return value;
	}

	template <class T>
	const T* resolve(const Component& origin, const toml::table& tbl, const char* key, bool required = true)
	{
		QualifiedName name(get_value<string>(tbl, key, required));

		const T* entity = Entity::resolve<T>(origin, name, true, true);

		if (!entity && required)
			throw jadegit_exception(format("Entity '{}' referred to by configuration setting '{}' is invalid ({})", string(name), key, position(tbl[key].node())));
		
		return entity;
	}

	DeployConfig::Script loadScript(const Assembly& assembly, const toml::table& tbl)
	{
		// TODO: Add support for inline scripts / workspace files, based on which method setting wouldn't be required

		DeployConfig::Script script;
		script.schema = resolve<Schema>(assembly, tbl, "schema");
		script.app = resolve<Application>(*script.schema, tbl, "app");
		script.method = resolve<Method>(Entity::resolve<Class&>(*script.schema, "JadeScript"), tbl, "method");
		script.param = get_value<string>(tbl, "param", script.method && !script.method->parameters.empty());
		script.transient = get_value<bool>(tbl, "transient", false);
		
		// Sanity check method options
		if (script.method)
		{
			// TODO: Throw errors for interface/mapping methods

			if (!script.param.empty() && script.method->parameters.empty())
				throw jadegit_exception(format("Deployment script param [{}] is not required by method [{}] ({})", script.param, script.method->GetQualifiedName(), position(&tbl)));

			if (script.transient && script.method->isTypeMethod())
				throw jadegit_exception(format("Deployment script transient option is invalid for type method [{}] ({})", script.method->GetQualifiedName(), position(&tbl)));
		}

		return script;
	}

	void DeployConfig::load()
	{
		scripts.clear();

		auto config = assembly.getConfig()["deployment"]["scripts"];

		if (auto arr = config.as_array())
		{
			scripts.reserve(arr->size());
			arr->for_each([this](const toml::table& tbl) { scripts.push_back(loadScript(assembly, tbl)); });
		}
	}
}