#include <jadegit/data/Form.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/FormMeta.h>
#include "LocaleEntityRegistration.h"

// TODO: Remove dependencies for metadata
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Reference.h>

namespace JadeGit::Data
{
	const std::filesystem::path Form::subFolder("forms");

	static LocaleEntityRegistration<Form> registrar("Form", &Locale::forms);

	template ObjectValue<Locale* const, &FormMeta::locale>;
	template MemberKeyDictionary<Control, &Entity::name>;
	template ObjectValue<Array<Control*>, &FormMeta::controlList>;
	template ObjectValue<Array<MenuItem*>, &FormMeta::menuList>;

	Form::Form(Locale* parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::form), name),
		locale(parent)
	{
	}

	void Form::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	// Resolve form class
	GUIClass* Form::GetClass() const
	{
		return Entity::resolve<GUIClass>(*this, name);
	}

	// Resolve super-form
	Form* Form::GetSuperForm() const
	{
		if (GUIClass* formClass = GetClass())
		{
			if (const Class* superClass = formClass->getSuperClass())
				return superClass->GetName() == "Form" ? nullptr : EntityFactory::Get().Resolve<Form>(locale, superClass->name, true);
		}

		return nullptr;
	}

	FormMeta::FormMeta(RootSchema& parent, const WindowMeta& superclass) : RootClass(parent, "Form", superclass),
		allowDocking(NewInteger("allowDocking")),
		backBrush(NewBinary("backBrush")),
		caption(NewString("caption", 256)),
		clientHeight(NewInteger("clientHeight")),
		clientWidth(NewInteger("clientWidth")),
		clipControls(NewBoolean("clipControls")),
		controlBox(NewBoolean("controlBox")),
		controlList(NewReference<ExplicitInverseRef>("controlList", NewType<CollClass>("ControlOrdList"))),
		icon(NewBinary("icon")),
		ignoreSkinAllControls(NewBoolean(Version(22, 0, 1), "ignoreSkinAllControls")),
		locale(NewReference<ExplicitInverseRef>("locale", NewType<Class>("Locale"))),
		maxButton(NewBoolean("maxButton")),
		mdiChild(NewInteger("mdiChild")),
		mdiFrame(NewBoolean("mdiFrame")),
		menuList(NewReference<ExplicitInverseRef>("menuList", NewType<CollClass>("MenuOrdList"))),
		minButton(NewBoolean("minButton")),
		minimumHeight(NewInteger("minimumHeight")),
		minimumWidth(NewInteger("minimumWidth")),
		printerForm(NewBoolean("printerForm")),
		scaleForm(NewBoolean("scaleForm")),
		scrollBars(NewInteger("scrollBars")),
		secureForm(NewBoolean("secureForm")),
		showMdiCloseAllButPinnedMenu(NewBoolean(Version(20, 0, 1), "showMdiCloseAllButPinnedMenu")),
		showMdiCloseAllButThisMenu(NewBoolean(Version(20, 0, 1), "showMdiCloseAllButThisMenu")),
		showMdiCloseMenu(NewBoolean(Version(20, 0, 1), "showMdiCloseMenu")),
		showMdiDockMenu(NewBoolean(Version(20, 0, 1), "showMdiDockMenu")),
		showMdiFloatMenu(NewBoolean(Version(20, 0, 1), "showMdiFloatMenu")),
		showMdiPinMenu(NewBoolean(Version(20, 0, 1), "showMdiPinMenu")),
		webBrowserAutoRefreshInterval(NewInteger("webBrowserAutoRefreshInterval")),
		webBrowserAutoRefreshURL(NewString("webBrowserAutoRefreshURL")),
		webBrowserDisableBackButton(NewBoolean("webBrowserDisableBackButton")),
		webEncodingType(NewCharacter("webEncodingType")),
		webFileName(NewString("webFileName")),
		webForm(NewBoolean("webForm")),
		windowState(NewInteger("windowState"))
	{
		controlList->automatic().bind(&Form::controlList);
		locale->manual().parent().bind(&Form::locale);
		menuList->automatic().bind(&Form::menuList);

		new ExternalMethod(*this, nullptr, "activate");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "deactivate");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "firstChange");
		new ExternalMethod(*this, nullptr, "formMove");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "load");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mdiDocked");
		new ExternalMethod(*this, nullptr, "mdiFloated");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "queryUnload");
		new ExternalMethod(*this, nullptr, "resize");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "trayIconClicked");
		new ExternalMethod(*this, nullptr, "unload");
		new ExternalMethod(*this, nullptr, "userNotify");
	}
}