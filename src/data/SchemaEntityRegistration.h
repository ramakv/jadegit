#pragma once
#include <jadegit/data/Schema.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	template <typename TDerived>
	class SchemaEntityRegistration : public EntityRegistration<TDerived, Schema>
	{
	public:
		using EntityRegistration<TDerived, Schema>::EntityRegistration;
		
	protected:
		TDerived* Resolve(Schema* parent, const std::string& name, bool shallow, bool inherit) const override
		{
			// Iterate through super schemas
			Schema* schema = parent;
			while (schema)
			{
				// Attempt standard load
				if (TDerived* entity = EntityRegistration<TDerived, Schema>::Resolve(schema, name, shallow, inherit))
					return entity;

				if (!inherit)
					break;

				schema = schema->GetSuperSchema();
			}

			// Entity doesn't exist in schema hierarchy
			return nullptr;
		}
	};
}