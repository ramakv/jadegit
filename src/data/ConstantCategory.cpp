#include <jadegit/data/ConstantCategory.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ConstantCategoryMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	class ConstantCategoryRegistration : public EntityRegistration<ConstantCategory, Schema>
	{
	public:
		ConstantCategoryRegistration() : EntityRegistration("ConstantCategory", &Schema::constantCategories) {}

	protected:
		ConstantCategory* Resolve(const Component* origin, bool expected) const override
		{
			if (ConstantCategory* result = EntityRegistration::Resolve(origin, false))
				return result;

			/* Special handling needed for resolving parent category (as opposed to parent schema) */
			if (const GlobalConstant* constant = dynamic_cast<const GlobalConstant*>(origin))
			{
				if (!constant->category)
					throw jadegit_exception("Global constant [" + constant->GetQualifiedName() + "] has no category");

				return constant->category;
			}

			if(expected)
				throw jadegit_exception("Failed to resolve constant category");

			return nullptr;
		}
	};

	static ConstantCategoryRegistration registrar;

	template ObjectValue<Schema* const, &ConstantCategoryMeta::schema>;
	template ObjectValue<Set<GlobalConstant*>, &ConstantCategoryMeta::consts>;

	ConstantCategory::ConstantCategory(Schema* parent, const Class* dataClass, const char* name) : SchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::constantCategory), name),
		schema(parent)
	{
	}

	void ConstantCategory::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	ConstantCategoryMeta::ConstantCategoryMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "ConstantCategory", superclass),
		consts(NewReference<ExplicitInverseRef>("consts", NewType<CollClass>("ConstantNDict"))),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema")))
	{
		consts->automatic().bind(&ConstantCategory::constants);
		schema->manual().parent().bind(&ConstantCategory::schema);
	}
}