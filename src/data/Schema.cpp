#include <jadegit/data/Schema.h>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/SchemaMeta.h>
#include <jadegit/data/Property.h>
#include <assert.h>
#include "EntityRegistration.h"
#include "State.h"

// TODO: Remove meta setup dependency
#include <jadegit/data/CollClass.h>

// TODO: MetaSchema registrations?
#include <jadegit/data/CardSchema.h>

namespace JadeGit::Data
{
	const std::filesystem::path Schema::subFolder("schemas");

	DEFINE_OBJECT_CAST(Schema)

	Schema* Schema::Load(Assembly* parent, const std::string& name, bool shallow)
	{
		if (name.empty())
			throw jadegit_exception("Missing schema name");

		// Find & load existing
		if (auto schema = parent->schemas.Get(name))
		{
			if (!shallow)
				schema->Load();

			return schema;
		}

		// Load CardSchema on demand
		if (name == "CardSchema")
			return *(parent->metaSchemas.emplace_back(std::make_unique<CardSchema>(*parent, parent->GetRootSchema(), Version())).get());

		// Derive schema path
		auto path = Schema::subFolder / name;

		// Load schema
		auto schema = Entity::Load(parent, path, shallow);

		// Verify type
		if (schema && !dynamic_cast<Schema*>(schema))
			throw jadegit_exception("Entity loaded is not a schema as expected [" + path.generic_string() + "]");

		return static_cast<Schema*>(schema);
	}

	template<>
	Schema* EntityRegistration<Schema, Assembly>::Load(Assembly* parent, const std::string& name, bool shallow) const
	{
		return Schema::Load(parent, name, shallow);
	}

	class SchemaRegistration : public EntityRegistration<Schema, Assembly>
	{
	public:
		SchemaRegistration() : EntityRegistration("Schema", &Assembly::schemas) {}

	protected:
		using EntityRegistration::Resolve;

		Schema* Resolve(const Component* origin, bool expected) const override
		{
			// Basic resolution to check ancestors
			if (Schema* schema = EntityRegistration::Resolve(origin, false))
				return schema;

			// Default to RootSchema for Assembly
			if (const Assembly* assembly = dynamic_cast<const Assembly*>(origin))
				return assembly->GetRootSchema();

			if (expected)
				throw jadegit_exception("Failed to resolve schema");

			return nullptr;
		}
	};

	static SchemaRegistration registrar;

	template EntityDict<ActiveXLibrary, &SchemaMeta::activeXLibraries>;
	template EntityDict<Application, &SchemaMeta::_applications>;
	template EntityDict<Class, &SchemaMeta::classes>;
	template EntityDict<ConstantCategory, &SchemaMeta::constantCategories>;
	template EntityDict<GlobalConstant, &SchemaMeta::consts>;
	template EntityDict<Database, &SchemaMeta::databases>;
	template EntityDict<JadeExportedPackage, &SchemaMeta::exportedPackages>;
	template EntityDict<JadeExposedList, &SchemaMeta::_exposedLists>;
	template EntityDict<Function, &SchemaMeta::functions>;
	template EntityDict<JadeHTMLDocument, &SchemaMeta::_jadeHTMLDocuments>;
	template EntityDict<JadeImportedPackage, &SchemaMeta::importedPackages>;
	template EntityDict<JadeInterface, &SchemaMeta::interfaces>;
	template EntityDict<Library, &SchemaMeta::libraries>;
	template EntityDict<Locale, &SchemaMeta::locales>;
	template EntityDict<LocaleFormat, &SchemaMeta::userFormats>;
	template Value<Locale*>;
	template EntityDict<PrimType, &SchemaMeta::primitives>;
	template EntityDict<PseudoType, &SchemaMeta::pseudoTypes>;
	template EntityDict<RelationalView, &SchemaMeta::relationalViews>;
	template ObjectValue<Set<Schema*>, &SchemaMeta::subschemas>;
	template ObjectValue<Schema*, &SchemaMeta::superschema>;

	Schema::Schema(Assembly& parent, const Class* dataClass, const char* name, const RootSchema& rootSchema) : MajorEntity(parent, nullptr, name)
	{
		if (!parent.rootSchema)
		{
			assert(GetName() == "RootSchema");
			state = StaticState::Instance();

			SetDataClass(*(new Class(this, new Class(this, nullptr, "Class"), "Schema")));

			parent.rootSchema = &rootSchema;
		}
		else
		{
			SetDataClass(**rootSchema.schema);
		}

		// Add to assembly
		parent.schemas.Add(this);
	}

	Schema::Schema(Assembly& parent, const Class* dataClass, const char* name) : Schema(parent, dataClass, name, parent.GetRootSchema()) {}

	Schema::~Schema()
	{
		getAssembly().schemas.Remove(this);

		// Delete locales ahead of other children
		// Intent is to remove child forms/controls ahead of types they may depend on
		std::vector<Locale*> locales;
		for (auto& locale : this->locales)
			locales.push_back(locale.second);
		for (auto& locale : locales)
			Object::dele(locale);
	}

	void Schema::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	bool Schema::InheritsFrom(const Schema* superschema) const
	{
		if (!superschema)
			return false;

		if (this == superschema)
			return true;

		return this->superschema ? this->superschema->InheritsFrom(superschema) : false;
	}

	bool Schema::IsDescendent(const Object* ancestor) const
	{
		return InheritsFrom(dynamic_cast<const Schema*>(ancestor));
	}

	bool Schema::IsRootSchema() const
	{
		// Either we're in the process of creating it or it can be dereferenced via metadata
		auto& assembly = getAssembly();
		return !assembly.rootSchema || this == *assembly.rootSchema;
	}

	Database* Schema::getDatabase() const
	{
		// Ensure embedded databases have been loaded
		Load();

		// Return first & only database
		return (databases.size() == 1 ? databases.begin()->second : nullptr);
	}

	Schema* Schema::GetSuperSchema() const
	{
		/* Ensure superschema has been set */
		if (!superschema && !IsRootSchema())
			throw jadegit_exception(GetQualifiedName() + " has no superschema");

		return superschema;
	}

	Type* Schema::getType(const std::string& name) const
	{
		if (auto type = primitives.Get(name))
			return type;

		if (auto type = classes.Get(name))
			return type;

		if (auto type = interfaces.Get(name))
			return type;

		if (auto type = pseudoTypes.Get(name))
			return type;

		return nullptr;
	}

	SchemaMeta::SchemaMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "Schema", superclass),
		_applications(NewReference<ExplicitInverseRef>("_applications", NewType<CollClass>("AppNameDict"))),
		_exposedLists(NewReference<ExplicitInverseRef>("_exposedLists", NewType<CollClass>("JadeExposedListNDict"))),
		_jadeHTMLDocuments(NewReference<ExplicitInverseRef>("_jadeHTMLDocuments", NewType<CollClass>("JadeHTMLDocumentDict"))),
		activeXLibraries(NewReference<ExplicitInverseRef>("activeXLibraries", NewType<CollClass>("ActiveXLibraryGuidDict"))),
		classes(NewReference<ExplicitInverseRef>("classes", NewType<CollClass>("ClassNDict"))),
		constantCategories(NewReference<ExplicitInverseRef>("constantCategories", NewType<CollClass>("ConstCategoryNDict"))),
		consts(NewReference<ExplicitInverseRef>("consts", NewType<CollClass>("ConstantNDict"))),
		databases(NewReference<ExplicitInverseRef>("databases", NewType<CollClass>("DatabaseNDict"))),
		exportedPackages(NewReference<ExplicitInverseRef>("exportedPackages", NewType<CollClass>("JadeExportedPackageNDict"))),
		formsManagement(NewByte("formsManagement")),
		functions(NewReference<ExplicitInverseRef>("functions", NewType<CollClass>("FunctionNDict"))),
		helpFileName(NewString("helpFileName")),
		helpKeyword(NewString("helpKeyword")),
		importedPackages(NewReference<ExplicitInverseRef>("importedPackages", NewType<CollClass>("JadeImportedPackageNDict"))),
		interfaces(NewReference<ExplicitInverseRef>("interfaces", NewType<CollClass>("JadeInterfaceNDict"))),
		libraries(NewReference<ExplicitInverseRef>("libraries", NewType<CollClass>("LibraryNDict"))),
		locales(NewReference<ExplicitInverseRef>("locales", NewType<CollClass>("LocaleNDict"))),
		name(NewString("name", 101)),
		primaryLocale(NewReference<ImplicitInverseRef>("primaryLocale", NewType<Class>("Locale"))),
		primitives(NewReference<ExplicitInverseRef>("primitives", NewType<CollClass>("PrimTypeNDict"))),
		pseudoTypes(NewReference<ExplicitInverseRef>("pseudoTypes", NewType<CollClass>("PseudoTypeNDict"))),
		relationalViews(NewReference<ExplicitInverseRef>("relationalViews", NewType<CollClass>("RelationalViewDict"))),
		subschemas(NewReference<ExplicitInverseRef>("subschemas", NewType<CollClass>("SchemaNDict"))),
		superschema(NewReference<ExplicitInverseRef>("superschema", NewType<Class>("Schema"))),
		text(NewString("text")),
		userFormats(NewReference<ExplicitInverseRef>("userFormats", NewType<CollClass>("SchemaEntityNDict")))
	{
		_applications->automatic().bind(&Schema::applications);
		_exposedLists->automatic().bind(&Schema::exposedLists);
		_jadeHTMLDocuments->automatic().bind(&Schema::htmlDocuments);
		activeXLibraries->automatic().bind(&Schema::activeXLibraries);
		classes->automatic().bind(&Schema::classes);
		constantCategories->automatic().bind(&Schema::constantCategories);
		consts->automatic().bind(&Schema::constants);
		databases->automatic().bind(&Schema::databases);
		exportedPackages->automatic().bind(&Schema::exportedPackages);
		functions->automatic().bind(&Schema::functions);
		importedPackages->automatic().bind(&Schema::importedPackages);
		interfaces->automatic().bind(&Schema::interfaces);
		libraries->automatic().bind(&Schema::libraries);
		locales->automatic().bind(&Schema::locales);
		name->unwritten().bind(&Schema::name);
		primaryLocale->bind(&Schema::primaryLocale);
		primitives->automatic().bind(&Schema::primitives);
		pseudoTypes->automatic().bind(&Schema::pseudoTypes);
		relationalViews->automatic().bind(&Schema::relationalViews);
		subschemas->automatic().bind(&Schema::subschemas);
		superschema->structural().bind(&Schema::superschema);
		text->bind(&Schema::text);
		userFormats->automatic().bind(&Schema::localeFormats);
	}
}