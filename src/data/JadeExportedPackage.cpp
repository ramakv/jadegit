#include <jadegit/data/JadeExportedPackage.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/JadeExportedClassMeta.h>
#include <jadegit/data/RootSchema/JadeExportedConstantMeta.h>
#include <jadegit/data/RootSchema/JadeExportedInterfaceMeta.h>
#include <jadegit/data/RootSchema/JadeExportedMethodMeta.h>
#include <jadegit/data/RootSchema/JadeExportedPackageMeta.h>
#include <jadegit/data/RootSchema/JadeExportedPropertyMeta.h>
#include <jadegit/data/RootSchema/JadeExportedTypeMeta.h>
#include <jadegit/data/RootSchema/JadePackageMeta.h>
#include "EntityRegistration.h"

using namespace std;

namespace JadeGit::Data
{
	const std::filesystem::path JadePackage::subFolder("packages");

	DEFINE_OBJECT_CAST(JadeExportedType);
	DEFINE_OBJECT_CAST(JadeExportedPackage);

	static EntityRegistration<JadeExportedConstant, JadeExportedType> exportedConstant("JadeExportedConstant", "exported constant", &JadeExportedType::exportedConstants);
	static EntityRegistration<JadeExportedMethod, JadeExportedType> exportedMethod("JadeExportedMethod", "exported method", &JadeExportedType::exportedMethods);
	static EntityRegistration<JadeExportedProperty, JadeExportedType> exportedProperty("JadeExportedProperty", "exported property", &JadeExportedType::exportedProperties);
	static EntityRegistration<JadeExportedType, JadeExportedPackage> exportedType("JadeExportedType", "exported type", std::mem_fn(&JadeExportedPackage::getType));
	static EntityRegistration<JadeExportedClass, JadeExportedPackage> exportedClass("JadeExportedClass", "exported class", &JadeExportedPackage::classes);
	static EntityRegistration<JadeExportedInterface, JadeExportedPackage> exportedInterface("JadeExportedInterface", "exported interface", &JadeExportedPackage::interfaces);
	static EntityRegistration<JadeExportedPackage, Schema> exportedPackage("JadeExportedPackage", "exported package", &Schema::exportedPackages);
	
	template ObjectValue<JadeExportedType*, &JadeExportedFeatureMeta::exportedType>;

	JadeExportedFeature::JadeExportedFeature(JadeExportedType* parent, const Class* dataClass, const char* name) : JadeExportedEntity(parent, dataClass, name)
	{
	}

	unique_ptr<QualifiedName> JadeExportedFeature::GetOriginalEntityName() const
	{
		unique_ptr<QualifiedName> result = make_unique<QualifiedName>(GetName());
		assert(!result->parent);

		result->parent = exportedType->GetOriginalEntityName();
		return result;
	}

	template ObjectValue<Constant*, &JadeExportedConstantMeta::constant>;
	template ObjectValue<Set<JadeImportedConstant*>, &JadeExportedConstantMeta::importedConstantRefs>;

	JadeExportedConstant::JadeExportedConstant(JadeExportedType* parent, const Class* dataClass, const char* name) : JadeExportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExportedConstant), name)
	{
		exportedType = parent;
	}

	void JadeExportedConstant::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	const Constant& JadeExportedConstant::getOriginal() const
	{
		return *constant;
	}

	void JadeExportedConstant::loaded(std::queue<std::future<void>>& tasks)
	{
		JadeExportedFeature::loaded(tasks);

		// Resolve original constant
		tasks.push(async(launch::deferred, [this]() { constant = EntityFactory::Get().Resolve<Constant>(this, GetOriginalEntityName().get()); }));
	}

	template ObjectValue<Set<JadeImportedMethod*>, &JadeExportedMethodMeta::importedMethodRefs>;
	template ObjectValue<Method*, &JadeExportedMethodMeta::method>;

	JadeExportedMethod::JadeExportedMethod(JadeExportedType* parent, const Class* dataClass, const char* name) : JadeExportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExportedMethod), name)
	{
		exportedType = parent;
	}

	void JadeExportedMethod::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	const Method& JadeExportedMethod::getOriginal() const
	{
		return *method;
	}

	void JadeExportedMethod::loaded(std::queue<std::future<void>>& tasks)
	{
		JadeExportedFeature::loaded(tasks);

		// Resolve original method
		tasks.push(async(launch::deferred, [this]() { method = EntityFactory::Get().Resolve<Method>(this, GetOriginalEntityName().get()); }));
	}

	template ObjectValue<Set<JadeImportedProperty*>, &JadeExportedPropertyMeta::importedPropertyRefs>;
	template ObjectValue<Property*, &JadeExportedPropertyMeta::property>;

	JadeExportedProperty::JadeExportedProperty(JadeExportedType* parent, const Class* dataClass, const char* name) : JadeExportedFeature(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExportedProperty), name)
	{
		exportedType = parent;
	}

	void JadeExportedProperty::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	const Property& JadeExportedProperty::getOriginal() const
	{
		return *property;
	}

	void JadeExportedProperty::loaded(std::queue<std::future<void>>& tasks)
	{
		JadeExportedFeature::loaded(tasks);

		// Resolve original property
		tasks.push(async(launch::deferred, [this]() { property = EntityFactory::Get().Resolve<Property>(this, GetOriginalEntityName().get()); }));
	}

	template EntityDict<JadeExportedConstant, &JadeExportedTypeMeta::exportedConstants>;
	template EntityDict<JadeExportedMethod, &JadeExportedTypeMeta::exportedMethods>;
	template EntityDict<JadeExportedProperty, &JadeExportedTypeMeta::exportedProperties>;
	template ObjectValue<JadeExportedPackage*, &JadeExportedTypeMeta::package>;

	JadeExportedType::JadeExportedType(JadeExportedPackage* parent, const Class* dataClass, const char* name) : JadeExportedEntity(parent, dataClass, name)
	{
	}

	unique_ptr<QualifiedName> JadeExportedType::GetOriginalEntityName() const
	{
		unique_ptr<QualifiedName> result = make_unique<QualifiedName>(GetName());
		assert(!result->parent);

		result->parent = package->GetOriginalEntityName();
		return result;
	}

	template ObjectValue<Set<JadeImportedClass*>, &JadeExportedClassMeta::importedClassRefs>;
	template ObjectValue<Class*, &JadeExportedClassMeta::originalClass>;

	JadeExportedClass::JadeExportedClass(JadeExportedPackage* parent, const Class* dataClass, const char* name) : JadeExportedType(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExportedClass), name)
	{
		package = parent;
	}

	void JadeExportedClass::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	const Class& JadeExportedClass::getOriginal() const
	{
		if (!originalClass)
			throw jadegit_exception("Missing original class");

		return originalClass->getOriginal();
	}

	void JadeExportedClass::loaded(std::queue<std::future<void>>& tasks)
	{
		JadeExportedType::loaded(tasks);

		// Resolve original class
		tasks.push(async(launch::deferred, [this]() { originalClass = EntityFactory::Get().Resolve<Class>(this, GetOriginalEntityName().get()); }));
	}

	template ObjectValue<Set<JadeImportedInterface*>, &JadeExportedInterfaceMeta::importedInterfaceRefs>;
	template ObjectValue<JadeInterface*, &JadeExportedInterfaceMeta::originalInterface>;

	JadeExportedInterface::JadeExportedInterface(JadeExportedPackage* parent, const Class* dataClass, const char* name) : JadeExportedType(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExportedInterface), name)
	{
		package = parent;
	}

	void JadeExportedInterface::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	const JadeInterface& JadeExportedInterface::getOriginal() const
	{
		if (!originalInterface)
			throw jadegit_exception("Missing original interface");

		return originalInterface->getOriginal();
	}

	void JadeExportedInterface::loaded(std::queue<std::future<void>>& tasks)
	{
		JadeExportedType::loaded(tasks);

		// Resolve original interface
		tasks.push(async(launch::deferred, [this]() { originalInterface = EntityFactory::Get().Resolve<JadeInterface>(this, GetOriginalEntityName().get()); }));
	}

	template ObjectValue<Schema*, &JadePackageMeta::schema>;

	JadePackage::JadePackage(Schema* parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass, name)
	{
	}

	template ObjectValue<Application*, &JadeExportedPackageMeta::application>;
	template EntityDict<JadeExportedClass, &JadeExportedPackageMeta::classes>;
	template EntityDict<JadeExportedInterface, &JadeExportedPackageMeta::interfaces>;
	template ObjectValue<Set<JadeImportedPackage*>, &JadeExportedPackageMeta::importedPackageRefs>;

	JadeExportedPackage::JadeExportedPackage(Schema* parent, const Class* dataClass, const char* name) : JadePackage(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeExportedPackage), name)
	{
		schema = parent;
	}

	void JadeExportedPackage::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	std::unique_ptr<QualifiedName> JadeExportedPackage::GetOriginalEntityName() const
	{
		// Root of original entity name is the schema, with package name omitted
		std::unique_ptr<QualifiedName> result(new QualifiedName(schema->GetQualifiedName()));
		return result;
	}

	JadeExportedType* JadeExportedPackage::getType(const std::string& name)
	{
		if (auto type = classes.Get(name))
			return type;

		if (auto type = interfaces.Get(name))
			return type;

		return nullptr;
	}

	JadeExportedFeatureMeta::JadeExportedFeatureMeta(RootSchema& parent, const JadeExportedEntityMeta& superclass) : RootClass(parent, "JadeExportedFeature", superclass),
		exportedType(NewReference<ExplicitInverseRef>("exportedType", NewType<Class>("JadeExportedType")))
	{
		exportedType->manual().parent().bind(&JadeExportedFeature::exportedType);
	}

	JadeExportedConstantMeta::JadeExportedConstantMeta(RootSchema& parent, const JadeExportedFeatureMeta& superclass) : RootClass(parent, "JadeExportedConstant", superclass),
		constant(NewReference<ExplicitInverseRef>("constant", NewType<Class>("Constant"))),
		importedConstantRefs(NewReference<ExplicitInverseRef>("importedConstantRefs", NewType<CollClass>("JadeImportedConstantSet")))
	{
		constant->unwritten().bind(&JadeExportedConstant::constant);
		importedConstantRefs->automatic().bind(&JadeExportedConstant::importedConstantRefs);
	}

	JadeExportedMethodMeta::JadeExportedMethodMeta(RootSchema& parent, const JadeExportedFeatureMeta& superclass) : RootClass(parent, "JadeExportedMethod", superclass),
		importedMethodRefs(NewReference<ExplicitInverseRef>("importedMethodRefs", NewType<CollClass>("JadeImportedMethodSet"))),
		method(NewReference<ExplicitInverseRef>("method", NewType<Class>("Method")))
	{
		importedMethodRefs->automatic().bind(&JadeExportedMethod::importedMethodRefs);
		method->unwritten().bind(&JadeExportedMethod::method);
	}

	JadeExportedPropertyMeta::JadeExportedPropertyMeta(RootSchema& parent, const JadeExportedFeatureMeta& superclass) : RootClass(parent, "JadeExportedProperty", superclass),
		importedPropertyRefs(NewReference<ExplicitInverseRef>("importedPropertyRefs", NewType<CollClass>("JadeImportedPropertySet"))),
		property(NewReference<ExplicitInverseRef>("property", NewType<Class>("Property")))
	{
		importedPropertyRefs->automatic().bind(&JadeExportedProperty::importedPropertyRefs);
		property->unwritten().bind(&JadeExportedProperty::property);
	}

	JadeExportedTypeMeta::JadeExportedTypeMeta(RootSchema& parent, const JadeExportedEntityMeta& superclass) : RootClass(parent, "JadeExportedType", superclass),
		exportedConstants(NewReference<ExplicitInverseRef>("exportedConstants", NewType<CollClass>("JadeExportedConstantNDict"))),
		exportedMethods(NewReference<ExplicitInverseRef>("exportedMethods", NewType<CollClass>("JadeExportedMethodNDict"))),
		exportedProperties(NewReference<ExplicitInverseRef>("exportedProperties", NewType<CollClass>("JadeExportedPropertyNDict"))),
		package(NewReference<ExplicitInverseRef>("package", NewType<Class>("JadePackage")))
	{
		exportedConstants->automatic().bind(&JadeExportedType::exportedConstants);
		exportedMethods->automatic().bind(&JadeExportedType::exportedMethods);
		exportedProperties->automatic().bind(&JadeExportedType::exportedProperties);
		package->manual().parent().bind(&JadeExportedType::package);
	}

	JadeExportedClassMeta::JadeExportedClassMeta(RootSchema& parent, const JadeExportedTypeMeta& superclass) : RootClass(parent, "JadeExportedClass", superclass),
		importedClassRefs(NewReference<ExplicitInverseRef>("importedClassRefs", NewType<CollClass>("JadeImportedClassSet"))),
		originalClass(NewReference<ExplicitInverseRef>("originalClass", NewType<Class>("Class"))),
		persistentAllowed(NewBoolean("persistentAllowed")),
		sharedTransientAllowed(NewBoolean("sharedTransientAllowed")),
		subclassPersistentAllowed(NewBoolean("subclassPersistentAllowed")),
		subclassSharedTransientAllowed(NewBoolean("subclassSharedTransientAllowed")),
		subclassTransientAllowed(NewBoolean("subclassTransientAllowed")),
		transient(NewBoolean("transient")),
		transientAllowed(NewBoolean("transientAllowed"))
	{
		importedClassRefs->automatic().bind(&JadeExportedClass::importedClassRefs);
		originalClass->unwritten().bind(&JadeExportedClass::originalClass);
		persistentAllowed->bind(&JadeExportedClass::persistentAllowed);
		sharedTransientAllowed->bind(&JadeExportedClass::sharedTransientAllowed);
		transient->bind(&JadeExportedClass::transient);
		transientAllowed->bind(&JadeExportedClass::transientAllowed);
	}

	JadeExportedInterfaceMeta::JadeExportedInterfaceMeta(RootSchema& parent, const JadeExportedTypeMeta& superclass) : RootClass(parent, "JadeExportedInterface", superclass),
		importedInterfaceRefs(NewReference<ExplicitInverseRef>("importedInterfaceRefs", NewType<CollClass>("JadeImportedInterfaceSet"))),
		originalInterface(NewReference<ExplicitInverseRef>("originalInterface", NewType<Class>("JadeInterface")))
	{
		importedInterfaceRefs->automatic().bind(&JadeExportedInterface::importedInterfaceRefs);
		originalInterface->unwritten().bind(&JadeExportedInterface::originalInterface);
	}

	JadePackageMeta::JadePackageMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "JadePackage", superclass),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema")))
	{
		schema->manual().parent().bind(&JadePackage::schema);
	}

	JadeExportedPackageMeta::JadeExportedPackageMeta(RootSchema& parent, const JadePackageMeta& superclass) : RootClass(parent, "JadeExportedPackage", superclass),
		application(NewReference<ExplicitInverseRef>("application", NewType<Class>("Application"))),
		classes(NewReference<ExplicitInverseRef>("classes", NewType<CollClass>("JadeExportedClassNDict"))),
		importedPackageRefs(NewReference<ExplicitInverseRef>("importedPackageRefs", NewType<CollClass>("JadeImportedPackageSet"))),
		interfaces(NewReference<ExplicitInverseRef>("interfaces", NewType<CollClass>("JadeExportedInterfaceNDict")))
	{
		application->bind(&JadeExportedPackage::application);
		classes->automatic().bind(&JadeExportedPackage::classes);
		importedPackageRefs->automatic().bind(&JadeExportedPackage::importedPackageRefs);
		interfaces->automatic().bind(&JadeExportedPackage::interfaces);
	}
}