#include <jadegit/data/Locale.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/LocaleMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	const std::filesystem::path Locale::subFolder("locales");

	static EntityRegistration<Locale, Schema> registrar("Locale", &Schema::locales);

	template ObjectValue<Schema* const, &LocaleMeta::schema>;
	template Value<Locale*>;
	template EntityDict<Form, &LocaleMeta::forms>;
	template EntityDict<TranslatableString, &LocaleMeta::translatableStrings>;

	Locale::Locale(Schema* parent, const Class* dataClass, const char* name, Locale* cloneOf) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::locale), name),
		schema(parent),
		cloneOf(cloneOf)
	{
	}

	void Locale::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	bool Locale::isPrimary() const
	{
		schema->Load();

		return schema->primaryLocale == this;
	}

	Locale* Locale::getSuperLocale() const
	{
		if (Schema* schema = this->schema->GetSuperSchema())
			return registrar.Resolve(schema, name);

		return nullptr;
	}

	LocaleMeta::LocaleMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "Locale", superclass),
		cloneOf(NewReference<ExplicitInverseRef>("cloneOf", NewType<Class>("Locale"))),
		forms(NewReference<ExplicitInverseRef>("forms", NewType<CollClass>("FormNameDict"))),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		translatableStrings(NewReference<ExplicitInverseRef>("translatableStrings", NewType<CollClass>("ConstantNDict")))
	{
		cloneOf->bind(&Locale::cloneOf);
		forms->automatic().bind(&Locale::forms);
		schema->manual().parent().bind(&Locale::schema);
		translatableStrings->automatic().bind(&Locale::translatableStrings);
	}
}