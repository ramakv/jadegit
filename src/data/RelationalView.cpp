#include <jadegit/data/RelationalView.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/RACollectionValueMeta.h>
#include <jadegit/data/RootSchema/RAMethodMeta.h>
#include <jadegit/data/RootSchema/RAOidMeta.h>
#include <jadegit/data/RootSchema/RAParameterMeta.h>
#include <jadegit/data/RootSchema/RAPropertyMeta.h>
#include <jadegit/data/RootSchema/RARelationIndexMeta.h>
#include <jadegit/data/RootSchema/RARelationKeyMeta.h>
#include <jadegit/data/RootSchema/RARelationOidMeta.h>
#include <jadegit/data/RootSchema/RARpsPropertyMeta.h>
#include <jadegit/data/RootSchema/RelationalTableClassMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMeta.h>
#include <jadegit/data/RootSchema/RelationalTableCollectionMethMeta.h>
#include <jadegit/data/RootSchema/RelationalTableRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalTableXRelationshipMeta.h>
#include <jadegit/data/RootSchema/RelationalViewMeta.h>
#include <jadegit/data/CollClass.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	const std::filesystem::path RelationalView::subFolder("relational-views");

	static EntityRegistration<RelationalView, Schema> relationalView("RelationalView", &Schema::relationalViews);
	static EntityRegistration<RelationalTable, RelationalView> relationalTable("RelationalTable", &RelationalView::tables);
	static EntityRegistration<RelationalAttribute, RelationalTable> relationalAttribute("RelationalAttribute", &RelationalTable::attributes);
	static EntityRegistration<RAMethod, RelationalTable> raMethod("RAMethod", &RelationalTable::attributes);

	template ObjectValue<Schema* const, &RelationalViewMeta::schema>;
	template EntityDict<RelationalTable, &RelationalViewMeta::tables>;

	RelationalView::RelationalView(Schema* parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::relationalView), name),
		schema(parent)
	{
	}

	void RelationalView::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template EntityDict<RelationalAttribute, &RelationalTableMeta::attributes>;
	template ObjectValue<RelationalView* const, &RelationalTableMeta::rView>;

	RelationalTable::RelationalTable(RelationalView* parent, const Class* dataClass, const char* name) : Entity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::relationalTable), name),
		view(parent)
	{
	}

	void RelationalTable::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template ObjectValue<RelationalTable* const, &RelationalAttributeMeta::table>;

	RelationalAttribute::RelationalAttribute(RelationalTable* parent, const Class* dataClass, const char* name) : Entity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::relationalAttribute), name),
		table(parent)
	{
	}

	void RelationalAttribute::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	RAMethod::RAMethod(RelationalTable* parent, const Class* dataClass, const char* name) : RelationalAttribute(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::raMethod), name) {}

	RelationalAttributeMeta::RelationalAttributeMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "RelationalAttribute", superclass),
		attrName(NewString("attrName", 101)),
		decimals(NewInteger("decimals")),
		feature(NewReference<ExplicitInverseRef>("feature", NewType<Class>("Feature"))),
		length(NewInteger("length")),
		name(NewString("name", 101)),
		referencedClassName(NewString("referencedClassName", 101)),
		rpsColType(NewInteger("rpsColType")),
		rpsExcluded(NewBoolean("rpsExcluded")),
		rpsKeyKind(NewInteger("rpsKeyKind")),
		rpsMapKind(NewInteger("rpsMapKind")),
		sqlType(NewInteger("sqlType")),
		table(NewReference<ExplicitInverseRef>("table", NewType<Class>("RelationalTable")))
	{
		name->unwritten().bind(&RelationalAttribute::name);
		table->unwritten().bind(&RelationalAttribute::table);
	}

	RACollectionValueMeta::RACollectionValueMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RACollectionValue", superclass) {}

	RAMethodMeta::RAMethodMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RAMethod", superclass) {}

	RAOidMeta::RAOidMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RAOid", superclass) {}

	RAParameterMeta::RAParameterMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RAParameter", superclass) {}

	RAPropertyMeta::RAPropertyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RAProperty", superclass) {}

	RARelationIndexMeta::RARelationIndexMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RARelationIndex", superclass),
		fromFirstClass(NewBoolean("fromFirstClass")),
		propertyName(NewString("propertyName", 101)) {}

	RARelationKeyMeta::RARelationKeyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RARelationKey", superclass),
		keyIndex(NewInteger("keyIndex")) {}

	RARelationOidMeta::RARelationOidMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RARelationOid", superclass),
		fromFirstClass(NewBoolean("fromFirstClass")) {}

	RARpsPropertyMeta::RARpsPropertyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RARpsProperty", superclass),
		excluded(NewBoolean("excluded")),
		guid(NewString("guid", 39)) {}

	RelationalTableMeta::RelationalTableMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "RelationalTable", superclass),
		attributes(NewReference<ExplicitInverseRef>("attributes", NewType<CollClass>("RelationalAttributeDict"))),
		classes(NewReference<ExplicitInverseRef>("classes", NewType<CollClass>("ClassSet"))),
		firstClassName(NewString("firstClassName", 101)),
		name(NewString("name", 101)),
		rView(NewReference<ExplicitInverseRef>("rView", NewType<Class>("RelationalView"))),
		rpsExcluded(NewBoolean("rpsExcluded")),
		rpsExtra(NewBoolean("rpsExtra")),
		rpsIncludeInCallback(NewBoolean("rpsIncludeInCallback")),
		rpsShowMethods(NewBoolean("rpsShowMethods")),
		rpsShowObjectFeatures(NewBoolean("rpsShowObjectFeatures")),
		rpsShowVirtualProperties(NewBoolean("rpsShowVirtualProperties")),
		rpsTableKind(NewInteger("rpsTableKind")),
		uuid(NewBinary("uuid", 16))
	{
		attributes->automatic().bind(&RelationalTable::attributes);
		name->unwritten().bind(&RelationalTable::name);
		rView->unwritten().bind(&RelationalTable::view);
	}

	RelationalTableClassMeta::RelationalTableClassMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableClass", superclass),
		bCallIFAllInstances(NewBoolean("bCallIFAllInstances")),
		rootCollectionName(NewString("rootCollectionName", 101)) {}

	RelationalTableCollectionMeta::RelationalTableCollectionMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableCollection", superclass),
		collectionName(NewString("collectionName", 101)),
		collectionType(NewInteger("collectionType")) {}

	RelationalTableCollectionMethMeta::RelationalTableCollectionMethMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableCollectionMeth", superclass),
		collectionType(NewInteger("collectionType")),
		methodName(NewString("methodName", 101)),
		secondClassName(NewString("secondClassName", 101)) {}

	RelationalTableRelationshipMeta::RelationalTableRelationshipMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableRelationship", superclass),
		firstPropertyName(NewString("firstPropertyName", 101)),
		secondClassName(NewString("secondClassName", 101)) {}

	RelationalTableXRelationshipMeta::RelationalTableXRelationshipMeta(RootSchema& parent, const RelationalTableMeta& superclass) : RootClass(parent, "RelationalTableXRelationship", superclass),
		firstPropertyName(NewString("firstPropertyName", 101)),
		secondClassName(NewString("secondClassName", 101)) {}

	RelationalViewMeta::RelationalViewMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "RelationalView", superclass),
		defaultVisibility(NewInteger("defaultVisibility")),
		includeSystemClasses(NewBoolean("includeSystemClasses")),
		includedObjectFeatures(NewReference<ImplicitInverseRef>("includedObjectFeatures", NewType<CollClass>("FeatureArray"))),
		name(NewString("name", 101)),
		resyncOnExecute(NewBoolean("resyncOnExecute")),
		rootClass(NewString("rootClass", 101)),
		rpsDatabaseName(NewString("rpsDatabaseName", 129)),
		rpsDatabaseType(NewInteger("rpsDatabaseType")),
		rpsDefaultConnectionString(NewString("rpsDefaultConnectionString")),
		rpsDefaultPassword(NewString("rpsDefaultPassword", 129)),
		rpsDefaultUserName(NewString("rpsDefaultUserName", 31)),
		rpsExceptionCreate(NewInteger("rpsExceptionCreate")),
		rpsExceptionDelete(NewInteger("rpsExceptionDelete")),
		rpsExceptionUpdate(NewInteger("rpsExceptionUpdate")),
		rpsLoggingOptions(NewInteger("rpsLoggingOptions")),
		rpsShowMethods(NewBoolean("rpsShowMethods")),
		rpsShowObjectFeatures(NewBoolean("rpsShowObjectFeatures")),
		rpsShowVirtualProperties(NewInteger("rpsShowVirtualProperties")),
		rpsTopSchemaName(NewString("rpsTopSchemaName", 101)),
		rpsUseOidClassInstMap(NewBoolean("rpsUseOidClassInstMap")),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		securityMethod(NewString("securityMethod", 101)),
		tables(NewReference<ExplicitInverseRef>("tables", NewType<CollClass>("RelationalTableDict"))),
		uuid(NewBinary("uuid", 16))
	{
		name->unwritten().bind(&RelationalView::name);
		schema->unwritten().bind(&RelationalView::schema);
		tables->automatic().bind(&RelationalView::tables);
	}
}