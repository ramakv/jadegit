#include <jadegit/data/Database.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/DatabaseMeta.h>
#include <jadegit/data/RootSchema/DbClassMapMeta.h>
#include <jadegit/data/RootSchema/DbFileMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(DbClassMap)

	class DatabaseRegistration : public EntityRegistration<Database, Schema>
	{
	public:
		DatabaseRegistration() : EntityRegistration("Database", &Schema::databases) {}

	protected:
		Database* Resolve(const Component* origin, bool expected) const override
		{
			// Basic resolution to check ancestors
			if (Database* database = EntityRegistration::Resolve(origin, false))
				return database;

			// Special handling needed for resolving parent database (as opposed to parent class)
			if (const DbClassMap* classMap = dynamic_cast<const DbClassMap*>(origin))
			{
				if (!classMap->database)
					throw jadegit_exception("Class map has no database");

				return classMap->database;
			}

			// Default to schema database
			Schema* schema = ObjectFactory::Get().Resolve<Schema>(origin);
			if(Database* database = schema->getDatabase())
				return database;
			
			throw jadegit_exception("Failed to resolve database");
		}

		Database* Resolve(Schema* parent, const std::string& name, bool shallow, bool inherit) const override
		{
			// Check immediate parent, and then superschemas
			Schema* superschema = parent;
			while (superschema)
			{
				if (Database* database = EntityRegistration::Resolve(superschema, name, shallow, inherit))
					return database;

				if (!inherit)
					break;

				superschema = superschema->GetSuperSchema();
			}

			return nullptr;
		}
	};
	static DatabaseRegistration database;

	class DbFileRegistration : public EntityRegistration<DbFile, Database>
	{
	public:
		DbFileRegistration() : EntityRegistration("DbFile", "database file", &Database::files) {}

	protected:
		using EntityRegistration::Resolve;

		DbFile* Resolve(Database* parent, const std::string& name, bool shallow, bool inherit) const override
		{
			// Check immediate parent, and then superschemas
			Database* database = parent;
			while (database)
			{
				if (DbFile* dbFile = EntityRegistration::Resolve(database, name, shallow, inherit))
					return dbFile;

				if (!inherit)
					break;

				Schema* superschema = database->schema->GetSuperSchema();
				database = superschema ? superschema->getDatabase() : nullptr;
			}

			return nullptr;
		}
	};
	static DbFileRegistration dbFile;

	static ObjectRegistration<DbClassMap, Class> dbClassMap("DbClassMap");

	std::map<DbClassMap::Mode, const char*> EnumStrings<DbClassMap::Mode>::data =
	{
		{ DbClassMap::Mode::SharedInstances, "shared" },
		{ DbClassMap::Mode::SubobjectInstances, "subobjects" },
		{ DbClassMap::Mode::AllInstances, "all" }
	};

	template ObjectValue<Schema* const, &DatabaseMeta::schema>;
	template Value<DbFile*>;
	template ObjectValue<Set<DbClassMap*>, &DatabaseMeta::classMaps>;
	template EntityDict<DbFile, &DatabaseMeta::dbFiles>;

	Database::Database(Schema* parent, const Class* dataClass, const char* name) : SchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::database), name),
		schema(parent)
	{
	}

	void Database::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	template ObjectValue<Database*, &DbClassMapMeta::database>;
	template ObjectValue<Class* const, &DbClassMapMeta::diskClass>;
	template ObjectValue<DbFile*, &DbClassMapMeta::diskFile>;
	template Value<DbClassMap::Mode>;

	DbClassMap::DbClassMap(Class* parent, const Class* dataClass) : Object(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::dbClassMap)),
		diskClass(parent)
	{
	}

	void DbClassMap::LoadHeader(const FileElement& source)
	{
		Object::LoadHeader(source);

		// Load file
		if (auto file = source.header("file"))
		{
			if (diskFile = EntityFactory::Get().Resolve<DbFile>(diskClass, QualifiedName(file)))
			{
				// Attach class mapping to database associated with map file, switching to schema default if inherited
				database = diskFile->database;
				if (!database || database->schema != static_cast<Schema*>(diskClass->schema))
					database = diskClass->schema->getDatabase();
			}
		}

		// Load mode
		if (auto mode = source.header("mode"))
			GetRootSchema().dbClassMap->mode->SetValue<std::string>(*this, mode);
	}

	void DbClassMap::WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const
	{
		Object::WriteHeader(element, origin, reference);

		element->SetAttribute("file", diskFile->GetQualifiedName(this->diskClass, true).c_str());
		
		if (mode != Mode::SharedInstances)
			element->SetAttribute("mode", mode.ToString().c_str());
	}

	template ObjectValue<Database* const, &DbFileMeta::database>;
	template ObjectValue<Set<DbClassMap*>, &DbFileMeta::classMapRefs>;

	DbFile::DbFile(Database* parent, const Class* dataClass, const char* name) : SchemaEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::dbFile), name),
		database(parent)
	{
	}

	void DbFile::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	const Entity* DbFile::GetQualifiedParent() const
	{
		return database->schema;
	}

	DatabaseMeta::DatabaseMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "Database", superclass),
		classMaps(NewReference<ExplicitInverseRef>("classMaps", NewType<CollClass>("DbClassMapDict"))),
		dbFiles(NewReference<ExplicitInverseRef>("dbFiles", NewType<CollClass>("DbFileNDict"))),
		defaultFile(NewReference<ExplicitInverseRef>("defaultFile", NewType<Class>("DbFile"))),
		path(NewString("path")),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		serverName(NewString("serverName", 31))
	{
		classMaps->automatic().bind(&Database::classMaps);
		dbFiles->automatic().bind(&Database::files);
		defaultFile->bind(&Database::defaultFile);
		schema->manual().parent().bind(&Database::schema);
	}

	DbClassMapMeta::DbClassMapMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "DbClassMap", superclass),
		database(NewReference<ExplicitInverseRef>("database", NewType<Class>("Database"))),
		diskClass(NewReference<ExplicitInverseRef>("diskClass", NewType<Class>("Class"))),
		diskFile(NewReference<ExplicitInverseRef>("diskFile", NewType<Class>("DbFile"))),
		mode(NewCharacter("mode"))
	{
		database->unwritten().bind(&DbClassMap::database);
		diskClass->manual().parent().bind(&DbClassMap::diskClass);
		diskFile->unwritten().bind(&DbClassMap::diskFile);
		mode->unwritten().bind(&DbClassMap::mode);
	}

	DbFileMeta::DbFileMeta(RootSchema& parent, const SchemaEntityMeta& superclass) : RootClass(parent, "DbFile", superclass),
		classMapRefs(NewReference<ExplicitInverseRef>("classMapRefs", NewType<CollClass>("UserDbClassMapSet"))),
		database(NewReference<ExplicitInverseRef>("database", NewType<Class>("Database"))),
		excludeFromBackup(NewBoolean("excludeFromBackup")),
		kind(NewInteger("kind")),
		partitionable(NewBoolean("partitionable")),
		physicalFileName(NewString("physicalFileName"))
	{
		classMapRefs->automatic().bind(&DbFile::classMapRefs);
		database->manual().parent().bind(&DbFile::database);
	}
}