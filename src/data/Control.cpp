#include <jadegit/data/Control.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/Form.h>
#include <jadegit/data/Locale.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ActiveXControlMeta.h>
#include <jadegit/data/RootSchema/BaseControlMeta.h>
#include <jadegit/data/RootSchema/BrowseButtonsMeta.h>
#include <jadegit/data/RootSchema/ButtonMeta.h>
#include <jadegit/data/RootSchema/CheckBoxMeta.h>
#include <jadegit/data/RootSchema/ComboBoxMeta.h>
#include <jadegit/data/RootSchema/FolderMeta.h>
#include <jadegit/data/RootSchema/FrameMeta.h>
#include <jadegit/data/RootSchema/SheetMeta.h>
#include <jadegit/data/RootSchema/JadeDockBarMeta.h>
#include <jadegit/data/RootSchema/JadeDockContainerMeta.h>
#include <jadegit/data/RootSchema/JadeDotNetVisualComponentMeta.h>
#include <jadegit/data/RootSchema/JadeEditMaskMeta.h>
#include <jadegit/data/RootSchema/JadeRichTextMeta.h>
#include <jadegit/data/RootSchema/JadeEditorMeta.h>
#include <jadegit/data/RootSchema/JadeXamlControlMeta.h>
#include <jadegit/data/RootSchema/ProgressBarMeta.h>
#include <jadegit/data/RootSchema/WebHotSpotMeta.h>
#include <jadegit/data/RootSchema/WebInsertMeta.h>
#include <jadegit/data/RootSchema/WebJavaAppletMeta.h>
#include <jadegit/data/RootSchema/ListBoxMeta.h>
#include <jadegit/data/RootSchema/MultiMediaMeta.h>
#include <jadegit/data/RootSchema/OcxMeta.h>
#include <jadegit/data/RootSchema/OleControlMeta.h>
#include <jadegit/data/RootSchema/OptionButtonMeta.h>
#include <jadegit/data/RootSchema/JadeMaskMeta.h>
#include <jadegit/data/RootSchema/HScrollMeta.h>
#include <jadegit/data/RootSchema/VScrollMeta.h>
#include <jadegit/data/RootSchema/StatusLineMeta.h>
#include <jadegit/data/RootSchema/TableMeta.h>
#include <jadegit/data/RootSchema/WebHTMLMeta.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	static EntityRegistration<Control, Form> registrar("Control", &Form::controls);

	template ObjectValue<Form* const, &ControlMeta::form>;

	Control::Control(Form* parent, const Class* dataClass, const char* name) : Window(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::control), name),
		form(parent)
	{
		parent->controls.Add(this);
	}

	void Control::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	ControlMeta::ControlMeta(RootSchema& parent, const WindowMeta& superclass) : RootClass(parent, "Control", superclass),
		backBrush(NewBinary("backBrush")),
		borderColorSingle(NewInteger("borderColorSingle")),
		focusBackColor(NewInteger("focusBackColor")),
		focusForeColor(NewInteger("focusForeColor")),
		fontBold(NewBoolean("fontBold")),
		fontItalic(NewBoolean("fontItalic")),
		fontName(NewString("fontName", 32)),
		fontSize(NewReal("fontSize")),
		fontStrikethru(NewBoolean("fontStrikethru")),
		fontUnderline(NewBoolean("fontUnderline")),
		fontWeight(NewInteger("fontWeight")),
		foreColor(NewInteger("foreColor")),
		form(NewReference<ExplicitInverseRef>("form", NewType<GUIClass>("Form"))),
		parent(NewReference<ExplicitInverseRef>("parent", NewType<GUIClass>("Window"))),
		parentAspect(NewInteger("parentAspect")),
		parentBottomOffset(NewInteger("parentBottomOffset")),
		parentRightOffset(NewInteger("parentRightOffset")),
		relativeHeight(NewBoolean("relativeHeight")),
		relativeLeft(NewBoolean("relativeLeft")),
		relativeTop(NewBoolean("relativeTop")),
		relativeWidth(NewBoolean("relativeWidth")),
		show3D(NewInteger("show3D")),
		tabIndex(NewInteger("tabIndex")),
		tabStop(NewBoolean("tabStop"))
	{
		form->manual().parent().bind(&Control::form);

		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "windowCreated");
	}

	ActiveXControlMeta::ActiveXControlMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "ActiveXControl", superclass),
		savedData(NewBinary("savedData"))
	{
		savedData->SetAlias("_savedData");

		new ExternalMethod(*this, nullptr, "paint");
	}

	BaseControlMeta::BaseControlMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "BaseControl", superclass),
		clipControls(NewBoolean("clipControls")),
		scrollBars(NewInteger("scrollBars")),
		transparent(NewBoolean("transparent"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	BrowseButtonsMeta::BrowseButtonsMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "BrowseButtons", superclass)
	{
		new ExternalMethod(*this, nullptr, "browse");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	ButtonMeta::ButtonMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "Button", superclass),
		autoSize(NewInteger("autoSize")),
		buttonPicture(NewInteger("buttonPicture")),
		cancel(NewBoolean("cancel")),
		caption(NewString("caption")),
		default_(NewBoolean("default")),
		picture(NewBinary("picture")),
		pictureDisabled(NewBinary("pictureDisabled")),
		pictureDown(NewBinary("pictureDown")),
		style(NewInteger("style")),
		value(NewBoolean("value")),
		webFileName(NewString("webFileName"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	CheckBoxMeta::CheckBoxMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "CheckBox", superclass),
		alignment(NewInteger("alignment")),
		autoSize(NewBoolean("autoSize")),
		caption(NewString("caption")),
		readOnly(NewBoolean("readOnly")),
		transparent(NewBoolean("transparent")),
		value(NewBoolean("value"))
	{
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	ComboBoxMeta::ComboBoxMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "ComboBox", superclass),
		defaultLineHeight(NewInteger(Version(20, 0, 1), "defaultLineHeight")),
		hasPictures(NewBoolean("hasPictures")),
		hasPlusMinus(NewBoolean("hasPlusMinus")),
		hasTreeLines(NewBoolean("hasTreeLines")),
		listWidth(NewInteger("listWidth")),
		maxLength(NewInteger("maxLength")),
		pictureClosed(NewBinary("pictureClosed")),
		pictureLeaf(NewBinary("pictureLeaf")),
		pictureMinus(NewBinary("pictureMinus")),
		pictureOpen(NewBinary("pictureOpen")),
		picturePlus(NewBinary("picturePlus")),
		sortCased(NewBoolean("sortCased")),
		sorted(NewBoolean("sorted")),
		style(NewInteger("style"))
	{
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "closeup");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "displayEntry");
		new ExternalMethod(*this, nullptr, "displayRow");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "dropDown");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "pictureClick");
		new ExternalMethod(*this, nullptr, "pictureDblClick");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	FolderMeta::FolderMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "Folder", superclass),
		tabActiveColor(NewInteger("tabActiveColor")),
		tabInactiveColor(NewInteger("tabInactiveColor")),
		tabsAlignment(NewInteger("tabsAlignment")),
		tabsFixedWidth(NewBoolean("tabsFixedWidth")),
		tabsHeight(NewInteger("tabsHeight")),
		tabsLines(NewInteger("tabsLines")),
		tabsPosition(NewInteger("tabsPosition")),
		tabsRaggedRight(NewBoolean("tabsRaggedRight")),
		tabsStyle(NewInteger("tabsStyle"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "querySheetChg");
		new ExternalMethod(*this, nullptr, "sheetChg");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	FrameMeta::FrameMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "Frame", superclass),
		alignChildren(NewInteger("alignChildren")),
		alignContainer(NewInteger("alignContainer")),
		alignment(NewInteger("alignment")),
		allowDocking(NewInteger("allowDocking")),
		bevelColor(NewInteger("bevelColor")),
		bevelInner(NewInteger("bevelInner")),
		bevelInnerWidth(NewInteger("bevelInnerWidth")),
		bevelOuter(NewInteger("bevelOuter")),
		bevelOuterWidth(NewInteger("bevelOuterWidth")),
		bevelShadowColor(NewInteger("bevelShadowColor")),
		boundaryBrush(NewInteger("boundaryBrush")),
		boundaryColor(NewInteger("boundaryColor")),
		boundaryWidth(NewInteger("boundaryWidth")),
		caption(NewString("caption")),
		clipControls(NewBoolean("clipControls")),
		transparent(NewBoolean("transparent")),
		wordWrap(NewBoolean("wordWrap"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	GroupBoxMeta::GroupBoxMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "GroupBox", superclass),
		caption(NewString("caption")),
		clipControls(NewBoolean("clipControls")),
		transparent(NewBoolean("transparent"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	SheetMeta::SheetMeta(RootSchema& parent, const GroupBoxMeta& superclass) : RootClass(parent, "Sheet", superclass),
		icon(NewBinary("icon")) {}

	JadeDockBaseMeta::JadeDockBaseMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "JadeDockBase", superclass),
		alignContainer(NewInteger("alignContainer")),
		borderHeightBottom(NewInteger("borderHeightBottom")),
		borderHeightTop(NewInteger("borderHeightTop")),
		borderWidthLeft(NewInteger("borderWidthLeft")),
		borderWidthRight(NewInteger("borderWidthRight")),
		caption(NewString("caption")),
		drawGrip(NewInteger("drawGrip")),
		floatingStyle(NewInteger("floatingStyle")),
		maximumHeight(NewInteger("maximumHeight")),
		maximumWidth(NewInteger("maximumWidth")),
		minimumHeight(NewInteger("minimumHeight")),
		minimumWidth(NewInteger("minimumWidth")),
		showResizeBar(NewBoolean("showResizeBar")) {}

	JadeDockBarMeta::JadeDockBarMeta(RootSchema& parent, const JadeDockBaseMeta& superclass) : RootClass(parent, "JadeDockBar", superclass),
		alignChildren(NewInteger("alignChildren")),
		autoSpacingX(NewInteger("autoSpacingX")),
		autoSpacingY(NewInteger("autoSpacingY"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "docked");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "floated");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "queryDock");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
		new ExternalMethod(*this, nullptr, "userResize");
	}

	JadeDockContainerMeta::JadeDockContainerMeta(RootSchema& parent, const JadeDockBaseMeta& superclass) : RootClass(parent, "JadeDockContainer", superclass),
		allowDocking(NewInteger("allowDocking"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "docked");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "floated");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "queryDock");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
		new ExternalMethod(*this, nullptr, "userResize");
	}

	JadeDotNetVisualComponentMeta::JadeDotNetVisualComponentMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "JadeDotNetVisualComponent", superclass) {}

	JadeEditMaskMeta::JadeEditMaskMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "JadeEditMask", superclass),
		autoSize(NewBoolean("autoSize")),
		autoTab(NewBoolean("autoTab")),
		insertMode(NewBoolean("insertMode")),
		languageId(NewInteger("languageId")),
		mask(NewString("mask")),
		promptCharacter(NewCharacter("promptCharacter")),
		readOnly(NewBoolean("readOnly")),
		selectionStyle(NewInteger("selectionStyle")),
		text(NewString("text"))
	{
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "firstChange");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
		new ExternalMethod(*this, nullptr, "validate");
	}

	JadeRichTextMeta::JadeRichTextMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "JadeRichText", superclass),
		acceptTabs(NewBoolean("acceptTabs")),
		alignment(NewInteger("alignment")),
		autoURLDetect(NewBoolean("autoURLDetect")),
		bulletIndent(NewInteger("bulletIndent")),
		bulletStyle(NewInteger("bulletStyle")),
		contextMenuOptions(NewInteger("contextMenuOptions")),
		firstLineIndent(NewInteger("firstLineIndent")),
		initialContent(NewString("initialContent")),
		leftIndent(NewInteger("leftIndent")),
		lineWidth(NewInteger("lineWidth")),
		maxLength(NewInteger("maxLength")),
		readOnly(NewBoolean("readOnly")),
		rightIndent(NewInteger("rightIndent")),
		scrollBars(NewInteger("scrollBars")),
		selectionStyle(NewInteger("selectionStyle")),
		targetDevice(NewInteger("targetDevice")),
		wantReturn(NewBoolean("wantReturn")),
		zoom(NewInteger("zoom"))
	{
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "firstChange");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "linkClicked");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "protected");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "selChanged");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
		new JadeMethod(*this, nullptr, "windowCreated");
	}

	JadeTextEditMeta::JadeTextEditMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "JadeTextEdit", superclass),
		edgeColor(NewInteger("edgeColor")),
		edgeColumn(NewInteger("edgeColumn")),
		edgeMode(NewInteger("edgeMode")),
		endOfLineMode(NewInteger("endOfLineMode")),
		foldFlags(NewInteger("foldFlags")),
		foldSymbols(NewInteger("foldSymbols")),
		folding(NewBoolean("folding")),
		indentGuides(NewBoolean("indentGuides")),
		indentWidth(NewInteger("indentWidth")),
		markerMargin(NewBoolean("markerMargin")),
		readOnly(NewBoolean("readOnly")),
		selBackColor(NewInteger("selBackColor")),
		tabWidth(NewInteger("tabWidth")),
		useTabs(NewBoolean("useTabs")),
		viewEndOfLine(NewBoolean("viewEndOfLine")),
		viewLinenumbers(NewBoolean("viewLinenumbers")),
		viewWhitespace(NewInteger("viewWhitespace")),
		wrapIndent(NewInteger("wrapIndent")),
		wrapMode(NewInteger("wrapMode")),
		wrapVisualFlags(NewInteger("wrapVisualFlags"))
	{
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dynamicTextBuilder");
		new ExternalMethod(*this, nullptr, "firstChange");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	JadeEditorMeta::JadeEditorMeta(RootSchema& parent, const JadeTextEditMeta& superclass) : RootClass(parent, "JadeEditor", superclass) {}

	JadeXamlControlMeta::JadeXamlControlMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "JadeXamlControl", superclass),
		xaml(NewString("xaml"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	LabelMeta::LabelMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "Label", superclass),
		alignment(NewInteger("alignment")),
		autoSize(NewBoolean("autoSize")),
		caption(NewString("caption")),
		formatOut(NewString("formatOut", 101)),
		hyperlink(NewString("hyperlink")),
		noPrefix(NewBoolean("noPrefix")),
		transparent(NewBoolean("transparent")),
		wordWrap(NewBoolean("wordWrap"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	ProgressBarMeta::ProgressBarMeta(RootSchema& parent, const LabelMeta& superclass) : RootClass(parent, "ProgressBar", superclass),
		partsDone(NewInteger("partsDone")),
		partsInJob(NewInteger("partsInJob")),
		showTaskBarProgress(NewBoolean("showTaskBarProgress")) {}

	WebHotSpotMeta::WebHotSpotMeta(RootSchema& parent, const LabelMeta& superclass) : RootClass(parent, "WebHotSpot", superclass)
	{
		new JadeMethod(*this, nullptr, "windowCreated");
	}

	WebInsertMeta::WebInsertMeta(RootSchema& parent, const LabelMeta& superclass) : RootClass(parent, "WebInsert", superclass)
	{
		new JadeMethod(*this, nullptr, "windowCreated");
	}

	WebJavaAppletMeta::WebJavaAppletMeta(RootSchema& parent, const LabelMeta& superclass) : RootClass(parent, "WebJavaApplet", superclass),
		appletName(NewString("appletName", 129)),
		code(NewBinary("code")),
		codebase(NewString("codebase", 129)),
		horizontalSpace(NewInteger("horizontalSpace")),
		parameters(NewString("parameters")),
		verticalSpace(NewInteger("verticalSpace"))
	{
		new JadeMethod(*this, nullptr, "windowCreated");
	}

	ListBoxMeta::ListBoxMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "ListBox", superclass),
		alternatingRowBackColor(NewInteger("alternatingRowBackColor")),
		alternatingRowBackColorCount(NewInteger("alternatingRowBackColorCount")),
		defaultLineHeight(NewInteger("defaultLineHeight")),
		hasPictures(NewBoolean("hasPictures")),
		hasPlusMinus(NewBoolean("hasPlusMinus")),
		hasTreeLines(NewBoolean("hasTreeLines")),
		integralHeight(NewBoolean("integralHeight")),
		multiSelect(NewInteger("multiSelect")),
		nameSeparator(NewString("nameSeparator", 2)),
		pictureClosed(NewBinary("pictureClosed")),
		pictureLeaf(NewBinary("pictureLeaf")),
		pictureMinus(NewBinary("pictureMinus")),
		pictureOpen(NewBinary("pictureOpen")),
		picturePlus(NewBinary("picturePlus")),
		scrollBars(NewInteger("scrollBars")),
		scrollHorizontal(NewInteger("scrollHorizontal")),
		sortAsc(NewBoolean("sortAsc")),
		sortCased(NewBoolean("sortCased")),
		sorted(NewBoolean("sorted"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "displayEntry");
		new ExternalMethod(*this, nullptr, "displayRow");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "pictureClick");
		new ExternalMethod(*this, nullptr, "pictureDblClick");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	MultiMediaMeta::MultiMediaMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "MultiMedia", superclass),
		autoSize(NewBoolean("autoSize")),
		mediaData(NewBinary("mediaData")),
		mediaName(NewString("mediaName")),
		repeat(NewBoolean("repeat")),
		showMenu(NewBoolean("showMenu")),
		showMode(NewBoolean("showMode")),
		showName(NewBoolean("showName")),
		showOpenMenu(NewBoolean("showOpenMenu")),
		showPlayBar(NewBoolean("showPlayBar")),
		showPosition(NewBoolean("showPosition")),
		showRecord(NewBoolean("showRecord")),
		timerPeriod(NewInteger("timerPeriod")),
		useDotNetVersion(NewBoolean("useDotNetVersion")),
		zoom(NewInteger("zoom"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "notifyMedia");
		new ExternalMethod(*this, nullptr, "notifyMode");
		new ExternalMethod(*this, nullptr, "notifyPosition");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	OcxMeta::OcxMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "Ocx", superclass),
		autoSize(NewInteger("autoSize")),
		savedData(NewBinary("savedData"))
	{
		savedData->SetAlias("_savedData");

		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	OleControlMeta::OleControlMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "OleControl", superclass),
		activation(NewInteger("activation")),
		allowInPlace(NewBoolean("allowInPlace")),
		displayAsIcon(NewBoolean("displayAsIcon")),
		showMenu(NewBoolean("showMenu")),
		sizeMode(NewInteger("sizeMode"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "updated");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	OptionButtonMeta::OptionButtonMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "OptionButton", superclass),
		alignment(NewInteger("alignment")),
		autoSize(NewBoolean("autoSize")),
		caption(NewString("caption")),
		transparent(NewBoolean("transparent")),
		value(NewBoolean("value"))
	{
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	PictureMeta::PictureMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "Picture", superclass),
		clipControls(NewBoolean("clipControls")),
		hyperlink(NewString("hyperlink")),
		picture(NewBinary("picture")),
		pictureDisabled(NewBinary("pictureDisabled")),
		pictureDown(NewBinary("pictureDown")),
		rotation(NewReal("rotation")),
		scrollBars(NewInteger("scrollBars")),
		stretch(NewInteger("stretch")),
		transparent(NewBoolean("transparent")),
		transparentColor(NewInteger("transparentColor")),
		upAndDownSwapped(NewBoolean("upAndDownSwapped")),
		webFileName(NewString("webFileName"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	JadeMaskMeta::JadeMaskMeta(RootSchema& parent, const PictureMeta& superclass) : RootClass(parent, "JadeMask", superclass),
		activeColor(NewInteger("activeColor")),
		alignment(NewInteger("alignment")),
		canHaveFocus(NewBoolean("canHaveFocus")),
		cancel(NewBoolean("cancel")),
		caption(NewString("caption")),
		captionHeight(NewInteger("captionHeight")),
		captionLeft(NewInteger("captionLeft")),
		captionTop(NewInteger("captionTop")),
		captionWidth(NewInteger("captionWidth")),
		createRegionFromMask(NewBoolean("createRegionFromMask")),
		default_(NewBoolean("default")),
		disabledForeColor(NewInteger("disabledForeColor")),
		pictureFocus(NewBinary("pictureFocus")),
		pictureFocusDown(NewBinary("pictureFocusDown")),
		pictureMask(NewBinary("pictureMask")),
		pictureRollOver(NewBinary("pictureRollOver")),
		pictureRollUnder(NewBinary("pictureRollUnder")),
		style(NewInteger("style")),
		value(NewBoolean("value"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	ScrollBarMeta::ScrollBarMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "ScrollBar", superclass),
		largeChange(NewInteger("largeChange")),
		max_(NewInteger("max")),
		min_(NewInteger("min")),
		smallChange(NewInteger("smallChange")),
		value(NewInteger("value"))
	{
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	HScrollMeta::HScrollMeta(RootSchema& parent, const ScrollBarMeta& superclass) : RootClass(parent, "HScroll", superclass) {}

	VScrollMeta::VScrollMeta(RootSchema& parent, const ScrollBarMeta& superclass) : RootClass(parent, "VScroll", superclass) {}

	StatusLineMeta::StatusLineMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "StatusLine", superclass),
		alignment(NewInteger("alignment")),
		autoSize(NewBoolean("autoSize")),
		bevelColor(NewInteger("bevelColor")),
		bevelInner(NewInteger("bevelInner")),
		bevelInnerWidth(NewInteger("bevelInnerWidth")),
		bevelOuter(NewInteger("bevelOuter")),
		bevelOuterWidth(NewInteger("bevelOuterWidth")),
		bevelShadowColor(NewInteger("bevelShadowColor")),
		boundaryBrush(NewInteger("boundaryBrush")),
		boundaryColor(NewInteger("boundaryColor")),
		boundaryWidth(NewInteger("boundaryWidth")),
		caption(NewString("caption")),
		clipControls(NewBoolean("clipControls")),
		wordWrap(NewBoolean("wordWrap"))
	{
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	TableMeta::TableMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "Table", superclass),
		allowDrag(NewInteger("allowDrag")),
		allowResize(NewInteger("allowResize")),
		autoSize(NewInteger("autoSize")),
		columns(NewInteger("columns")),
		defaultRowHeight(NewInteger("defaultRowHeight")),
		displayHotKey(NewBoolean("displayHotKey")),
		dropDown(NewInteger("dropDown")),
		expandedHeight(NewInteger("expandedHeight")),
		fixed3D(NewBoolean("fixed3D")),
		fixedColumns(NewInteger("fixedColumns")),
		fixedRows(NewInteger("fixedRows")),
		gridColor(NewInteger("gridColor")),
		gridLines(NewBoolean("gridLines")),
		readOnly(NewBoolean("readOnly")),
		rows(NewInteger("rows")),
		scrollBars(NewInteger("scrollBars")),
		selectMode(NewInteger("selectMode")),
		sheets(NewInteger("sheets")),
		showFocus(NewBoolean("showFocus")),
		stretch(NewInteger("stretch")),
		tabActiveColor(NewInteger("tabActiveColor")),
		tabInactiveColor(NewInteger("tabInactiveColor")),
		tabKey(NewInteger("tabKey"))
	{
		new ExternalMethod(*this, nullptr, "cellInputReady");
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "closeup");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "displayRow");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "openup");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "queryColumnMove");
		new ExternalMethod(*this, nullptr, "queryRowColChg");
		new ExternalMethod(*this, nullptr, "queryRowMove");
		new ExternalMethod(*this, nullptr, "resizeColumn");
		new ExternalMethod(*this, nullptr, "resizeRow");
		new ExternalMethod(*this, nullptr, "rowColumnChg");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "sheetChg");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	TextBoxMeta::TextBoxMeta(RootSchema& parent, const ControlMeta& superclass) : RootClass(parent, "TextBox", superclass),
		alignment(NewInteger("alignment")),
		autoTab(NewBoolean("autoTab")),
		case_(NewInteger("case")),
		dataType(NewInteger("dataType")),
		decimals(NewInteger("decimals")),
		formatOut(NewString("formatOut", 101)),
		hintBackColor(NewInteger("hintBackColor")),
		hintForeColor(NewInteger("hintForeColor")),
		hintText(NewString("hintText")),
		integralHeight(NewBoolean("integralHeight")),
		maxLength(NewInteger("maxLength")),
		passwordField(NewBoolean("passwordField")),
		readOnly(NewBoolean("readOnly")),
		scrollBars(NewInteger("scrollBars")),
		scrollHorizontal(NewBoolean("scrollHorizontal")),
		scrollVertical(NewBoolean("scrollVertical")),
		selectionStyle(NewInteger("selectionStyle")),
		text(NewString("text")),
		textOffset(NewInteger("textOffset")),
		wantReturn(NewBoolean("wantReturn")),
		webInputType(NewCharacter("webInputType"))
	{
		new ExternalMethod(*this, nullptr, "change");
		new ExternalMethod(*this, nullptr, "click");
		new ExternalMethod(*this, nullptr, "contextMenu");
		new ExternalMethod(*this, nullptr, "dblClick");
		new ExternalMethod(*this, nullptr, "dragDrop");
		new ExternalMethod(*this, nullptr, "dragOver");
		new ExternalMethod(*this, nullptr, "firstChange");
		new ExternalMethod(*this, nullptr, "gotFocus");
		new ExternalMethod(*this, nullptr, "keyDown");
		new ExternalMethod(*this, nullptr, "keyPress");
		new ExternalMethod(*this, nullptr, "keyUp");
		new ExternalMethod(*this, nullptr, "lostFocus");
		new ExternalMethod(*this, nullptr, "mouseDown");
		new ExternalMethod(*this, nullptr, "mouseEnter");
		new ExternalMethod(*this, nullptr, "mouseHover");
		new ExternalMethod(*this, nullptr, "mouseLeave");
		new ExternalMethod(*this, nullptr, "mouseMove");
		new ExternalMethod(*this, nullptr, "mouseUp");
		new ExternalMethod(*this, nullptr, "paint");
		new ExternalMethod(*this, nullptr, "scrolled");
		new ExternalMethod(*this, nullptr, "sysNotify");
		new ExternalMethod(*this, nullptr, "userNotify");
	}

	WebHTMLMeta::WebHTMLMeta(RootSchema& parent, const TextBoxMeta& superclass) : RootClass(parent, "WebHTML", superclass),
		ignoreHeight(NewBoolean("ignoreHeight")),
		ignoreWidth(NewBoolean("ignoreWidth")),
		transparent(NewBoolean("transparent"))
	{
		new JadeMethod(*this, nullptr, "windowCreated");
	}
}