#pragma once
#include <jadegit/Exception.h>
#include <jadegit/Progress.h>
#include <Log.h>
#include <vector>
#include <unordered_map>
#include <functional>
#include <assert.h>
#include <algorithm>
#include <sstream>

namespace JadeGit
{
	template<class TVertex>
	class Graph
	{
	public:
		using Priority = std::function<int(const TVertex*)>;
		Graph(Priority priority = [](const TVertex*){ return 0; }) : priority(priority) {}

		void AddVertex(TVertex* vertex)
		{
			// Vertices must not be added twice
			assert(neighbours.find(vertex) == neighbours.end());

			// Add to graph and initialise adjacency list
			vertices.push_back(vertex);
			neighbours[vertex];
		}

		void AddEdge(TVertex* prerequisite, TVertex* dependent)
		{
			if (!prerequisite || !dependent || prerequisite == dependent)
				return;

			// Vertices must be explicitly added before edges
			assert(neighbours.find(prerequisite) != neighbours.end());
			assert(neighbours.find(dependent) != neighbours.end());

			// Add prerequisite neighbour
			neighbours[dependent].push_back(prerequisite);
		}

		bool isDependent(TVertex* dependent, TVertex* prerequisite)
		{
			if (!prerequisite || !dependent || prerequisite == dependent)
				return false;

			assert(neighbours.find(dependent) != neighbours.end());

			auto& neighbours = this->neighbours[dependent];
			if (std::find(neighbours.begin(), neighbours.end(), prerequisite) != neighbours.end())
				return true;

			for (auto& neighbour : neighbours)
			{
				if (isDependent(neighbour, prerequisite))
					return true;
			}

			return false;
		}

		bool traverse(std::function<bool(TVertex*)> visit, std::function<void(bool)> flush, IProgress* progress = nullptr, const char* task = nullptr) const
		{
			size_t size = vertices.size();

			if (progress && !progress->start(size, task))
				return false;

			// Copy/sort vertices by priority
			std::vector<TVertex*> vertices = this->vertices;
			std::stable_sort(vertices.begin(), vertices.end(), [this](const TVertex* a, const TVertex* b) { return priority(b) < priority(a); });

			VertexMap<State> states;
			states.reserve(size);

			size_t visited = 0;
			size_t before = 0;

			do
			{
				// Remember how many have been successfully processed
				before = visited;

				if (visited)
				{
					// Perform flush operation between traversals
					if (flush)
						flush(false);

					// Reset those deferred previously
					for (auto& pair : states)
						if (pair.second == Deferred)
							pair.second = Incomplete;
				}

				// Traverse all vertices
				bool aborted = false;
				for (TVertex* vertex : vertices)
				{
					traverseVertex(vertex, states, visited, visit, aborted, progress);
					if (aborted)
						return false;
				}

			} while (visited < size && before < visited && flush);

			// Throw exception when we've failed to visit everything
			if (size != visited)
			{
				// TODO: Log warning messages somewhere to identify unhandled components
				throw jadegit_exception("Graph traversal failed");
			}

			// Perform final flush operation
			if (flush)
				flush(true);

			return !progress || progress->finish();
		}
		
		bool tryAddEdge(TVertex* prerequisite, TVertex* dependent)
		{
			if (isDependent(prerequisite, dependent))
				return false;

			AddEdge(prerequisite, dependent);
			return true;
		}

	private:
		Priority priority;

		template<typename T>
		using VertexMap = std::unordered_map<TVertex*, T>;

		std::vector<TVertex*> vertices;
		VertexMap<std::vector<TVertex*>> neighbours;
	
		enum State { Incomplete, Processing, Deferred, Completed };

		bool traverseVertex(TVertex* vertex, VertexMap<State>& states, size_t &visited, std::function<bool(TVertex*)> visit, bool& aborted, IProgress* progress) const
		{
			assert(!aborted);

			static thread_local int depth = -1;

			State& state = states[vertex];
			
			switch (state)
			{
			case Completed:
				return true;
			case Deferred:
				return false;
			case Processing:
			{
				LOG_DEBUG("build: " << std::string(depth + 1, '\t') << *vertex << " (cyclic!)");
				throw jadegit_exception("cycle in graph");
			}
			default:
				state = Processing;
			}

			depth++;
			LOG_DEBUG("build: " << std::string(depth, '\t') << *vertex << " (enter)");

			state = Processing;
			bool result = true;

			// Copy/sort neighbours by priority
			std::vector<TVertex*> neighbours = this->neighbours.at(vertex);
			std::stable_sort(neighbours.begin(), neighbours.end(), [this](const TVertex * a, const TVertex * b) { return priority(b) < priority(a); });

			// Try visit all neighbours
			for (TVertex* neighbour : neighbours)
			{
				result = traverseVertex(neighbour, states, visited, visit, aborted, progress) && result;
				if (aborted)
					return false;
			}

			// Try visit vertex, provided all neighbours were visited
			if (result = result && visit(vertex))
			{
				visited++;

				// Increment progress, aborting traversal if requested
				if (aborted = (progress && !progress->step()))
					return false;
			}

			LOG_DEBUG("build: " << std::string(depth, '\t') << *vertex << (result ? " (completed)" : " (deferred)"));
			depth--;

			state = result ? Completed : Deferred;
			return result;
		}
	};
}