#include "Function.h"
#include <dev/Development.h>
#include <dev/MsgBox.h>
#include <dev/PatchControl/PatchControl.h>
#include <dev/Session.h>
#include <jadegit/Exception.h>
#include <schema/data/Schema.h>
#include <Log.h>
#include <map>

using namespace std;
using namespace Jade;

namespace JadeGit::Development
{
	// Default function to use when one hasn't been explicitly registered
	class DefaultFunction : public Function
	{
	public:
		using Function::Function;

	protected:
		bool execute(const string& entityName) const final
		{
			// Convert entity name to qualified name
			QualifiedName qualifiedName(entityName);

			// Get schema name
			auto& name = qualifiedName.first().name;

			// Allow unsupported functions for RootSchema (which cannot be changed)
			if (name == "RootSchema")
				return true;

			// Allow unsupported functions for schemas excluded from source control
			// NOTE: This includes JadeGitSchema, updates to which are blocked via patch control hook
			if (Schema::GitSchema::status(name) == Schema::GitSchema::Status::Excluded)
				return true;

			// Unimplemented function
			throw jadegit_unimplemented_feature();
		}
	};
	static DefaultFunction defaultFunction;

	static map<string, Function*> registry;

	Function::Function(std::initializer_list<const char*> taskNames)
	{
		for (auto taskName : taskNames)
		{
			if (registry.find(taskName) != registry.end())
				throw jadegit_exception("Duplicate function task detected");

			registry[taskName] = this;
		}
	}

	Function& Function::get(const string& taskName)
	{
		auto iter = registry.find(taskName);
		return (iter != registry.end()) ? *iter->second : defaultFunction;
	}

	bool SessionFunction::execute(const string& entityName) const
	{	
		return execute(Session::resolve(), entityName);
	}

	int jadeFunctionSelected(const Character* pUserName, const Character* pTaskName, const Character* pEntityName)
	{
		try
		{
			auto userName = narrow(pUserName);
			auto taskName = narrow(pTaskName);
			auto entityName = narrow(pEntityName);

			LOG_INFO("FunctionSelected: userName=" << userName << ", taskName=" << taskName << ", entityName=" << entityName);

			// Reset entity details captured prior to last update
			if (!PatchControl::get().prelude(0, std::string()))
				return JADEGIT_DENY;

			return Function::get(taskName).execute(entityName) ? JADEGIT_ALLOW : JADEGIT_DENY;
		}
		catch (std::runtime_error& e)
		{
			MsgBox::display(e);
		}

		return JADEGIT_ERROR;
	};
}