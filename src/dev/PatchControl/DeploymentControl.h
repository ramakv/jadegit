#pragma once
#include "PatchControl.h"

namespace JadeGit::Development
{
	// DeploymentControl handles checks while deployment is in progress
	class DeploymentControl : public PatchControl
	{
	protected:
		bool execute(const std::string& entityType, const QualifiedName& entityName, const std::string& operation) override;
	};
}