#pragma once
#include "EntityControl.h"

namespace JadeGit::Development
{
	class FormControl : public EntityControl
	{
	protected:
		bool prelude(const ClassNumber& entityType, const QualifiedName& entityName, bool removal) final;
		bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation) final;
	
	private:
		bool adding = false;
		std::unique_ptr<QualifiedName> form;
		std::unique_ptr<Extract::Entity> guiClass;
		std::string method;
	};
}