#include "FormControl.h"
#include <dev/Session.h>
#include <extract/EntityFactory.h>
#include <schema/data/Change.h>

using namespace std;

namespace JadeGit::Development
{
	bool FormControl::prelude(const ClassNumber& entityType, const QualifiedName& entityName, bool removal)
	{
		adding = false;
		form.reset();
		guiClass.reset();

		// Pass onto next in chain, supplying class, if form is about to be removed
		if (entityType == DSKFORM && removal)
			return PatchControl::prelude(DSKGUICLASS, QualifiedName(string(entityName.first()) + "::" + entityName.name), true);

		// Pass onto next in chain if form isn't being saved
		if (entityType != DSKFORM)
			return PatchControl::prelude(entityType, entityName, removal);

		// Store current form name, indicating save is in progress
		form = make_unique<QualifiedName>(string(entityName));

		// Remember oid/name association for rename detection
		return EntityControl::prelude(entityType, entityName, removal);
	}

	bool FormControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		// Handle changes during form save
		if (form)
		{
			// Handle class update
			if (entityType == "GUIClass")
			{
				// Resolve class (may have been renamed)
				guiClass = EntityControl::resolve(entityType, entityName);

				// Register change directly, making note of whether form class is actually being added (implies what operation to use when staging SFBC operation below)
				adding = EntityControl::change(guiClass, operation).isAdd();
				return true;
			}
			// Handle form update indicated by SFBC (save form before commit)
			// Entity type supplied in this scenario is the new unqualified form name
			else if (operation == "SFBC")
			{
				assert(string(form->name) == entityName.name);
				return EntityControl::execute(session, "Form", string(*entityName.parent) + "::" + entityType, adding ? "A" : "U");
			}
			// Handle form copy indicated by SFABC (save form as before commit)
			else if (operation == "SFABC")
			{
				return EntityControl::execute(session, "Form", entityName, "A");
			}
			// Handle nested control rename operation, which implies property rename
			else if (operation == "CR")
			{
				// Resolve class based on form name if required
				if (!guiClass)
					guiClass = EntityControl::resolve("GUIClass", QualifiedName(string(form->first()) + "::" + form->name));

				// Resolve control property being renamed (entityType being the new name)
				auto property = Extract::EntityFactory::Get().resolve(DSKIMPLICITINVERSEREF, guiClass.get(), entityType);

				// Register rename
				EntityControl::rename(*property, entityName.name);
				return true;
			}
			// Handle control event methods being renamed
			else if (entityType == "JadeMethod")
			{
				// Store method being renamed
				if (operation == "MRBC")
				{
					method = entityName;
					PatchControl::prelude(DSKJADEMETHOD, entityName, false);
					return true;
				}
				// Ignore redundant update operation with original method name
				else if (operation == "U" && method == string(entityName))
					return true;
			}
		}
		// Ignore nested control rename operations when form save has been ignored (passed through from schema control without status check)
		else if (operation == "CR")
			return true;

		// Pass onto next in chain
		return PatchControl::execute(session, entityType, entityName, operation);
	}
}