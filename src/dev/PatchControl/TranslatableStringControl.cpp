#include "TranslatableStringControl.h"
#include <jade/Transaction.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Development
{
	bool TranslatableStringControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		if (operation != "D" || entityType != "TranslatableString")
			return PatchControl::execute(session, entityType, entityName, operation);

		// Need to explictly start transaction as hook for 'D'elete operation called outside of transaction state
		// This is a work-around, it puts source control at risk as there's no guarantee everything is committed/aborted atomically
		Transaction transaction;
		if (PatchControl::execute(session, entityType, entityName, operation))
		{
			transaction.commit();
			return true;
		}

		return false;
	}
}