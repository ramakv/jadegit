#include "Log.h"
#include "Environment.h"
#include <iostream>
#include <ctime>
#include <thread>
#include <process.h>
#include <iomanip>
#include <filesystem>
#include <fstream>

using namespace std;

namespace JadeGit
{
	filesystem::path directory;
	fstream logfile;

	void setLogDirectory(const filesystem::path& path)
	{
		if (logfile.is_open())
			logfile.close();

		directory = path;
	}

	filesystem::path getLogDirectory()
	{
		// Use directory specified
		if (!directory.empty())
			return directory;

		// Default to logs folder within home directory
		return getHomeDirectory() / "logs";
	}

	void openLog()
	{
		// Check log isn't already open
		if (logfile.is_open())
			return;

		// Determine log directory
		auto path = getLogDirectory();

		// Ensure directories have been created
		filesystem::create_directories(path);

		// Open log
		logfile.open(path / "jadegit.log", fstream::app);
	}

	void log(const string& message)
	{
		// TODO: Implement numbered rolling logs, where as the log file reaches a certain size (i.e. 1 MB), we want to rollover to a new log (like Jade does).
		// For which, we need to have a thread level variable for the current number, to compare to a shared variable to know when we've rolled over, in which
		// case the log file needs to be re-opened.  After we've logged a message, we need to check if we've just exceed the file size, in which case increase
		// shared variable, so all threads rollover when they next log a message.

		// Open log if required
		openLog();

		// Get time
		time_t rawtime;
		time(&rawtime);

		struct tm timeinfo;
		localtime_s(&timeinfo, &rawtime);

		char buffer[80];
		strftime(buffer, sizeof(buffer), "%x %X ", &timeinfo);

		// Log message, following time and process/thread ID's
		logfile << buffer << std::setfill('0') << std::setw(5) << std::hex << ::_getpid() << "-" << std::setw(4) << std::hex << std::this_thread::get_id() << " " << message << std::endl;
	}
}