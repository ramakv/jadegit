#pragma once
#include "EntityFactory.h"
#include "ObjectRegistration.h"

namespace JadeGit::Extract
{
	template <class TDerived>
	class EntityRegistration : protected ObjectRegistration<TDerived, EntityFactory::Registration>
	{
	public:
		EntityRegistration(const ClassNumber& key) : ObjectRegistration<TDerived, EntityFactory::Registration>(key)
		{
			EntityFactory::Get().Register(key, this);
		}

		TDerived* Create() const override
		{
			return new TDerived();
		}
	};
}