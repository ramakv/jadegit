#include "Assembly.h"
#include <jadegit/Version.h>
#include <jombuild.h>
#include "DataMap.h"
#include "Platform.h"

namespace JadeGit::Extract
{
	Assembly::Assembly(const FileSystem& source) : Data::Assembly(source, jadeVersion)
	{
	}

	Assembly::~Assembly()
	{
		for (auto maps : dataMaps)
			delete maps.second;
	}

	void Assembly::unload()
	{
		// Unload data maps for non-static schemas which are about to be unloaded
		auto it = dataMaps.begin();
		while (it != dataMaps.end())
		{
			if (!it->second->cls->isStatic())
			{
				delete it->second;
				it = dataMaps.erase(it);
			}
			else
				it++;
		}

		Data::Assembly::unload();
	}
}