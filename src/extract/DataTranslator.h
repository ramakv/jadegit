#pragma once
#include <Singleton.h>
#include <jomobj.hpp>
#include <string>

namespace JadeGit::Data
{
	class Any;
	class Property;
	class Object;
}

namespace JadeGit::Extract
{
	class Assembly;
	class Object;

	class DataTranslator
	{
	public:
		DataTranslator(bool basic) : basic(basic) {}

		const bool basic;

		virtual void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const = 0;
	};

	class AttributeTranslator : public DataTranslator, public Singleton<AttributeTranslator>
	{
	public:
		AttributeTranslator() : DataTranslator(true) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;

	private:
		Data::Any Translate(const DskParam& value) const;
	};

	class ChildTranslator : public DataTranslator
	{
	protected:
		ChildTranslator() : DataTranslator(false) {}

		bool Copy(Assembly& assembly, Data::Object* parent, const DskObjectId& oid, const std::string& name, int index, const std::string& trail, bool deep) const;
	};

	class ChildReferenceTranslator : public ChildTranslator, public Singleton<ChildReferenceTranslator>
	{
	public:
		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;
	};

	class ChildCollectionTranslator : public ChildTranslator, public Singleton<ChildCollectionTranslator>
	{
	public:
		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;
	};

	class PeerReferenceTranslator : public DataTranslator, public Singleton<PeerReferenceTranslator>
	{
	public:
		PeerReferenceTranslator() : DataTranslator(true) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;
	};

	class PeerCollectionTranslator : public DataTranslator, public Singleton<PeerCollectionTranslator>
	{
	public:
		PeerCollectionTranslator() : DataTranslator(false) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;
	};
}