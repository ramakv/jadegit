#include "Property.h"
#include "Type.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/JadeDynamicCompAttributeMeta.h>
#include <jadegit/data/RootSchema/JadeDynamicPrimAttributeMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	bool hasUnboundedLength(const DskAttribute& attribute)
	{
		if (!attribute.isPrimAttribute())
			return false;

		DskParam param;
		paramSetBoolean(param);
		jade_throw(attribute.sendMsg(TEXT("hasUnboundedLength"), nullptr, &param));

		bool result;
		paramGetBoolean(param, result);
		return result;
	}

	bool hasSpecificLength(const DskAttribute& attribute)
	{
		if (hasUnboundedLength(attribute))
			return false;

		DskType type;
		jade_throw(attribute.getType(type));
		return !hasPredefinedLength(type);
	}

	static DataMapper<AttributeMeta> attributeMapper(DSKATTRIBUTE, &RootSchema::attribute, {
		// Extract length where it's been specified and not predefined by type
		{PRP_Attribute_length, new DataProperty(&AttributeMeta::length, [](const DskObject& object) { return hasSpecificLength(DskAttribute(&object.oid)); })}
		});

	static DataMapper<JadeDynamicCompAttributeMeta> dynamicCompAttributeMapper(DSKJADEDYNAMICCOMPATTRIBUTE, &RootSchema::jadeDynamicCompAttribute, {
		{PRP_JadeDynamicCompAttribute_dynamicPropertyCluster, new DataProperty(&JadeDynamicCompAttributeMeta::dynamicPropertyCluster)}
		});

	bool hasDecimalType(const DskAttribute& attribute)
	{
		DskType type;
		jade_throw(attribute.getType(type));

		TypeNum number = 0;
		jade_throw(type.getNumber(&number));
		return number == DSKDECIMAL_PRIMITIVE;
	}

	static DataMapper<PrimAttributeMeta> primAttributeMapper(DSKPRIMATTRIBUTE, &RootSchema::primAttribute, {
		{PRP_PrimAttribute_precision, new DataProperty(&PrimAttributeMeta::precision, [](const DskObject& object) { return hasDecimalType(DskAttribute(&object.oid)); })},
		{PRP_PrimAttribute_scaleFactor, new DataProperty(&PrimAttributeMeta::scaleFactor)},
		{PRP_PrimAttribute__xmlType, new DataProperty(&PrimAttributeMeta::xmlType)}
		});

	static DataMapper<JadeDynamicPrimAttributeMeta> dynamicPrimAttributeMapper(DSKJADEDYNAMICPRIMATTRIBUTE, &RootSchema::jadeDynamicPrimAttribute, {
		{PRP_JadeDynamicPrimAttribute_dynamicPropertyCluster, new DataProperty(&JadeDynamicPrimAttributeMeta::dynamicPropertyCluster)}
		});

	static EntityRegistration<Property> compAttribute(DSKCOMPATTRIBUTE);
	static EntityRegistration<Property> primAttribute(DSKPRIMATTRIBUTE);

	class JadeDynamicCompAttribute : public Property
	{
	public:
		using Property::Property;

	protected:
		bool isShallow() const final
		{
			return getProperty<bool>(PRP_JadeDynamicCompAttribute_dynamicDefinition);
		}
	};
	static EntityRegistration<JadeDynamicCompAttribute> dynamicCompAttribute(DSKJADEDYNAMICCOMPATTRIBUTE);

	class JadeDynamicPrimAttribute : public Property
	{
	public:
		using Property::Property;

	protected:
		bool isShallow() const final
		{
			return getProperty<bool>(PRP_JadeDynamicPrimAttribute_dynamicDefinition);
		}
	};
	static EntityRegistration<JadeDynamicPrimAttribute> dynamicPrimAttribute(DSKJADEDYNAMICPRIMATTRIBUTE);
}
