#include "Locale.h"
#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/LocaleMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<LocaleMeta> mapper(DSKLOCALE, &RootSchema::locale, {
		{PRP_Locale__priorForms, nullptr},
		{PRP_Locale__transStringsByNumber, nullptr},
		{PRP_Locale_languageId, nullptr}
		});

	static EntityRegistration<Locale> registrar(DSKLOCALE);
	
	bool Locale::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_locales);
	}
}