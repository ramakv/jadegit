#include "Library.h"
#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/LibraryMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<LibraryMeta> mapper(DSKLIBRARY, &RootSchema::library, {
		/* Routines are extracted as children of schema or type */
		{PRP_Library_entrypoints, nullptr} 
		});

	static EntityRegistration<Library> registrar(DSKLIBRARY);

	bool Library::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_libraries);
	}

	void Library::dependents(std::set<DskObjectId>& dependents) const
	{
		SchemaEntity::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_Library_entrypoints, dependents);
	}
}