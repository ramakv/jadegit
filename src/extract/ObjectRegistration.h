#pragma once
#include "ObjectFactory.h"

namespace JadeGit::Extract
{
	template <class TDerived, class TInterface = ObjectFactory::Registration>
	class ObjectRegistration : public TInterface
	{
	public:
		ObjectRegistration(const ClassNumber& key)
		{
			ObjectFactory::Get().Register(key, this);
		}

		TDerived* Create(const DskObjectId& oid) const override
		{
			return new TDerived(oid);
		}
	};
}