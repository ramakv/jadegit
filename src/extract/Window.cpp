#include "Locale.h"
#include "Type.h"
#include "DataMapper.h"
#include "DataTranslator.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/FormMeta.h>
#include <jadegit/data/RootSchema/ActiveXControlMeta.h>
#include <jadegit/data/RootSchema/FrameMeta.h>
#include <jadegit/data/RootSchema/FolderMeta.h>
#include <jadegit/data/RootSchema/ComboBoxMeta.h>
#include <jadegit/data/RootSchema/ListBoxMeta.h>
#include <jadegit/data/RootSchema/OcxMeta.h>
#include <jadegit/data/RootSchema/OleControlMeta.h>
#include <jadegit/data/RootSchema/TableMeta.h>
#include <jadegit/data/RootSchema/MenuItemMeta.h>
#include <Log.h>

using namespace Jade;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	// Custom translator for MenuItem::shortCutKey which handles fixing legacy systems where
	// shortCutKey is 63 ('?' Unknown/Invalid), which should now be 0 '(none)'.
	class MenuItemShortcutTranslator : public DataTranslator, public Singleton<MenuItemShortcutTranslator>
	{
	public:
		MenuItemShortcutTranslator() : DataTranslator(true) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final
		{
			assert(property);
			LOG_DEBUG("Extracting attribute: shortCutKey");

			assert(value.header.format == PFormat::DSKCHARACTER);
			assert(name == "shortCutKey");

			char shortCutKey = static_cast<char>(value.body.character);

			if (shortCutKey == '?')
			{
				shortCutKey = 0;	// Reset to '(none)'
			}

			target->SetValue(property, shortCutKey);
		}
	};

	static DataMapper<WindowMeta> windowMapper(DSKWINDOW, &RootSchema::window, {
		{PRP_Window___children, nullptr},
		{PRP_Window___userObject, nullptr},
		{PRP_Window__hyperText, nullptr},
		{PRP_Window__sysDHTMLScript, nullptr},
		{PRP_Window__webEventMappings, nullptr},
		{PRP_Window_allControlChildren, nullptr},
		{PRP_Window_controlChildren, nullptr},		// Covered by Form::controlList
		{PRP_Window_userObject, nullptr},			// Runtime
		{PRP_Window_userScript, nullptr}			// Runtime
		});

	static DataMapper<FormMeta> formMapper(DSKFORM, &RootSchema::form, {
		{PRP_Form__saveControlData, nullptr},
		{PRP_Form_allMenuItems, nullptr},
		{PRP_Form_applications, nullptr},
		{PRP_Form_controlList, new DataProperty<FormMeta>(nullptr)},
		{PRP_Form_mdiPinned, nullptr},				// Runtime
		{PRP_Form_menuList, new DataProperty<FormMeta>(nullptr)},
		{PRP_Form_paintedDialogBaseUnits, nullptr},
		{PRP_Form_topLevelMenuItems, nullptr},		// Covered by menuList
		{PRP_Form_userProfile, nullptr},
		});

	static DataMapper<ControlMeta> controlMapper(DSKCONTROL, &RootSchema::control, {
		{PRP_Control_index, nullptr},
		{PRP_Control_parent, new DataProperty(&ControlMeta::parent)}
		});

	static DataMapper<ActiveXControlMeta> activeXControl(DSKACTIVEXCONTROL, &RootSchema::activeXControl, {
		{PRP_ActiveXControl__savedData, new DataProperty(&ActiveXControlMeta::savedData)},
		{PRP_ActiveXControl_usePresentationClient, nullptr}
		});

	static DataMapper<ComboBoxMeta> comboBox(DSKCOMBOBOX, &RootSchema::comboBox, {
		// Runtime properties
		{PRP_ComboBox_itemBackColor, nullptr},
		{PRP_ComboBox_itemData, nullptr},
		{PRP_ComboBox_itemEnabled, nullptr},
		{PRP_ComboBox_itemExpanded, nullptr},
		{PRP_ComboBox_itemForeColor, nullptr},
		{PRP_ComboBox_itemLevel, nullptr},
		{PRP_ComboBox_itemObject, nullptr},
		{PRP_ComboBox_itemPicture, nullptr},
		{PRP_ComboBox_itemPictureType, nullptr},
		{PRP_ComboBox_itemText, nullptr}
		});

	static DataMapper<FolderMeta> folder(DSKFOLDER, &RootSchema::folder, {
		{PRP_Folder__webHorizontalSheets, nullptr}
		});

	static DataMapper<FrameMeta> frame(DSKFRAME, &RootSchema::frame, {
		{PRP_Frame__createdControls, nullptr},
		{PRP_Frame__generatedHTML, nullptr}
		});

	static DataMapper<ListBoxMeta> listBox(DSKLISTBOX, &RootSchema::listBox, {
		// Runtime properties
		{PRP_ListBox_itemBackColor, nullptr},
		{PRP_ListBox_itemData, nullptr},
		{PRP_ListBox_itemEnabled, nullptr},
		{PRP_ListBox_itemExpanded, nullptr},
		{PRP_ListBox_itemForeColor, nullptr},
		{PRP_ListBox_itemLevel, nullptr},
		{PRP_ListBox_itemObject, nullptr},
		{PRP_ListBox_itemPicture, nullptr},
		{PRP_ListBox_itemPictureType, nullptr},
		{PRP_ListBox_itemSelected, nullptr},
		{PRP_ListBox_itemText, nullptr}
		});

	static DataMapper<OcxMeta> ocx(DSKOCX, &RootSchema::ocx, {
		{PRP_Ocx__savedData, new DataProperty(&OcxMeta::savedData)}
		});

	static DataMapper<OleControlMeta> oleControl(DSKOLECONTROL, &RootSchema::oleControl, {
		// Jade seems to ignore this one when extracting/building DDB
		{PRP_OleControl_oleObject, nullptr}
		});

	static DataMapper<TableMeta> table(DSKTABLE, &RootSchema::table, {
		{PRP_Table__hyperlinkRowColumn, nullptr},
		{PRP_Table_accessedCell, nullptr},
		{PRP_Table_accessedColumn, nullptr},
		{PRP_Table_accessedRow, nullptr},
		{PRP_Table_accessedSheet, nullptr},
		{PRP_Table_columnVisible, nullptr},
		{PRP_Table_columnWidth, nullptr},
		{PRP_Table_hyperlinkColumn, nullptr},
		{PRP_Table_rowHeight, nullptr},
		{PRP_Table_rowVisible, nullptr},
		{PRP_Table_sheetVisible, nullptr},
		{PRP_Table_sortAsc, nullptr},
		{PRP_Table_sortCased, nullptr},
		{PRP_Table_sortColumn, nullptr},
		{PRP_Table_sortType, nullptr}
		});

	static DataMapper<MenuItemMeta> mapper(DSKMENUITEM, &RootSchema::menuItem, {
		{PRP_MenuItem__eventMappings, nullptr},
		{PRP_MenuItem_allChildren, nullptr},
		{PRP_MenuItem_children, nullptr},
		{PRP_MenuItem_commandId, new DataProperty(&MenuItemMeta::commandId)},
		{PRP_MenuItem_hasSubMenu, new DataProperty(&MenuItemMeta::hasSubMenu)},
		{PRP_MenuItem_helpList, new DataProperty(&MenuItemMeta::helpList)},
		{PRP_MenuItem_index, nullptr},
		{PRP_MenuItem_level, new DataProperty(&MenuItemMeta::level)},
		{PRP_MenuItem_shortCutFlags, new DataProperty(&MenuItemMeta::shortCutFlags)},
		{PRP_MenuItem_shortCutKey, new DataProperty(&MenuItemMeta::shortCutKey, nullptr, MenuItemShortcutTranslator::Instance())},
		{PRP_MenuItem_windowList, new DataProperty(&MenuItemMeta::windowList)},
		{PRP_MenuItem_userObject, nullptr}
		});

	class Window : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const override
		{
			return getProperty<std::string>(PRP_Window_name);
		}
	};

	class Form : public Window
	{
	public:
		using Window::Window;

		// Always perform deep extract for forms (to include controls which aren't updated individually)
		void Extract(Assembly& assembly, bool deep) const override
		{
			Entity::Extract(assembly, true);
		}

		// Renaming forms implicitly renames associated class
		void GetNamesakes(std::set<DskObjectId>& namesakes) const override
		{
			Entity::GetNamesakes(namesakes);

			// Resolve schema
			Locale locale(GetParentId());
			DskSchema schema;
			locale.GetParentId(schema.oid);

			// Lookup class
			DskClass klass;
			jade_throw(schema.getLocalClass(widen(getName()).c_str(), klass));

			// Add class to namesakes
			namesakes.insert(klass.oid);
		}

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			return Entity::lookup<Locale>(ancestor, path, PRP_Locale_forms);
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_Form_locale);
		}
	};
	static EntityRegistration<Form> form(DSKFORM);

	class Control : public Window
	{
	public:
		using Window::Window;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override
		{
			// Need to explicitly lookup control by name
			throw jadegit_unimplemented_feature();
		}

		DskObjectId GetParentId() const override
		{
			return getProperty<DskObjectId>(PRP_Control_form);
		}
	};
	static EntityRegistration<Control> control(DSKCONTROL);

	class MenuItemData : public Object
	{
	public:
		using Object::Object;
	};

	class MenuItem : public MenuItemData
	{
	public:
		using MenuItemData::MenuItemData;
	};
	static ObjectRegistration<MenuItem> menuItem(DSKMENUITEM);
}
