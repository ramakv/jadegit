#include "Type.h"
#include "DataMapper.h"
#include <jadegit/data/RootSchema/ScriptMeta.h>
#include <jadegit/data/RootSchema/ParameterMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ScriptMeta> scriptMapper(DSKSCRIPT, &RootSchema::script, {
		{PRP_Script_compiledOK, nullptr},
		{PRP_Script_compilerVersion, nullptr},
		{PRP_Script_errorCode, nullptr},
		{PRP_Script_errorLength, nullptr},
		{PRP_Script_errorPosition, nullptr},
		{PRP_Script_inlineTypeUsages, nullptr},
		{PRP_Script_notImplemented, new DataProperty(&ScriptMeta::notImplemented)},
		{PRP_Script_source, new DataProperty(&ScriptMeta::source)},
		{PRP_Script_status, nullptr},
		{PRP_Script_warningCount, nullptr}
		});

	static DataMapper<ScriptElementMeta> scriptElementMapper(DSKSCRIPTELEMENT, &RootSchema::scriptElement, {
		{PRP_ScriptElement__systemBasic, nullptr},
		{PRP_ScriptElement_textLength, nullptr},
		{PRP_ScriptElement_textPosition, nullptr}
		});
}