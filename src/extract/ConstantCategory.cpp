#include "ConstantCategory.h"
#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/ConstantCategoryMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ConstantCategoryMeta> mapper(DSKCONSTANTCATEGORY, &RootSchema::constantCategory, {
		/* Global constants are extracted as children of schema */
		{PRP_ConstantCategory_consts, nullptr}
		});

	static EntityRegistration<ConstantCategory> registrar(DSKCONSTANTCATEGORY);

	bool ConstantCategory::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_constantCategories);
	}

	void ConstantCategory::dependents(std::set<DskObjectId>& dependents) const
	{
		SchemaEntity::dependents(dependents);

		// Direct dependencies
		getProperty(PRP_ConstantCategory_consts, dependents);
	}
}