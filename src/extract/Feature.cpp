#include "Feature.h"

namespace JadeGit::Extract
{
	void Feature::dependents(std::set<DskObjectId>& dependents) const
	{
		SchemaEntity::dependents(dependents);

		getProperty(PRP_Feature_exposedFeatureRefs, dependents);
	}
}