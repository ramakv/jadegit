# Configure build header using project variables
configure_file(${CMAKE_CURRENT_LIST_DIR}/build.h.in ${CMAKE_CURRENT_BINARY_DIR}/include/jadegit/build.h)

# Retrieve the current commit
execute_process(
  COMMAND git rev-parse HEAD
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  OUTPUT_VARIABLE GIT_COMMIT_ID
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Retrieve the repository URL
execute_process(
  COMMAND git remote get-url origin
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  OUTPUT_VARIABLE GIT_REPOSITORY_URL
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Configure build source using git variables retrieved above
configure_file(${CMAKE_CURRENT_LIST_DIR}/build.cpp.in ${CMAKE_CURRENT_BINARY_DIR}/build.cpp)

# Setup common object library
add_library(jadegit OBJECT "")
target_compile_features(jadegit PUBLIC cxx_std_20)
target_compile_definitions(jadegit PRIVATE jadegit_EXPORTS)
target_include_directories(jadegit
	PUBLIC
	$<BUILD_INTERFACE:${jadegit_SOURCE_DIR}/include>
	$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
	$<INSTALL_INTERFACE:include>
	PRIVATE
	${jadegit_SOURCE_DIR}/src
)

# Use precompiled headers
target_precompile_headers(jadegit PRIVATE
	<algorithm>
	<chrono>
	<filesystem>
	<functional>
	<future>
	<iomanip>
	<iostream>
	<list>
	<new>
	<map>
	<memory>
	<mutex>
	<optional>
	<queue>
	<set>
	<sstream>
	<stack>
	<stdexcept>
	<string>
	<string_view>
	<type_traits>
	<typeindex>
	<vector>
)

# Link dependencies
if(USE_JADE)
	target_link_libraries(jadegit PUBLIC jade)
	target_precompile_headers(jadegit PRIVATE <jomobj.hpp>)
endif()

if(USE_GIT2)
	target_link_libraries(jadegit PUBLIC libgit2package)
	target_precompile_headers(jadegit PRIVATE <git2.h>)
endif()

# Add main modules
target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_BINARY_DIR}/build.cpp
	${CMAKE_CURRENT_LIST_DIR}/Environment.cpp
	${CMAKE_CURRENT_LIST_DIR}/Exception.cpp
	${CMAKE_CURRENT_LIST_DIR}/Log.cpp
	${CMAKE_CURRENT_LIST_DIR}/MemoryAllocated.cpp
	${CMAKE_CURRENT_LIST_DIR}/Progress.cpp
	${CMAKE_CURRENT_LIST_DIR}/Version.cpp
)
include(${CMAKE_CURRENT_LIST_DIR}/auth/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/build/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/data/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/deploy/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/extract/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/registry/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/schema/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/vfs/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/xml/CMakeLists.txt)

# Add modules that may link to shared schema library
include(${CMAKE_CURRENT_LIST_DIR}/console/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/dev/CMakeLists.txt)