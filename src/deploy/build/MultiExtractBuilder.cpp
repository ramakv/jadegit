#include "MultiExtractBuilder.h"
#include <jadegit/vfs/File.h>
#include <jadegit/Exception.h>

namespace JadeGit::Deploy
{
	MultiExtractBuilder::MultiExtractBuilder(const FileSystem& fs, const std::string& name, bool supportCommandFiles) : DirectoryBuilder(fs, ""), name(name), supportCommandFiles(supportCommandFiles)
	{
	}

	void MultiExtractBuilder::start()
	{
		// Setup mul file
		mul = fs.open(name + ".mul").createOutputStream();
		*mul << "#MULTIPLE_SCHEMA_EXTRACT" << std::endl;
	}

	void MultiExtractBuilder::finish()
	{
		if (mul)
			*mul << std::flush;
	}

	void MultiExtractBuilder::addCommandFile(const std::filesystem::path& jcf, const char* loadStyle)
	{
		if (!supportCommandFiles)
			throw jadegit_unsupported_feature("Loading command files during MUL deployment");

		*mul << jcf.string() << std::endl;
	}
	
	void MultiExtractBuilder::addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle)
	{
		if (!scm.empty() && !ddb.empty())
			*mul << scm.string() << " " << ddb.string() << std::endl;
		else if (!scm.empty())
			*mul << scm.string() << std::endl;
		else if (!ddb.empty())
			*mul << ddb.string() << std::endl;
	}
}