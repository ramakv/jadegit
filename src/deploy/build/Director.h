#pragma once
#include <jadegit/Progress.h>
#include <vfs/GitFileSystem.h>

namespace JadeGit::Registry
{
	class Manager;
}

namespace JadeGit::Deploy
{
	using namespace std;

	class Builder;

	class Director
	{
	public:
		Director(git_repository& repo, Builder& builder, IProgress* progress = nullptr);

		bool build(const string& revision) const;
		bool build(const git_commit* from, const git_commit* to, const FileSignature* signature = nullptr) const;
		bool build(Registry::Manager registry, const string& revision) const;
		bool build(Registry::Manager registry, unique_ptr<git_commit> revision, const FileSignature* signature = nullptr) const;

	private:
		git_repository& repo;
		Builder& builder;
		IProgress* progress = nullptr;

		void make_filesystems(const Registry::Manager& registry, unique_ptr<GitFileSystem>& current, unique_ptr<GitFileSystem>& target, const FileSignature* signature) const;

		void parse_revision(const string& revision, unique_ptr<git_commit>& from, unique_ptr<git_commit>& to) const;
		void parse_revision(const string& revision, unique_ptr<git_commit>& to) const;
	};
}