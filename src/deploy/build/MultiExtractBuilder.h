#pragma once
#include "DirectoryBuilder.h"

namespace JadeGit::Deploy
{
	class MultiExtractBuilder : public DirectoryBuilder
	{
	public:
		MultiExtractBuilder(const FileSystem& fs, const std::string& name, bool supportCommandFiles = false);

	protected:
		void start() final;
		void start(const Registry::Root& registry) final {};
		void finish(const Registry::Root& registry) final {};
		void finish() final;
		
		void addCommandFile(const std::filesystem::path& jcf, const char* loadStyle) final;
		void addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, const char* loadStyle) final;
		void addReorg() final {};
		void addScript(const Build::Script& script) final {};
		

	private:
		std::unique_ptr<std::ostream> mul;
		const std::string name;
		const bool supportCommandFiles;
	};
}