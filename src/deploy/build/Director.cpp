#include "Director.h"
#include "Builder.h"
#include <build/Builder.h>
#include <jadegit/build/Director.h>
#include <jadegit/vfs/FileSignature.h>
#include <registry/Manager.h>
#include <registry/Commit.h>
#include <vfs/GitDiffSource.h>

using namespace std;

namespace JadeGit::Deploy
{
	class DeploymentBuilder : public Build::Builder
	{
	public:
		DeploymentBuilder(Deploy::Builder& builder, Registry::Manager& registry, const git_repository& repo) : builder(builder), registry(registry), repo(repo) {}

		void RegisterSchema(const string& schema) final
		{
			registry.schema_add(repo, schema);
		}

		void DeregisterSchema(const string& schema) final
		{
			registry.schema_remove(repo, schema);
		}
	
		unique_ptr<ostream> AddCommandFile(bool latestVersion) final
		{
			return builder.AddCommandFile(latestVersion);
		}

		unique_ptr<ostream> AddSchemaFile(const string& schema, bool latestVersion) final
		{
			return builder.AddSchemaFile(schema, latestVersion);
		}

		unique_ptr<ostream> AddSchemaDataFile(const string& schema, bool latestVersion) final
		{
			return builder.AddSchemaDataFile(schema, latestVersion);
		}

		void addScript(const Build::Script& script) final
		{
			builder.addScript(script);
		}

		void Reorg() final
		{
			builder.reorg();
		};

		void Flush(bool reorgIfNeeded) final
		{
			builder.flush(reorgIfNeeded);
		}

	private:
		Deploy::Builder& builder;
		Registry::Manager& registry;
		const git_repository& repo;
	};

	Director::Director(git_repository& repo, Builder& builder, IProgress* progress) : repo(repo), builder(builder), progress(progress)
	{
	}

	bool Director::build(const std::string& revision) const
	{
		unique_ptr<git_commit> from;
		unique_ptr<git_commit> to;
		parse_revision(revision, from, to);

		return build(Registry::Manager(repo, from.get()), move(to));
	}

	bool Director::build(const git_commit* from, const git_commit* to, const FileSignature* signature) const
	{
		unique_ptr<git_commit> dup;
		git_throw(git_commit_dup(git_ptr(dup), const_cast<git_commit*>(to)));

		return build(Registry::Manager(repo, from), move(dup), signature);
	}

	bool Director::build(Registry::Manager registry, const std::string& revision) const
	{
		unique_ptr<git_commit> to;
		parse_revision(revision, to);

		return build(move(registry), move(to));
	}

	bool Director::build(Registry::Manager registry, std::unique_ptr<git_commit> revision, const FileSignature* signature) const
	{
		// update registry with latest commit for repository
		registry.update(repo, revision.get());

		// start build
		this->builder.start();
		this->builder.start(registry);
		
		// setup current/target filesystems
		unique_ptr<GitFileSystem> current;
		unique_ptr<GitFileSystem> target;
		make_filesystems(registry, current, target, signature);

		// setup build source
		GitDiffSource source(&repo, current.get(), target.get());

		// build updates
		DeploymentBuilder builder(this->builder, registry, repo);
		Build::Director director(source, this->progress);
		if (!director.Build(builder))
			return false;

		// finish build
		this->builder.finish(registry);
		this->builder.finish();
		return true;
	}

	void Director::make_filesystems(const Registry::Manager& registry, unique_ptr<GitFileSystem>& current, unique_ptr<GitFileSystem>& target, const FileSignature* signature) const
	{
		auto make_filesystem = [&](const vector<JadeGit::Registry::CommitT>& commits, const FileSignature* signature)
		{
			if (commits.empty())
				return unique_ptr<GitFileSystem>(nullptr);

			auto fs = make_unique<GitFileSystem>(&repo, false, signature);
			for (auto& commit : commits)
				fs->add(Registry::lookup(repo, commit));

			return fs;
		};

		// Use empty file signature for previous to suppress resolving actual author/commit
		static FileSignature empty;

		auto& repo_registry = registry.get(repo);
		current = make_filesystem(repo_registry.previous, &empty);
		target = make_filesystem(repo_registry.latest, signature);
	}

	void Director::parse_revision(const std::string& revision, std::unique_ptr<git_commit>& from, std::unique_ptr<git_commit>& to) const
	{
		git_revspec revspec;
		git_throw(git_revparse(&revspec, &repo, revision.c_str()));

		switch (revspec.flags)
		{
		case git_revspec_t::GIT_REVSPEC_MERGE_BASE:
			throw jadegit_exception("Revision string may be a range, not a merge base");

		case git_revspec_t::GIT_REVSPEC_RANGE:
			git_throw(git_object_peel((git_object**)(static_cast<git_commit**>(git_ptr(from))), revspec.from, git_object_t::GIT_OBJECT_COMMIT));
			git_object_free(revspec.from);
			git_throw(git_object_peel((git_object**)(static_cast<git_commit**>(git_ptr(to))), revspec.to, git_object_t::GIT_OBJECT_COMMIT));
			git_object_free(revspec.to);
			break;

		case git_revspec_t::GIT_REVSPEC_SINGLE:
			git_throw(git_object_peel((git_object**)(static_cast<git_commit**>(git_ptr(to))), revspec.from, git_object_t::GIT_OBJECT_COMMIT));
			git_object_free(revspec.from);
			break;
		}
	}

	void Director::parse_revision(const std::string& revision, std::unique_ptr<git_commit>& to) const
	{
		git_revspec revspec;
		git_throw(git_revparse(&revspec, &repo, revision.c_str()));

		switch (revspec.flags)
		{
		case git_revspec_t::GIT_REVSPEC_MERGE_BASE:
		case git_revspec_t::GIT_REVSPEC_RANGE:
			throw jadegit_exception("Revision string cannot be a range");

		case git_revspec_t::GIT_REVSPEC_SINGLE:
			git_throw(git_object_peel((git_object**)(static_cast<git_commit**>(git_ptr(to))), revspec.from, git_object_t::GIT_OBJECT_COMMIT));
			git_object_free(revspec.from);
			break;
		}
	}
}