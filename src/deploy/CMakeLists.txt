if(USE_JADE)
	target_sources(jadegit PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Commands.cpp
		${CMAKE_CURRENT_LIST_DIR}/Manifest.cpp
	)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/build/CMakeLists.txt)