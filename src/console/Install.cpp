#include "Command.h"
#include "CommandRegistration.h"
#include "Session.h"
#include <schema/install/Install.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Console
{
	class Install : public Command
	{
	public:
		static constexpr const char* description = "Load JadeGitSchema (required for development)";

		Install(CLI::App& cmd, Session& session) : Command(cmd, session)
		{
		}

		void execute() final
		{
			// Sign-off console app before updating
			if (!AppContext::IsSystem())
			{
				session.app.reset();
				session.app = std::make_unique<AppContext>(&session.db->node, TEXT("RootSchema"), TEXT("RootSchemaApp"));
			}

			// Install/update schema
			Schema::install();

			// Sign-on console app
			session.app.reset();
			session.app = std::make_unique<AppContext>(&session.db->node, TEXT("JadeGitSchema"), TEXT("JadeGitConsole"));
		}
	};
	static CommandRegistration<Install, true> registration("install");
}