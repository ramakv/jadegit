#include "Command.h"
#include "CommandRegistration.h"
#include <registry/Root.h>
#include <Platform.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Registry;

namespace JadeGit::Console
{
	class RegistryCommand : public Command
	{
	public:
		static constexpr const char* description = "Registry commands";

		RegistryCommand(CLI::App& cmd, Session& session) : Command(cmd, session)
		{
			// Need at least one subcommand
			cmd.require_subcommand(1);

#if USE_JADE
			// Initialise
			auto init = cmd.add_subcommand("init", "Initialise registry for repository");
			init->add_option("--name", this->name, "Repository name")->required();
			init->add_option("--repo", this->repo, "Path to repository")->check(CLI::ExistingDirectory)->required();
			init->add_option("--file", this->path, "Path to registry file");
			init->callback([this]() { Root::init(this->name, this->repo, this->path); });

			// Extract 
			auto extract = cmd.add_subcommand("extract", "Save registry data to file");
			extract->add_option("path", this->path, "Path to output file")->check(CLI::NonexistentPath)->required();
			extract->callback([this]() { Root::extract(this->path); });

			// Load
			auto load = cmd.add_subcommand("load", "Load updated registry data from file");
			load->add_option("path", this->path, "Path to input file")->check(CLI::ExistingFile)->required();
			load->add_flag("--force", this->force, "Force update");
			load->callback([this]() { Root::load(this->path, this->force); });
#endif

			// Show
			auto show = cmd.add_subcommand("show", "Display registry contents");
			show->add_option("path", this->path, "Path to registry file")->check(CLI::ExistingFile)->required(!use_jade);
			show->callback([this]() { Root::write(this->path, cout); });
		}

		void execute() final
		{
			// Not applicable
		}

	protected:
		string name;
		string repo;
		string path;
		bool force = false;
	};
	static CommandRegistration<RegistryCommand> registration("registry");
}