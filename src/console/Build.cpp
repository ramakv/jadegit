#include "RepositoryCommand.h"
#include "CommandRegistration.h"
#include "Progress.h"
#include "Session.h"
#include <deploy/build/Director.h>
#include <deploy/build/JARIDeploymentBuilder.h>
#include <deploy/build/MultiExtractBuilder.h>
#include <deploy/build/PowerShellDeploymentBuilder.h>
#include <deploy/build/XMLDeploymentBuilder.h>
#include <jadegit/vfs/NativeFileSystem.h>
#include <registry/Manager.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Deploy;

namespace JadeGit::Console
{
	class Build : public RepositoryCommand
	{
	public:
		static constexpr const char* description = R"(Builds deployment based on existing registry from target database and/or range of commits.)";

		Build(CLI::App& cmd, Session& session) : RepositoryCommand(cmd, session)
		{
			init_registry = cmd.add_flag("--init-registry", "Initialize registry for fresh database");
			cmd.add_option("--registry", this->registry, "Path to existing registry data file")->check(CLI::ExistingFile)->excludes(init_registry);
			cmd.add_option("--revision", this->revision, "Target commit range (see http://git-scm.com/docs/git-rev-parse.html#_specifying_revisions for syntax).")->capture_default_str();

			// Need one subcommand to setup deployment format(s)
			cmd.require_subcommand(1);

			// Setup subcommand to build JARI deployment
			{
				auto sub = cmd.add_subcommand("jari", "Build JARI deployment (*.jri)");
				sub->add_option("destination", this->destination, "Build output directory")->required();
				sub->add_option("--environment", this->name, "Release environment name")->required();
				sub->add_option("--description", this->shortDescription, "Short release description")->required();
				sub->add_option("--developer", this->username, "Developer username (required for authorisation)")->required();
				sub->add_option("--working-directory", this->workingDirectory, "Working directory from which JARI will be submitted (defaults to output directory)");
				sub->add_flag("--defeat-backups", this->defeatBackups, "Defeat all backups");
				sub->add_flag("--defeat-pre-backup", this->defeatPreBackup, "Defeat pre-installation backup");
				sub->add_flag("--defeat-post-backup", this->defeatPostBackup, "Defeat post-installation backup");
				sub->callback([this]() { jari(); });
			}
			
			// Setup subcommand to build standard extract
			{
				auto sub = cmd.add_subcommand("mul", "Build standard multiple schema file extract (*.mul)");
				sub->add_option("destination", this->destination, "Build output directory")->required();
				sub->add_option("-n,--name", this->name, "Optional name")->capture_default_str();
				sub->callback([this]() { mul(); });
			}

			// Setup subcommand to build PowerShell deployment
			{
				auto sub = cmd.add_subcommand("pwsh", "Build scripted deployment using PowerShell");
				sub->add_option("destination", this->destination, "Build output directory")->required();
				sub->callback([this]() { pwsh(); });
			}

			// Setup subcommand to build generic xml deployment
			{
				auto sub = cmd.add_subcommand("xml", "Build generic deployment with instructions in XML format");
				sub->add_option("destination", this->destination, "Build output directory")->required();
				sub->callback([this]() { xml(); });
			}
		}

		void execute() final
		{
			// open repository if required
			RepositoryCommand::execute();

			// setup deployment director
			Progress progress(*this, session);
			Deploy::Director director(*session.repo, *builder, &progress);

			// build deployment
			if (!registry.empty() || *init_registry)
			{
				director.build(Registry::Manager::make(registry, static_cast<bool>(*init_registry)), revision);
			}
			else
			{
				director.build(revision);
			}
			
			builder.reset();
			fs.reset();
		}

	protected:
		CLI::Option* init_registry = nullptr;

		string registry;
		string revision = "HEAD";

		string destination;
		string name = "All";
		string shortDescription;
		string username;
		bool defeatBackups = false;
		bool defeatPreBackup = false;
		bool defeatPostBackup = false;
		string workingDirectory;

		unique_ptr<FileSystem> fs;
		unique_ptr<Builder> builder;


		void jari()
		{
			this->fs = make_unique<NativeFileSystem>(destination, true);
			auto builder = make_unique<JARIDeploymentBuilder>(*fs, name, shortDescription);

			if (defeatBackups)
			{
				builder->setDefeatBackups();
			}
			else
			{
				builder->setDefeatPreBackup(defeatPreBackup);
				builder->setDefeatPostBackup(defeatPostBackup);
			}
			builder->setDeveloperId(username);
			builder->setWorkingDirectory(workingDirectory.empty() ? destination : workingDirectory);

			this->builder = move(builder);
		}

		// Setup multiple schema file extract
		void mul()
		{
			this->fs = make_unique<NativeFileSystem>(destination, true);
			this->builder = make_unique<MultiExtractBuilder>(*fs, name);
		}

		void pwsh()
		{
			this->fs = make_unique<NativeFileSystem>(destination, true);
			this->builder = make_unique<PowerShellDeploymentBuilder>(*fs);
		}

		void xml()
		{
			this->fs = make_unique<NativeFileSystem>(destination, true);
			this->builder = make_unique<XMLDeploymentBuilder>(*fs);
		}
	};
	static CommandRegistration<Build> registration("build");
}