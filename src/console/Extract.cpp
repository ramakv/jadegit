#include "RepositoryCommand.h"
#include "CommandRegistration.h"
#include "Session.h"
#include <extract/Assembly.h>
#include <extract/SchemaIterator.h>
#include <jade/Iterator.h>
#include <jade/Transient.h>
#include <vfs/GitFileSystem.h>
#include <regex>

using namespace std;

namespace JadeGit::Console
{
	class Extract : public RepositoryCommand
	{
	public:
		static constexpr const char* description = "Extract schemas";

		Extract(CLI::App& cmd, Session& context) : RepositoryCommand(cmd, context, true)
		{
			cmd.add_option("schemas", this->schemas, "Schemas to extract")->required();
		}

		void execute() final
		{
			// open repository if required
			RepositoryCommand::execute();

			// Treat schemas specified as regular expressions
			vector<regex> patterns;
			for (auto& pattern : this->schemas)
				patterns.push_back(regex(pattern));

			// Define filter (returns true for schemas to be extracted)
			auto filter = [&](const std::string& name)
			{
				// Supplied schemas which are generally excluded by source control must be explicitly specified for extract
				if (JadeGit::Extract::Schema::isSupplied(name))
				{
					auto& v = this->schemas;
					if (std::find(v.begin(), v.end(), name) != v.end())
						return true;
				}
				else
				{
					for (const auto& pattern : patterns)
					{
						if (regex_match(name, pattern))
							return true;
					}
				}

				return false;
			};

			// Setup file system
			GitFileSystem files(session.repo.get(), session.index.get());

			// Setup assembly to stage changes
			JadeGit::Extract::Assembly assembly(files);

			// Iterate schemas in dependency order
			JadeGit::Extract::Schema schema;
			JadeGit::Extract::SchemaIterator iter;
			bool acyclic = false;
			bool extracted = false;
			while (iter.next(schema, acyclic))
			{
				auto name = schema.getName();

				// Perform deep extract for schema if included
				if (filter(name))
				{
					print("Extracting " + name);
					schema.Extract(assembly, true);

					extracted = true;
				}

				// Save & unload schemas to recover memory, provided schema just extracted isn't part of a cyclic dependency
				if (extracted && acyclic)
				{
					assembly.save(true);
					extracted = false;
				}
			}

			assert(!extracted);
			print("Done");
		}

	protected:
		std::vector<std::string> schemas;
	};
	static CommandRegistration<Extract> registration("extract");
}