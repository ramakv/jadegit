#include "Exception.h"

namespace JadeGit
{
	xml_exception::xml_exception(tinyxml2::XMLError error) : jadegit_exception(tinyxml2::XMLDocument::ErrorIDToName(error)) {}

	/*
		TODO: Include more detailed message from XMLDocument ?

		std::string message(tinyxml2::XMLDocument& doc, tinyxml2::XMLError error)
		{
			std::string result = doc.ErrorIDToName(error);
			//	result += "\n\n";
			//	result += doc.GetErrorStr1();
			return result;
		}

	*/

	xml_exception::xml_exception(tinyxml2::XMLDocument& doc, tinyxml2::XMLError error) : jadegit_exception(doc.ErrorIDToName(error)) {}
}