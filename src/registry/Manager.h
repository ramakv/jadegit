#pragma once
#include "Root.h"
#include <git2/types.h>
#include <map>

namespace JadeGit::Registry
{
	class Manager
	{
	public:
		static Manager make(const std::filesystem::path& path, bool init);

		Manager(Root data);
		Manager(const std::filesystem::path& path);
		Manager(const git_repository& repo, const git_commit* commit);

		const RepositoryT& get(const git_repository& repo) const;
		const RepositoryT* get(const void* associate) const;

		const RepositoryT& init(const git_repository& repo);
		void update(const git_repository& repo, const git_commit* commit, bool replace_all = true);

		void schema_add(const git_repository& repo, const std::string& name);
		void schema_remove(const git_repository& repo, const std::string& name);

		operator const Root& () const { return latest; }

	private:
		Root current;
		Root latest;
		std::map<const void*, const RepositoryT*> associations;
	};
}