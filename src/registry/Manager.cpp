#include "Manager.h"
#include "Commit.h"
#include "Repository.h"
#include "Schema.h"
#include "storage/File.h"
#include <jadegit/git2.h>

using namespace std;

namespace JadeGit::Registry
{
	Manager Manager::make(const std::filesystem::path& path, bool init)
	{
		if (init && !path.empty())
			throw logic_error("Path not required when initializing registry");

		if (init)
			return Manager(Root());
		else
			return Manager(path);
	}

	Manager::Manager(Root data) : current(move(data))
	{
	}

	Manager::Manager(const std::filesystem::path& path) : current(FileStorage(path))
	{
	}
	
	Manager::Manager(const git_repository& src, const git_commit* commit)
	{
		RepositoryT repo;
		repo.origin = repo_origin(src);
		repo.name = repo_name(repo.origin);

		if (commit)
		{
			repo.latest.push_back(make_commit(*commit));

			// TODO: Populate known schemas based (assuming all are installed for supplied commit)
		}

		current.add(repo);
	}

	const RepositoryT& Manager::get(const git_repository& repo) const
	{
		auto data = get(&repo);
		if (!data)
			throw logic_error("Repository hasn't been initialized");

		return *data;
	}

	const RepositoryT* Manager::get(const void* associate) const
	{
		auto iter = associations.find(associate);
		if (iter != associations.end())
			return iter->second;

		return nullptr;
	}

	const RepositoryT& Manager::init(const git_repository& repo)
	{
		// suppress if already initialized
		if (auto data = associations[&repo])
			return *data;

		RepositoryT* latest = nullptr;

		// look for match in current data
		for (auto& current : this->current.repos)
		{
			if (repo_match(current, repo))
			{
				// safeguard against duplicate matches
				if (associations[&current])
					throw logic_error("Duplicate repository match");

				// copy to latest
				latest = this->latest.add(current);
				
				// update previous commits to reflect current version
				latest->previous = latest->latest;

				// setup association between current & latest
				associations[&current] = latest;
				associations[latest] = &current;

				break;
			}
		}

		// setup new repo if not matched
		if (!latest)
		{
			RepositoryT new_;
			new_.origin = repo_origin(repo);
			new_.name = repo_name(new_.origin);
			
			latest = this->latest.add(new_);
		}
		
		// setup association with repository
		associations[&repo] = latest;
		return *latest;
	}

	void Manager::update(const git_repository& repo, const git_commit* commit, bool replace_all)
	{
		auto& data = const_cast<RepositoryT&>(init(repo));
		
		if (replace_all)
			data.latest.clear();
		else
			throw jadegit_unimplemented_feature("Deploying/merging multiple branches");

		if (commit)
			data.latest.push_back(make_commit(*commit));
	}

	void Manager::schema_add(const git_repository& repo, const string& name)
	{
		auto& data = const_cast<RepositoryT&>(get(repo));
		data.schemas.push_back(make_schema(name));
	}

	void Manager::schema_remove(const git_repository& repo, const string& name)
	{
		auto& data = const_cast<RepositoryT&>(get(repo));
		erase_if(data.schemas, [&](const SchemaT& v) { return v.name == name; });
	}
}