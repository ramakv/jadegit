#include "Validator.h"
#include <format>

namespace JadeGit::Registry
{
	bool Validator::visitEnter(const RepositoryT& repo)
	{
		// Check name, ignoring unnamed repos when origin known
		if (!repo.name.empty() || repo.origin.empty())
			if (!repos.insert(repo.name).second)
				throw jadegit_exception(format("Repository name duplicated ({})", repo.name.empty() ? "<unknown>" : repo.name));

		// Check origin, ignoring unknown origin when repo is named
		if (!repo.origin.empty() || repo.name.empty())
			if (!origins.insert(repo.origin).second)
				throw jadegit_exception(format("Repository origin duplicated ({})", repo.origin));

		return true;
	}

	bool Validator::visit(const SchemaT& schema, bool first)
	{
		// Check schema hasn't been registered multiple times
		if (!schemas.insert(schema.name).second)
			throw jadegit_exception(format("Schema cannot be registered more than once ({})", schema.name));

		return true;
	}
}