#include "File.h"
#include <format>
#include <fstream>
#include <jadegit/vfs/NativeFileSystem.h>

using namespace std;
using namespace std::filesystem;

namespace JadeGit::Registry
{
	FileStorage::FileStorage(filesystem::path path, bool mustexist) : FileStorage(make_unique<NativeFileSystem>(path.parent_path()), path.filename(), mustexist)
	{
	}

	FileStorage::FileStorage(std::unique_ptr<FileSystem> fs, std::filesystem::path path, bool mustexist) : fs(move(fs)), mustexist(mustexist)
	{
		if (!path.has_filename())
			throw invalid_argument("Filename required");

		file = this->fs->open(path);
	}

	FileStorage::FileStorage(File file, bool mustexist) : file(move(file)), mustexist(mustexist)
	{
		if (!this->file || this->file.isDirectory())
			throw invalid_argument("File required");
	}

	void FileStorage::load(Root& registry) const
	{
		// Handle nonexistent file
		if (!mustexist && !file.exists())
			return;

		// Open input stream
		auto stream = file.createInputStream(ios::binary | ios::ate);

		// Determine file length
		streamsize length = stream->tellg();
		stream->seekg(0, ios::beg);

		// Load buffer from file
		vector<char> buffer(length);
		if (stream->read(buffer.data(), length).fail())
			throw jadegit_exception("Failed to read file stream");

		// Verify & load buffer
		Storage::load(registry, reinterpret_cast<uint8_t*>(buffer.data()), length);
	}

	void FileStorage::save(const uint8_t* buffer, size_t length) const
	{
		// Write to file
		auto stream = file.createOutputStream(ios::binary | ios::out);
		stream->write(reinterpret_cast<const char*>(buffer), length);
		stream->flush();
	}
}