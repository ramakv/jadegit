#include "Database.h"
#include <jade/AppContext.h>
#include <jade/Binary.h>
#include <jade/JadeDynamicObject.h>
#include <jade/Iterator.h>
#include <jade/Transaction.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Registry
{
	class DataObject : public JadeDynamicObject
	{
	public:
		static DataObject& get()
		{
			// Return cached reference to data object
			static DataObject data;
			assert(data.isValid());
			return data;
		}

		void getRegistryData(DskParamBinary& param) const
		{
			getProperty(TEXT("registry"), param);
		}

		void setRegistryData(DskParamBinary& param) const
		{
			setProperty(TEXT("registry"), param);
		}

	private:
		DataObject() : JadeDynamicObject()
		{
			// Define routine to find/setup data object
			auto setup = [&]()
			{
				auto name = TEXT("jadegit");

				// Find existing
				DskClass type;
				jade_throw(DskSchema(&RootSchemaOid).getClassByNumber(DSKJADEDYNAMICOBJECT, type));

				JadeDynamicObject instance;
				Iterator<JadeDynamicObject> iter(type, TEXT("instances"));
				while (iter.next(instance))
				{
					if (instance.name() == name)
					{
						oid = instance.oid;
						return;
					}
				}

				// Instantiate
				Transaction transaction;
				createObject();
				setName(name);
				addProperty(TEXT("registry"), TEXT("Binary"));
				transaction.commit();
			};

			// Handle setup via separate thread if executing within Jade process (which can't iterate dynamic objects), or current thread is in transaction state
			if (AppContext::currentSchema() == JadeSchemaOid || Transaction::InTransactionState())
			{
				DskHandle node = { 0 };
				jade_throw(jomGetNodeContextHandle(nullptr, &node));

				packaged_task task([&] { AppContext app(&node, TEXT("RootSchema"), TEXT("RootSchemaApp")); setup(); });

				future f = task.get_future();
				thread(move(task)).join();
				f.get();
			}
			// Handle setup via current thread
			else
			{
				setup();
			}
		}
	};

	void DatabaseStorage::load(Root& registry) const
	{
		// Load buffer from database
		DskParamBinary param;
		DataObject::get().getRegistryData(param);
		BYTE* buffer = nullptr;
		Size length = 0;
		jade_throw(paramGetBinary(param, buffer, length));

		// Verify & load non-empty buffer
		if (length)
			Storage::load(registry, buffer, length);
	}

	void DatabaseStorage::save(const uint8_t* buffer, size_t length) const
	{
		// Save to database
		DskParamBinary param(buffer, length);

		Transaction transaction;
		DataObject::get().setRegistryData(param);
		transaction.commit();
	}
}