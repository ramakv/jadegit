#pragma once

namespace JadeGit::Registry
{
	class Root;

	class Storage
	{
	public:
		virtual void load(Root& registry) const = 0;
		void save(const Root& registry) const;

	protected:
		void load(Root& registry, const uint8_t* buffer, size_t length) const;
		virtual void save(const uint8_t* buffer, size_t length) const = 0;
	};
}