#include "Root.h"
#include "Printer.h"
#include "Repository.h"
#include "Schema.h"
#include "Storage.h"
#include "Validator.h"
#include <flatbuffers/vector.h>
#include <Platform.h>

using namespace std;
using namespace flatbuffers;

#if USE_JADE
#include <jade/Iterator.h>
#include <jade/Object.h>

using namespace Jade;
#endif

namespace JadeGit::Registry
{
#if USE_JADE && USE_GIT2
	void Root::init(const string& name, const path& repo, const path& path)
	{
		auto init = [&](const Storage& storage)
		{
			// Load existing registry data
			Root registry(storage);

			// Update with new repository data
			registry.update(Root(repo_init(name, repo)), true);

			// Save
			registry.save(storage);
		};

		if (!path.empty())
			init(FileStorage(path, false));
		else
			init(DatabaseStorage());
	}
#endif

#if USE_JADE
	class UnknownRepositoryBuilder : private Visitor
	{
	public:
		static void build(Root& registry)
		{
			UnknownRepositoryBuilder builder;
			registry.accept(builder);
			registry.add(builder.repo);
		}

	protected:
		RepositoryT repo;
		set<string> schemas;

		UnknownRepositoryBuilder()
		{
			// Get installed schemas
			Object object;
			Iterator<Object> iter(DskSchema(&RootSchemaOid), PRP_Schema__schemasByNumber);
			while (iter.next(object))
				schemas.insert(object.getProperty<string>(TEXT("name")));
		}

		bool visit(const SchemaT& schema, bool first)
		{
			// Remove known schemas from installed list
			schemas.erase(schema.name);
			return true;
		}

		bool visitLeave(const DataT& registry)
		{
			// Populate repository with unknown schemas
			for (auto& name : schemas)
				repo.schemas.push_back(make_schema(name));

			return true;
		}
	};

	Root Root::extract()
	{
		// Load from database
		DatabaseStorage storage;
		Root registry(storage);

		// Add unknown repo/schemas
		UnknownRepositoryBuilder::build(registry);

		return registry;
	}

	void Root::extract(const std::filesystem::path& path)
	{
		// Extract & save to output path
		extract().save(FileStorage(path));
	}

	void Root::load(const Root& update, bool force)
	{
		// Load from database
		DatabaseStorage storage;
		Root registry(storage);

		// Apply update
		registry.update(update, force);

		// Save to database
		registry.save(storage);
	}

	void Root::load(const path& path, bool force)
	{
		// Load update from file
		load(Root(FileStorage(path)), force);
	}

	void Root::validate(const Root& update, bool force)
	{
		// Load from database
		DatabaseStorage storage;
		Root registry(storage);

		// Apply update
		registry.update(update, force);

		// Validate
		Validator validator;
		registry.accept(validator);
	}

	void Root::validate(const path& path, bool force)
	{
		// Validate update from file
		validate(Root(FileStorage(path)), force);
	}

	void Root::write(const path& path, ostream& os)
	{
		if (path.empty())
			os << Root(DatabaseStorage());
		else
			os << Root(FileStorage(path));
	}

#else
	void Root::write(const path& path, ostream& os)
	{
		os << Root(FileStorage(path));
	}
#endif

	Root::Root() : DataT()
	{
	}

	Root::Root(const Storage& storage)
	{
		storage.load(*this);
	}

	Root::Root(const RepositoryT& repo)
	{
		add(repo);
	}

	RepositoryT* Root::find_repo_by_name(const string& name)
	{
		return find_repo([&](RepositoryT& repo) { return repo.name == name; });
	}

	RepositoryT* Root::find_repo_by_origin(const string& origin)
	{
		return find_repo([&](RepositoryT& repo) { return repo.origin == origin; });
	}

	bool Root::accept(Visitor& visitor) const
	{
		return visitor.visit(*this);
	}

	RepositoryT* Root::add(const RepositoryT& repo)
	{
		repos.push_back(repo);
		return &repos.back();
	}

	void Root::update(const Root& from, bool force)
	{
		// Define delta list to match previous/latest repos for update
		using delta = pair<RepositoryT*, const RepositoryT*>;
		vector<delta> deltas;

		// Add previous repos
		for (auto& repo : repos)
			deltas.emplace_back(&repo, nullptr);

		// Add latest repos
		for (auto& repo : from.repos)
		{
			// Skip unknown repos
			if (repo_unknown(repo))
				continue;

			bool matched = false;
			for (auto& delta : deltas)
			{
				// Skip repositories already matched
				if (delta.second)
					continue;

				// Update delta if repositories match
				if (repo_match(*delta.first, repo))
				{
					delta.second = &repo;
					matched = true;
					break;
				}
			}

			// Add delta for new repo if not matched
			if (!matched)
				deltas.emplace_back(nullptr, &repo);
		}

		// Perform updates
		for (auto& delta : deltas)
		{
			// Skip existing repos which don't need updating
			if (!delta.second)
				continue;

			// Update existing
			if (delta.first)
			{
				repo_update(*delta.first, *delta.second, force);
			}
			else
			{
				// Add new, but treated like an update starting with repo in default state
				RepositoryT repo;
				repo_update(repo, *delta.second, force);
				repos.push_back(repo);
			}
		}

		// TODO: Cleanup empty/redundant repos?
	}

	void Root::save(const Storage& storage) const
	{
		storage.save(*this);
	}

	bool operator==(const PlatformT& lhs, const PlatformT& rhs)
	{
		return lhs.version == rhs.version &&
			lhs.charset == rhs.charset;
	}

	bool Root::operator==(const Root& rhs) const
	{
		return (experimental == rhs.experimental) &&
			(platform == rhs.platform && (!platform || *platform == *rhs.platform)) &&
			(repos == rhs.repos);
	}

	ostream& operator<<(ostream& os, const Root& registry)
	{
		Printer printer(os);
		registry.accept(printer);
		return os;
	}
}