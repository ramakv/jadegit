#include "GitFileSystem.h"
#include "GitFile.h"
#include "GitFileSystemSource.h"
#include <jadegit/vfs/File.h>
#include <assert.h>

using namespace std;

namespace JadeGit
{
	GitFileSystem::GitFileSystem(git_repository* repo, bool baseline, const FileSignature* signature) : repo(repo), baseline(baseline), signature(signature), index(nullptr) {}
	GitFileSystem::GitFileSystem(git_repository* repo, git_index* index) : repo(repo), baseline(false), index(index) {}

	GitFileSystem::GitFileSystem(unique_ptr<git_commit> commit) : repo(git_commit_owner(commit.get())), index(nullptr)
	{
		add(move(commit));
	}

	GitFileSystem::~GitFileSystem() {}

	void GitFileSystem::add(unique_ptr<GitFileSystemSource> source)
	{
		assert(!index);

		source->merge(our_tree);
		first ? first->add(move(source)) : first.swap(source);
	}

	void GitFileSystem::add(unique_ptr<git_commit> commit)
	{
		add(make_unique<GitFileCommitSource>(move(commit)));
	}

	void GitFileSystem::add(unique_ptr<git_reference> reference)
	{
		add(make_unique<GitFileCommitSource>(move(reference)));
	}

	void GitFileSystem::add(string author, git_oid tree, unique_ptr<git_commit> ours, unique_ptr<git_commit> theirs)
	{
		add(make_unique<GitFileTreeSource>(move(author), tree, move(ours), move(theirs)));
	}

	GitFileSystem::operator git_tree* () const
	{
		assert(!index);

		// Add main-worktree/HEAD if required (which is our baseline)
		if (!our_tree && baseline)
		{
			unique_ptr<git_reference> head;
			git_throw(git_reference_lookup(git_ptr(head), repo, "main-worktree/HEAD"));
			const_cast<GitFileSystem*>(this)->add(make_unique<GitFileCommitSource>(move(head)));
		}

		return our_tree.get();
	}

	bool GitFileSystem::isReadOnly() const
	{
		// File system is read-only if no index has been provided via which updates can be staged
		return !index;
	}

	File GitFileSystem::open(const std::filesystem::path& path) const
	{
		return File(std::make_unique<GitFile>(*this, path));
	}

	void GitFileSystem::flush() const
	{
		assert(index);
		
		// Save changes to disk
		if (git_index_path(index))
			git_throw(git_index_write(index));
	}
}