#pragma once
#include <jadegit/vfs/AnyFile.h>

namespace JadeGit
{
	class NativeFileSystem;

	class NativeFile : public AnyFile
	{
	public:
		NativeFile(const NativeFileSystem& fs, const std::filesystem::path& path);
		~NativeFile();

		std::unique_ptr<AnyFile> clone() const final;
		const FileSystem* vfs() const final;
		std::filesystem::path path() const final;
		bool exists() const final;
		bool isDirectory() const final;
		const FileSignature* getSignature(size_t lineFrom, size_t lineTo) const final;
		std::unique_ptr<AnyFileIterator> begin() const final;
		void createDirectory() final;
		void copy(AnyFile& dest) const final;
		void move(AnyFile& dest) final;
		void rename(const std::filesystem::path& filename) final;
		void remove() final;
		std::unique_ptr<std::istream> createInputStream(std::ios_base::openmode mode) const final;
		std::unique_ptr<std::ostream> createOutputStream(std::ios_base::openmode mode) final;

	protected:
		const NativeFileSystem& fs;
		std::filesystem::path relative_path;
	};
}