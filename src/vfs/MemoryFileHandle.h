#pragma once
#include <jadegit/vfs/AnyFile.h>

namespace JadeGit
{
	class MemoryFileNode;
	class MemoryFileSystem;

	class MemoryFileHandle : public AnyFile
	{
	public:
		MemoryFileHandle(const MemoryFileSystem& fs, const std::filesystem::path& path);
		~MemoryFileHandle();

		std::unique_ptr<AnyFile> clone() const final;
		const FileSystem* vfs() const final;
		std::filesystem::path path() const final;
		bool exists() const final;
		bool isDirectory() const final;
		const FileSignature* getSignature(size_t lineFrom, size_t lineTo) const final { return nullptr; }
		std::unique_ptr<AnyFileIterator> begin() const final;

		void createDirectory() final;
		void remove() final;

		std::unique_ptr<std::istream> createInputStream(std::ios_base::openmode mode) const final;
		std::unique_ptr<std::ostream> createOutputStream(std::ios_base::openmode mode) final;

	protected:
		const MemoryFileSystem& fs;
		std::filesystem::path relative_path;
		MemoryFileNode* file = nullptr;
	};
}