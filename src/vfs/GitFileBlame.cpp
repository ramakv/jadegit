#include "GitFileBlame.h"
#include "GitFileSystem.h"
#include <jadegit/vfs/FileSignature.h>
#include <chrono>

using namespace std;
using namespace std::chrono;

namespace JadeGit
{
	GitFileBlame::GitFileBlame(unique_ptr<git_blame> blame) : blame(move(blame)) {}

	GitFileBlame::GitFileBlame(const FileSignature& signature) : signature(&signature) {}

	const FileSignature* GitFileBlame::getSignature(const GitFileSystem& fs, size_t lineFrom, size_t lineTo) const
	{
		if (signature || !blame)
			return signature;

		const git_blame_hunk* result = nullptr;
		const git_blame_hunk* hunk = nullptr;

		for (auto line = lineFrom; line <= lineTo; line += hunk->lines_in_hunk)
		{
			if (hunk = git_blame_get_hunk_byline(blame.get(), line))
			{
				if (!result || hunk->final_signature->when.time > result->final_signature->when.time)
					result = hunk;
			}
			else
				break;
		}

		if (!result)
			return nullptr;

		// Use cached signature for same commit
		if (auto search = fs.signatures.find(result->final_commit_id); search != fs.signatures.end())
			return &search->second;

		// Create/cache new signature
		char buffer[9] = "";
		auto author = std::string(result->final_signature->name) + " (" + git_oid_tostr(buffer, 9, &result->final_commit_id) + ")";
		auto modified = system_clock::time_point{ seconds{ result->final_signature->when.time } };
		return &fs.signatures.emplace(make_pair(result->final_commit_id, FileSignature(author, modified))).first->second;
	}
}