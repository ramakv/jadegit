#include <jadegit/vfs/AnyFile.h>
#include <jadegit/vfs/File.h>
#include <jadegit/vfs/FileSystem.h>
#include <jadegit/Exception.h>

namespace JadeGit
{
	AnyFile::AnyFile() {}
	AnyFile::~AnyFile() {}

	File AnyFile::open(const std::filesystem::path& path) const
	{
		return vfs()->open(this->path() / path);
	}

	void AnyFile::copy(AnyFile& dest) const
	{
		// Generic copy
		auto in = createInputStream();
		auto out = dest.createOutputStream();
		*out << in->rdbuf() << std::flush;
	}

	void AnyFile::move(AnyFile& dest)
	{
		// Copy to destination
		copy(dest);

		// Remove source
		remove();
	}

	void AnyFile::rename(const std::filesystem::path& filename)
	{
		std::filesystem::path path = this->path();
		File dest = vfs()->open(path.replace_filename(filename));
		move(*dest.file);
	}
}