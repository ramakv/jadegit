#include "TempFileSystem.h"
#include "Environment.h"
#include <jadegit/vfs/FileIterator.h>

namespace fs = std::filesystem;

namespace JadeGit
{
	TempFileSystem::TempFileSystem(fs::path subfolder) : NativeFileSystem(makeTempDirectory() / subfolder, true)
	{
	}

	TempFileSystem::~TempFileSystem()
	{
		if (!std::uncaught_exceptions())
			fs::remove_all(base);
	}

	fs::path TempFileSystem::path() const
	{
		return base;
	}

	void TempFileSystem::purge()
	{
		for (auto file : open("").begin())
			file.remove();
	}
}