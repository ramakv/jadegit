#pragma once
#include <jadegit/git2.h>
#include <jadegit/vfs/FileSystem.h>
#include <map>

namespace JadeGit
{
	class GitFileSystemSource;

	class GitFileSystem : public FileSystem
	{
	public:
		GitFileSystem(git_repository* repo, bool baseline, const FileSignature* signature = nullptr);
		GitFileSystem(git_repository* repo, git_index* index);
		GitFileSystem(std::unique_ptr<git_commit> commit);
		virtual ~GitFileSystem();

		void add(std::unique_ptr<git_commit> commit);
		void add(std::unique_ptr<git_reference> reference);
		void add(std::string author, git_oid tree, std::unique_ptr<git_commit> ours, std::unique_ptr<git_commit> theirs = std::unique_ptr<git_commit>());

		bool isReadOnly() const override;
		File open(const std::filesystem::path& path) const override;
		void flush() const override;

		operator git_tree* () const;

	private:
		friend class GitFile;
		friend class GitFileBlame;
		friend class GitFileBuffer;

		void add(std::unique_ptr<GitFileSystemSource> source);

		git_repository* const repo;
		git_index* const index;
		const bool baseline = false;
		std::shared_ptr<git_tree> our_tree;

		std::unique_ptr<GitFileSystemSource> first;

		const FileSignature* signature = nullptr;
		mutable std::map<git_oid, FileSignature> signatures;
	};
}