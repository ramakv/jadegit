#pragma once
#include <jadegit/vfs/AnyFile.h>
#include <jadegit/git2.h>

namespace JadeGit
{
	class GitFileBlame;
	class GitFileSystem;

	class GitFile : public AnyFile
	{
	public:
		GitFile(const GitFileSystem& fs, const std::filesystem::path& path);
		GitFile(const GitFileSystem& fs, const std::filesystem::path& path, const git_oid& id);
		~GitFile();

		std::unique_ptr<AnyFile> clone() const final;
		const FileSystem* vfs() const final;
		std::filesystem::path path() const final;
		bool exists() const final;
		bool isDirectory() const final;
		const FileSignature* getSignature(size_t lineFrom, size_t lineTo) const final;
		std::unique_ptr<AnyFileIterator> begin() const final;
		void createDirectory() final;
		void remove() final;

		std::unique_ptr<std::istream> createInputStream(std::ios_base::openmode mode) const final;
		std::unique_ptr<std::ostream> createOutputStream(std::ios_base::openmode mode) final;

	protected:
		const GitFileSystem& fs;
		std::filesystem::path relative_path;
		git_oid id;

		mutable std::unique_ptr<GitFileBlame> blame;

	private:
		git_oid find(const GitFileSystem& fs, const std::filesystem::path& relative_path) const;
	};
}