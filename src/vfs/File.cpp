#include <jadegit/vfs/File.h>
#include <jadegit/vfs/AnyFile.h>
#include <jadegit/vfs/AnyFileIterator.h>
#include <jadegit/vfs/FileIterator.h>
#include <jadegit/vfs/FileSystem.h>
#include <sstream>

using namespace std;

namespace JadeGit
{
	File::File() {}

	File::File(unique_ptr<AnyFile>&& file) : file(std::move(file)) {}

	File::File(const File& handle) : file(handle.file ? handle.file->clone() : nullptr) {}

	File::File(File&& handle) : file(std::move(handle.file)) { }

	File::~File() {}

	File::operator bool() const
	{
		return static_cast<bool>(file);
	}

	File& File::operator=(const File& handle)
	{
		if (handle.file)
		{
			file = handle.file->clone();
		}
		else
		{
			file.reset(nullptr);
		}

		return *this;
	}

	File& File::operator=(File&& handle)
	{
		file = std::move(handle.file);
		return *this;
	}

	const FileSystem* File::vfs() const
	{
		return file->vfs();
	}

	filesystem::path File::path() const
	{
		return file ? file->path() : "";
	}

	filesystem::path File::filename() const
	{
		return file ? file->path().filename() : "";
	}

	bool File::exists() const
	{
		return file && file->exists();
	}

	bool File::isDirectory() const
	{
		return file->isDirectory();
	}

	const FileSignature* File::getSignature(size_t lineFrom, size_t lineTo) const
	{
		return file ? file->getSignature(lineFrom, lineTo) : nullptr;
	}

	FileIterator File::begin() const
	{
		return file ? FileIterator(file->begin()) : FileIterator();
	}

	FileIterator File::end() const
	{
		return FileIterator();
	}

	File File::open(const filesystem::path& relative_path) const
	{
		return file->open(relative_path);
	}

	void File::createDirectory()
	{
		file->createDirectory();
	}

	void File::copy(const File& dest) const
	{
		file->copy(*dest.file);
	}

	void File::move(File& dest)
	{
		file->move(*dest.file);
	}

	void File::rename(const filesystem::path& filename)
	{
		file->rename(filename);
	}

	void File::remove()
	{
		file->remove();
	}

	unique_ptr<istream> File::createInputStream(ios_base::openmode mode) const
	{
		return file->createInputStream(mode);
	}

	unique_ptr<ostream> File::createOutputStream(ios_base::openmode mode) const
	{
		return file->createOutputStream(mode);
	}

	string File::read() const
	{
		stringstream buffer;
		buffer << createInputStream()->rdbuf();
		return buffer.str();
	}

	void File::write(const string& content)
	{
		*createOutputStream() << content << flush;
	}

	void File::write(const string_view& content)
	{
		*createOutputStream() << content << flush;
	}

	ostream& operator<<(ostream& os, const File& file)
	{
		os << file.createInputStream()->rdbuf();
		return os;
	}
}