#include "NativeFile.h"
#include "NativeFileIterator.h"
#include <jadegit/vfs/NativeFileSystem.h>
#include <jadegit/Exception.h>
#include <fstream>

namespace fs = std::filesystem;

namespace JadeGit
{
	NativeFile::NativeFile(const NativeFileSystem& fs, const std::filesystem::path& path) : fs(fs), relative_path(path) {}

	NativeFile::~NativeFile() {}

	std::unique_ptr<AnyFile> NativeFile::clone() const
	{
		return std::make_unique<NativeFile>(fs, relative_path);
	}

	const FileSystem* NativeFile::vfs() const
	{
		return &fs;
	}

	fs::path NativeFile::path() const
	{	
		return relative_path;
	}

	bool NativeFile::exists() const
	{
		return fs::exists(fs.base / relative_path);
	}

	bool NativeFile::isDirectory() const
	{
		return fs::is_directory(fs.base / relative_path);
	}

	const FileSignature* NativeFile::getSignature(size_t lineFrom, size_t lineTo) const
	{
		return fs.signature;
	}

	std::unique_ptr<AnyFileIterator> NativeFile::begin() const
	{
		return std::make_unique<NativeFileIterator>(clone(), fs::directory_iterator(fs.base / relative_path));
	}

	void NativeFile::createDirectory()
	{
		fs::create_directory(fs.base / relative_path);
	}

	void NativeFile::copy(AnyFile& dest) const
	{
		// Use native method if same filesystem
		if (vfs() == dest.vfs())
			fs::copy(fs.base / relative_path, fs.base / dest.path());

		// Generic implementation
		else
			AnyFile::copy(dest);
	}

	void NativeFile::move(AnyFile& dest)
	{
		// Use native method if same filesystem
		if (vfs() == dest.vfs())
			fs::rename(fs.base / relative_path, fs.base / dest.path());

		// Generic implementation
		else
			AnyFile::move(dest);
	}

	void NativeFile::rename(const fs::path& filename)
	{
		// Copy current filename to compose new when renaming
		fs::path path = relative_path;

		// Rename file
		fs::rename(fs.base / relative_path, fs.base / relative_path.replace_filename(filename));
	}

	void NativeFile::remove()
	{
		fs::remove_all(fs.base / relative_path);
	}

	std::unique_ptr<std::istream> NativeFile::createInputStream(std::ios_base::openmode mode) const
	{
		auto stream = std::make_unique<std::ifstream>(fs.base / relative_path, mode);

		if (stream->fail())
			throw jadegit_exception("Failed to open input file [" + path().generic_string() + "]: " + strerror(errno));

		return stream;
	}

	std::unique_ptr<std::ostream> NativeFile::createOutputStream(std::ios_base::openmode mode)
	{
		// Derive entire path
		auto path = fs.base / relative_path;

		// Ensure parent directory exists
		if (path.has_parent_path())
			fs::create_directories(path.parent_path());

		// Open output stream
		auto stream = std::make_unique<std::ofstream>(path, mode);

		if (stream->fail())
			throw jadegit_exception("Failed to open output file stream [" + relative_path.generic_string() + "]: " + strerror(errno));

		return stream;
	}
}