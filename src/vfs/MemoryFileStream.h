#pragma once
#include "MemoryFile.h"
#include <sstream>

namespace JadeGit
{
	class MemoryFileBuffer : public std::stringbuf
	{
	public:
		MemoryFileBuffer(MemoryFile& file, std::ios_base::openmode mode) : std::stringbuf(file.content, mode), file(file) {}

	private:
		MemoryFile& file;

		// Copy buffer to file when stream is flushed which has to be caused explicitly
		// This approach is intended to reflect how we need to update the libgit2 index for error handling
		int sync() override
		{
			file.content = str();
			return 0;
		}
	};

	class MemoryFileStream : public std::iostream
	{
	public:
		MemoryFileStream(MemoryFile& file, std::ios_base::openmode mode) : std::iostream(&buffer), buffer(file, mode) {}

	private:
		MemoryFileBuffer buffer;
	};
}