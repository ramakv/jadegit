#include "TaskGraph.h"
#include "EmptyTask.h"

#include <jadegit/data/Application.h>
#include <jadegit/data/Attribute.h>
#include <jadegit/data/Database.h>
#include <jadegit/data/Function.h>
#include <jadegit/data/JadeExposedList.h>
#include <jadegit/data/JadeWebServicesClass.h>
#include <jadegit/data/JadeWebServicesMethod.h>
#include <jadegit/data/Library.h>
#include <jadegit/data/PrimType.h>

#include "ActiveXLibraryDelta.h"
#include "ClassDelta.h"
#include "CollClassDelta.h"
#include "ConstantCategoryDelta.h"
#include "FormDelta.h"
#include "GlobalConstantDelta.h"
#include "JadeExportedPackageDelta.h"
#include "JadeHTMLDocumentDelta.h"
#include "JadeImportedPackageDelta.h"
#include "JadeInterfaceDelta.h"
#include "LocaleDelta.h"
#include "LocaleFormatDelta.h"
#include "MethodDelta.h"
#include "PropertyDelta.h"
#include "ReferenceDelta.h"
#include "SchemaDelta.h"
#include "TranslatableStringDelta.h"

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	void TaskGraph::Add(Entity* entity, bool latest)
	{
		if (!entity)
			return;

		this->latest = latest;
		entity->AcceptAll(*this);
	}

	bool TaskGraph::analyze(IProgress* progress)
	{
		if (progress && !progress->start(deltas.size(), "Analyzing"))
			return false;

		std::stable_sort(deltas.begin(), deltas.end(), [this](const IDelta * a, const IDelta * b) { return a->Level() < b->Level(); });
		
		for (IDelta* delta : deltas)
		{
			delta->Analyze();

			if (progress && !progress->step())
				return false;
		}

		return !progress || progress->finish();
	}

	TaskGraph::~TaskGraph()
	{
		for (Task* command : commands)
			delete command;

		for (IDelta* delta : deltas)
			delete delta;
	}

	void TaskGraph::DuplicateCheck(const Entity* prior, const Entity* current) const
	{
		if (prior && prior != current)
			throw jadegit_exception((latest ? "Latest " : "Previous ")  + current->dataClass->name + " [" + current->GetQualifiedName() + "] duplicates guid already used by " + prior->dataClass->name + " [" + prior->GetQualifiedName() + "]");
	}

	void TaskGraph::Visit(ActiveXAttribute* attribute)
	{
		Deltify<ActiveXComponentDelta<ActiveXAttribute>>(attribute);
	}

	void TaskGraph::Visit(ActiveXClass* cls)
	{
		Deltify<ActiveXComponentDelta<ActiveXClass>>(cls);
	}

	void TaskGraph::Visit(ActiveXConstant* constant)
	{
		Deltify<ActiveXComponentDelta<ActiveXConstant>>(constant);
	}

	void TaskGraph::Visit(ActiveXLibrary* library)
	{
		Deltify<ActiveXLibraryDelta>(library);
	}

	void TaskGraph::Visit(ActiveXMethod* method)
	{
		Deltify<ActiveXComponentDelta<ActiveXMethod>>(method);
	}

	void TaskGraph::Visit(Application* application)
	{
		Deltify<SchemaComponentDelta<Application>>(application, "Application");
	}

	void TaskGraph::Visit(Class* cls)
	{
		Deltify<ClassDelta<Class>>(cls);
	}

	void TaskGraph::Visit(CollClass* cls)
	{
		Deltify<CollClassDelta<CollClass>>(cls);
	}

	void TaskGraph::Visit(CompAttribute* attribute)
	{
		Deltify<PropertyDelta<CompAttribute>>(attribute);
	}

	void TaskGraph::Visit(Constant* constant)
	{
		Deltify<ConstantDelta<>>(constant);
	}

	void TaskGraph::Visit(Data::Control* control)
	{
		// Possibly unrequired, so Control doesn't need to be an entity?
	}

	void TaskGraph::Visit(ConstantCategory* category)
	{
		Deltify<ConstantCategoryDelta>(category);
	}

	void TaskGraph::Visit(CurrencyFormat* format)
	{
		Deltify<LocaleFormatDelta<CurrencyFormat>>(format);
	}

	void TaskGraph::Visit(Database* database)
	{
		Deltify<SchemaComponentDelta<Database>>(database, "Database");
	}

	void TaskGraph::Visit(DateFormat* format)
	{
		Deltify<LocaleFormatDelta<DateFormat>>(format);
	}

	void TaskGraph::Visit(DbFile* dbFile)
	{
		Deltify<SchemaComponentDelta<DbFile>>(dbFile, "DbFile");
	}

	void TaskGraph::Visit(ExplicitInverseRef* reference)
	{
		Deltify<ReferenceDelta<ExplicitInverseRef>>(reference);
	}

	void TaskGraph::Visit(ExternalMethod* method)
	{
		Deltify<MethodDelta<ExternalMethod>>(method);
	}

	void TaskGraph::Visit(Form* form)
	{
		Deltify<FormDelta>(form);
	}

	void TaskGraph::Visit(Function* function)
	{
		Deltify<SchemaComponentDelta<Function>>(function, "ExternalFunction");
	}

	void TaskGraph::Visit(GlobalConstant* constant)
	{
		Deltify<GlobalConstantDelta>(constant);
	}

	void TaskGraph::Visit(ImplicitInverseRef* reference)
	{
		Deltify<ReferenceDelta<ImplicitInverseRef>>(reference);
	}

	void TaskGraph::Visit(JadeExportedClass* cls)
	{
		Deltify<JadeExportedEntityDelta<JadeExportedClass>>(cls);
	}

	void TaskGraph::Visit(JadeExportedConstant* constant)
	{
		Deltify<JadeExportedEntityDelta<JadeExportedConstant>>(constant);
	}

	void TaskGraph::Visit(JadeExportedInterface* interface)
	{
		Deltify<JadeExportedEntityDelta<JadeExportedInterface>>(interface);
	}

	void TaskGraph::Visit(JadeExportedMethod* method)
	{
		Deltify<JadeExportedEntityDelta<JadeExportedMethod>>(method);
	}

	void TaskGraph::Visit(JadeExportedPackage* package)
	{
		Deltify<JadeExportedPackageDelta>(package);
	}

	void TaskGraph::Visit(JadeExportedProperty* property)
	{
		Deltify<JadeExportedEntityDelta<JadeExportedProperty>>(property);
	}

	void TaskGraph::Visit(JadeExposedClass* cls)
	{
		Deltify<Delta<JadeExposedClass>>(cls);
	}

	void TaskGraph::Visit(JadeExposedFeature* feature)
	{
		Deltify<Delta<JadeExposedFeature>>(feature);
	}

	void TaskGraph::Visit(JadeExposedList* list)
	{
		Deltify<SchemaComponentDelta<JadeExposedList>>(list, "JadeExposedList");
	}

	void TaskGraph::Visit(JadeHTMLDocument* document)
	{
		Deltify<JadeHTMLDocumentDelta>(document);
	}

	void TaskGraph::Visit(JadeImportedClass* cls)
	{
		Deltify<JadeImportedClassDelta>(cls);
	}

	void TaskGraph::Visit(JadeImportedConstant* constant)
	{
		Deltify<JadeImportedFeatureDelta<JadeImportedConstant>>(constant);
	}

	void TaskGraph::Visit(JadeImportedInterface* interface)
	{
		Deltify<JadeImportedInterfaceDelta>(interface);
	}

	void TaskGraph::Visit(JadeImportedMethod* method)
	{
		Deltify<JadeImportedFeatureDelta<JadeImportedMethod>>(method);
	}

	void TaskGraph::Visit(JadeImportedPackage* package)
	{
		Deltify<JadeImportedPackageDelta>(package);
	}

	void TaskGraph::Visit(JadeImportedProperty* property)
	{
		Deltify<JadeImportedFeatureDelta<JadeImportedProperty>>(property);
	}

	void TaskGraph::Visit(JadeInterface* interface)
	{
		Deltify<JadeInterfaceDelta<JadeInterface>>(interface);
	}

	void TaskGraph::Visit(JadeInterfaceMethod* method)
	{
		Deltify<MethodDelta<JadeInterfaceMethod>>(method);
	}

	void TaskGraph::Visit(JadeMethod* method)
	{
		Deltify<MethodDelta<JadeMethod>>(method);
	}

	void TaskGraph::Visit(JadeWebServicesClass* cls)
	{
		Deltify<ClassDelta<JadeWebServicesClass>>(cls);
	}

	void TaskGraph::Visit(JadeWebServicesMethod* method)
	{
		Deltify<MethodDelta<JadeWebServicesMethod>>(method);
	}

	void TaskGraph::Visit(JadeWebServiceConsumerClass* cls)
	{
		Deltify<ClassDelta<JadeWebServiceConsumerClass>>(cls);
	}

	void TaskGraph::Visit(JadeWebServiceProviderClass* cls)
	{
		Deltify<ClassDelta<JadeWebServiceProviderClass>>(cls);
	}

	void TaskGraph::Visit(JadeWebServiceSoapHeaderClass* cls)
	{
		Deltify<ClassDelta<JadeWebServiceSoapHeaderClass>>(cls);
	}

	void TaskGraph::Visit(Library* library)
	{
		Deltify<SchemaComponentDelta<Library>>(library, "Library");
	}

	void TaskGraph::Visit(Locale* locale)
	{
		Deltify<LocaleDelta>(locale);
	}

	void TaskGraph::Visit(NumberFormat* format)
	{
		Deltify<LocaleFormatDelta<NumberFormat>>(format);
	}

	void TaskGraph::Visit(PrimAttribute* attribute)
	{
		Deltify<PropertyDelta<PrimAttribute>>(attribute);
	}

	void TaskGraph::Visit(Data::PrimType* primitive)
	{
		// Don't currently need to handle primitive types explicitly, as these are all subschema copies
		// that cannot be renamed, moved or deleted.  Any method changes are handled individually.
	}

	void TaskGraph::Visit(Data::PseudoType* pseudoType)
	{
		// Not applicable in this context (we can't define our own custom pseudo types)
	}

	void TaskGraph::Visit(RelationalAttribute* attribute)
	{
		Deltify<Delta<RelationalAttribute>>(attribute);
	}

	void TaskGraph::Visit(RelationalTable* table)
	{
		Deltify<Delta<RelationalTable>>(table);
	}

	void TaskGraph::Visit(RelationalView* view)
	{
		Deltify<SchemaComponentDelta<RelationalView>>(view, "RelationalView");
	}

	void TaskGraph::Visit(Schema* schema)
	{
		Deltify<SchemaDelta>(schema);
	}

	void TaskGraph::Visit(TimeFormat* format)
	{
		Deltify<LocaleFormatDelta<TimeFormat>>(format);
	}

	void TaskGraph::Visit(TranslatableString* string)
	{
		Deltify<TranslatableStringDelta>(string);
	}
}