#include "ClassDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/JadeWebServicesClass.h>

namespace JadeGit::Build::Classic
{
	template<class TClass>
	class JadeWebServicesClassDefinition : public ClassDefinition
	{
	public:
		JadeWebServicesClassDefinition(SchemaDefinition* schema, const TClass* klass) : ClassDefinition(schema, klass) {}

	protected:
		virtual void WriteWebServicesBody(std::ostream& output, const std::string& indent, const TClass* source) const
		{
			output << indent << "additionalInfo = `" << source->additionalInfo << "`;\n";
			output << indent << "wsdl = `" << source->wsdl << "`;\n";
		}

		void WriteTypeBody(std::ostream& output, const std::string& indent) const override final
		{
			output << indent << "webServicesClassProperties\n";
			output << indent << "(\n";
			WriteWebServicesBody(output, indent + "\t", static_cast<const TClass*>(source));
			output << indent << ")\n";

			ClassDefinition::WriteTypeBody(output, indent);
		}
	};
	
	class JadeWebServiceConsumerClassDefinition : public JadeWebServicesClassDefinition<Data::JadeWebServiceConsumerClass>
	{
	public:
		using JadeWebServicesClassDefinition::JadeWebServicesClassDefinition;

	protected:
		void WriteWebServicesBody(std::ostream& output, const std::string& indent, const Data::JadeWebServiceConsumerClass* source) const override
		{
			JadeWebServicesClassDefinition::WriteWebServicesBody(output, indent, source);

			output << indent << "endPointURL = \"" << source->endPointURL << "\";\n";
			output << indent << "wsdlName = \"" << source->wsdlName << "\";\n";
			output << indent << "_useSOAP12 = " << source->useSOAP12.ToString() << ";\n";
			output << indent << "useAsyncCalls = " << source->useAsyncCalls.ToString() << ";\n";
			output << indent << "useNewPrimTypes = " << source->useNewPrimTypes.ToString() << ";\n";
		}
	};

	class JadeWebServiceProviderClassDefinition : public JadeWebServicesClassDefinition<Data::JadeWebServiceProviderClass>
	{
	public:
		using JadeWebServicesClassDefinition::JadeWebServicesClassDefinition;

	protected:
		void WriteWebServicesBody(std::ostream& output, const std::string& indent, const Data::JadeWebServiceProviderClass* source) const override
		{
			JadeWebServicesClassDefinition::WriteWebServicesBody(output, indent, source);

			output << indent << "secureService = " << source->secureService.ToString() << ";\n";
		}
	};

	class JadeWebServiceSoapHeaderClassDefinition : public JadeWebServicesClassDefinition<Data::JadeWebServiceSoapHeaderClass>
	{
	public:
		using JadeWebServicesClassDefinition::JadeWebServicesClassDefinition;

	protected:
		void WriteWebServicesBody(std::ostream& output, const std::string& indent, const Data::JadeWebServiceSoapHeaderClass* source) const override
		{
			JadeWebServicesClassDefinition::WriteWebServicesBody(output, indent, source);

			output << indent << "soapHeaderDirection = " << source->direction.ToString() << ";\n";
			output << indent << "actor = \"" << source->actor << "\";\n";
			output << indent << "mustUnderstand = " << source->mustUnderstand.ToString() << ";\n";
		}
	};

	static NodeRegistration<JadeWebServicesClassDefinition<Data::JadeWebServicesClass>, Data::JadeWebServicesClass> jadeWebServicesClass;
	static NodeRegistration<JadeWebServiceConsumerClassDefinition, Data::JadeWebServiceConsumerClass> jadeWebServiceConsumerClass;
	static NodeRegistration<JadeWebServiceProviderClassDefinition, Data::JadeWebServiceProviderClass> jadeWebServiceProviderClass;
	static NodeRegistration<JadeWebServiceSoapHeaderClassDefinition, Data::JadeWebServiceSoapHeaderClass> jadeWebServiceSoapHeaderClass;
}