#pragma once
#include "SchemaEntityDefinition.h"
#include <jadegit/data/ConstantCategory.h>

namespace JadeGit::Build::Classic
{
	class SchemaDefinition;

	class ConstantCategoryDefinition : public SchemaEntityDefinition<Data::ConstantCategory, false, false, false>
	{
	public:
		using parent_node = SchemaDefinition;

		ConstantCategoryDefinition(SchemaDefinition* schema, const Data::ConstantCategory* category);

		using SchemaEntityDefinition<Data::ConstantCategory, false, false, false>::source;

		DefinitionNodes constantDefinitions;

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override;
		void WriteBody(std::ostream& output, const std::string& indent) override;
	};
}