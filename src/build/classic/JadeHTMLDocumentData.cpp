#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/JadeHTMLDocument.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/JadeHTMLDocumentMeta.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class JadeHTMLDocumentData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		JadeHTMLDocumentData(SchemaDefinition* schema, const JadeHTMLDocument* source, const char* oldName) : DataNode(schema->data, source), oldName(oldName) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const final
		{
			auto source = static_cast<const JadeHTMLDocument*>(this->source);

			printer.PushAttribute("name", oldName ? oldName : source->name.c_str());
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			// Include schema reference
			if (property == rootSchema.jadeHTMLDocument->schema)
				return true;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}

	private:
		const char* const oldName;
	};
	static NodeRegistration<JadeHTMLDocumentData, JadeHTMLDocument, const char*> registrar;
}