#pragma once
#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/JadeExportedPackage.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/JadeExportedPackageMeta.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class JadeExportedPackageData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		JadeExportedPackageData(SchemaDefinition* schema, const JadeExportedPackage* package) : DataNode(schema->data, package) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const override
		{
			auto source = static_cast<const JadeExportedPackage*>(this->source);
			printer.PushAttribute("name", source->name.c_str());
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const override
		{
			return property == rootSchema.jadeExportedPackage->application;
		}
	};
	static NodeRegistration<JadeExportedPackageData, JadeExportedPackage> registrar;
}