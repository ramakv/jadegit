#pragma once
#include "Node.h"
#include <jadegit/data/Object.h>

namespace JadeGit
{
	class XMLPrinter;
}

namespace JadeGit::Data
{
	class CollClass;
	class Collection;
}

namespace JadeGit::Build::Classic
{
	class DataNodes;

	class DataNode : public Node
	{
	public:
		using base_node = DataNode;

		DataNode(const Data::Object* source, const char* type = nullptr);
		DataNode(DataNodes& nodes, const Data::Object* source, const char* type = nullptr);

		void Print(XMLPrinter& printer) const;

	protected:
		const char* const type;
		const Data::Object* const source;

		virtual void PrintAttributes(XMLPrinter& printer) const = 0;
		virtual void PrintData(XMLPrinter& printer) const;
				void PrintData(XMLPrinter& printer, const Data::Object* source) const;

		virtual bool PrintPropertyFilter(const Data::RootSchema& rootSchema, const Data::Property* property) const;
		virtual void PrintCollection(XMLPrinter& printer, const Data::RootSchema& rootSchema, const Data::Property* property, const Data::CollClass& type, const Data::Type& memberType, const Data::Collection& coll) const;
				void PrintCollection(XMLPrinter& printer, const Data::Collection& coll, const char* tag, bool primitive) const;
		virtual void PrintValue(XMLPrinter& printer, const Data::Object* value) const;
				void PrintValue(XMLPrinter& printer, const Data::Type& type, const Data::Any& value, bool basic) const;
				void PrintValue(XMLPrinter& printer, const Data::Any& value, bool basic = true) const;

		virtual bool useBasicFormatting(const Data::RootSchema& rootSchema, const Data::Property* property) const;

	private:
		void PrintSignatureData(XMLPrinter& printer, const Data::Entity& source) const;
	};

	class DataNodes : public std::vector<DataNode*>
	{
	public:
		void Print(XMLPrinter& printer) const;
	};
}