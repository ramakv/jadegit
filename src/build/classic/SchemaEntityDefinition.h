#pragma once
#include "SchemaDefinitionNode.h"
#include <jadegit/data/SchemaEntity.h>

namespace JadeGit::Build::Classic
{
	template <class TEntity, bool declaration = false, bool bracketed = false, bool terminated = true>
	class SchemaEntityDefinition : public SchemaDefinitionNode<TEntity, bracketed, terminated>
	{
	public:
		using SchemaDefinitionNode<TEntity, bracketed, terminated>::SchemaDefinitionNode;
	
	protected:
		using SchemaDefinitionNode<TEntity, bracketed, terminated>::source;
		using SchemaDefinitionNode<TEntity, bracketed, terminated>::WriteAttribute;

		bool transient = false;

		void WriteAttributes(std::ostream& output) override
		{
			SchemaDefinitionNode<TEntity, bracketed, terminated>::WriteAttributes(output);

			WriteAttribute(output, "protected", source->access == Data::SchemaEntity::Protected);
			WriteAttribute(output, "readonly", source->access == Data::SchemaEntity::ReadOnly);
			WriteAttribute(output, "abstract", source->abstract);
			WriteAttribute(output, "transient", transient);
			WriteAttribute(output, "subschemaHidden", source->subAccess == Data::SchemaEntity::Hidden);
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			SchemaDefinitionNode<TEntity, bracketed, terminated>::WriteBody(output, indent);

			if constexpr (!declaration)
			{
				this->WriteDocumentation(output);
				this->WriteModifiedTimeStamp(output);
			}
		}
	};
}