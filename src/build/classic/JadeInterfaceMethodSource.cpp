#include "MethodSource.h"
#include "TypeSource.h"
#include <jadegit/data/JadeInterfaceMethod.h>

namespace JadeGit::Build::Classic
{
	class JadeInterfaceMethodSource : public MethodSource
	{
	public:
		JadeInterfaceMethodSource(SchemaDefinition* schema, TypeSource* type, const Data::JadeInterfaceMethod* method) : MethodSource(schema, type, method)
		{
			type->jadeMethodSources.push_back(this);
		}
	};
	static NodeRegistration<JadeInterfaceMethodSource, Data::JadeInterfaceMethod> registrar;
}