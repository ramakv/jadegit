#pragma once
#include "DefinitionNode.h"
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/FileSignature.h>
#include <iomanip>

namespace JadeGit::Build::Classic
{
	template<class TEntity, bool bracketed = true, bool terminated = true>
	class SchemaDefinitionNode : public DefinitionNode
	{
	public:
		SchemaDefinitionNode(const TEntity* entity) : source(entity) {}

		const TEntity* source = nullptr;

	protected:
		virtual void WriteAttributes(std::ostream& output) {}

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			output << indent << source->GetName();
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			WriteAttributes(output);

			if constexpr (bracketed)
				output << "\n" << indent << "(\n";
			else if constexpr (terminated)
				output << ";\n";
			else
				output << "\n";
		}		

		void WriteExit(std::ostream& output, const std::string& indent) override
		{
			if constexpr(bracketed)
				output << indent << ")\n";
		}

		template<class TSource>
		void WriteDocumentation(const TSource* source, std::ostream& output) const
		{
			if (source->text != "")
			{
				output << "\tdocumentationText\n`";

				for (auto c : source->text)
				{
					if (c == '`')
						output << "``";
					else
						output << c;
				}
					
				output << "`\n";
			}
		}

		void WriteDocumentation(std::ostream& output) const
		{
			WriteDocumentation(source, output);
		}

		void WriteModifiedTimeStamp(std::ostream& output) const
		{
			if (auto& signature = source->getSignature(); !signature.empty())
			{
				output << std::format("\t\tsetModifiedTimeStamp \"{}\" \"{}\"", signature.author.empty() ? "<unknown>" : signature.author, std::string(source->GetRootSchema().version));

				if (signature.patch)
					output << std::format(" {}", signature.patch);

				output << std::format(" {0:%Y:%m:%d:%T};\n", floor<milliseconds>(signature.modified));
			}
		}
	};
}