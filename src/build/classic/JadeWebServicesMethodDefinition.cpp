#include "MethodDefinition.h"
#include "TypeDefinition.h"
#include <jadegit/data/JadeWebServicesClass.h>
#include <jadegit/data/JadeWebServicesMethod.h>
#include <jadegit/data/ScriptElement.h>

namespace JadeGit::Build::Classic
{
	class JadeWebServicesMethodDefinition : public MethodDefinition
	{
	public:
		JadeWebServicesMethodDefinition(SchemaDefinition* schema, TypeDefinition* type, const Data::JadeWebServicesMethod* method) : MethodDefinition(schema, type, method, true), source(method)
		{
			type->webServicesMethodDefinitions.push_back(this);
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			MethodDefinition::WriteBody(output, indent);

			output << indent << "webServicesMethodProperties\n";
			output << indent << "(\n";
			
			output << indent << "\tinputEncodingStyle = \"" << source->inputEncodingStyle << "\";\n";
			output << indent << "\tinputNamespace = \"" << source->inputNamespace << "\";\n";
			output << indent << "\tinputUsesEncodedFormat = " << source->inputUsesEncodedFormat.ToString() << ";\n";
			output << indent << "\toutputEncodingStyle = \"" << source->outputEncodingStyle << "\";\n";
			output << indent << "\toutputNamespace = \"" << source->outputNamespace << "\";\n";
			output << indent << "\toutputUsesEncodedFormat = " << source->outputUsesEncodedFormat.ToString() << ";\n";
			output << indent << "\tsoapAction = \"" << source->soapAction << "\";\n";
			output << indent << "\tuseBareStyle = " << source->useBareStyle.ToString() << ";\n";
			output << indent << "\tuseSoap12 = " << source->useSoap12.ToString() << ";\n";
			output << indent << "\tusesRPC = " << (source->usesRPC != Data::JadeWebServicesMethod::RPC::None ? source->usesRPC : Data::Value(Data::JadeWebServicesMethod::RPC::Default)).ToString() << ";\n";
			output << indent << "\twsdlName = \"" << source->wsdlName << "\";\n";

			output << indent << "\tsoapHeaders = ";
			bool delim = false;
			for (auto& header : source->soapHeaders)
			{
				if (delim)
					output << ", ";
				else
					delim = true;

				output << header->name;
			}
			if (!delim)
				output << "null";
			output << ";\n";

			for (auto& param : source->parameters)
			{
				if (!param->wsdlName.empty())
					output << indent << "\tparameterWsdlName = \"" << param->name << "=" << param->wsdlName << "\";\n";
			}

			output << indent << ")\n";
		}

	private:
		const Data::JadeWebServicesMethod* const source;
	};

	static NodeRegistration<JadeWebServicesMethodDefinition, Data::JadeWebServicesMethod> registrar;
}