#include "MethodDefinition.h"
#include "JadeInterfaceDefinition.h"

namespace JadeGit::Build::Classic
{
	class JadeInterfaceMethodDefinition : public MethodDefinition
	{
	public:
		using parent_node = JadeInterfaceDefinition;

		JadeInterfaceMethodDefinition(SchemaDefinition* schema, JadeInterfaceDefinition* interface, const Data::JadeInterfaceMethod* method) : MethodDefinition(schema, interface, method)
		{
			interface->jadeMethodDefinitions.push_back(this);	// Yep, it's inconsistent 
		}
	};

	static NodeRegistration<JadeInterfaceMethodDefinition, Data::JadeInterfaceMethod> registrar;
}