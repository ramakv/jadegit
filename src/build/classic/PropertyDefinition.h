#pragma once
#include "FeatureDefinition.h"

namespace JadeGit::Build::Classic
{
	class ClassDefinition;

	template <class TProperty>
	class PropertyDefinition : public FeatureDefinition<TProperty>
	{
	public:
		using parent_node = ClassDefinition;

		using FeatureDefinition<TProperty>::FeatureDefinition;

	protected:
		using FeatureDefinition<TProperty>::source;
		using FeatureDefinition<TProperty>::WriteAttribute;
		
		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			FeatureDefinition<TProperty>::WriteEnter(output, indent);
			output << ": " << source->GetType()->GetLocalName();
		}

		void WriteAttributes(std::ostream& output) override
		{
			FeatureDefinition<TProperty>::WriteAttributes(output);

			WriteAttribute(output, "html", source->isHTMLProperty);
			WriteAttribute(output, "required", source->required);
			WriteAttribute(output, "virtual", source->virtual_);
			WriteAttribute(output, "xmlAttribute", source->xmlAttribute);
			WriteAttribute(output, "xmlNillable", source->xmlNillable);
			WriteAttribute(output, "webService", source->webService);
			WriteAttribute(output, "wsdlName", source->wsdlName);
		}
	};

	template <class TBase, class TProperty>
	class DynamicPropertyDefinition : public TBase
	{
	public:
		using TBase::TBase;

	protected:
		using TBase::WriteAttribute;

		void WriteAttributes(std::ostream& output) final
		{
			TBase::WriteAttributes(output);

			auto source = static_cast<const TProperty*>(this->source);

			if (!source->dynamicPropertyCluster)
				throw jadegit_exception("Missing dynamic property cluster");

			WriteAttribute(output, "dynamicPropertyCluster", source->dynamicPropertyCluster->name.c_str());
		}
	};
}