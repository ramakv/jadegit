#include "MethodDefinition.h"
#include "TypeDefinition.h"

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	MethodDefinition::MethodDefinition(SchemaDefinition* schema, TypeDefinition* type, const Method* method, bool webService) : RoutineDefinition(schema, method), source(method), webService(webService)
	{
		// Add event method mapping
		if (method->controlMethod)
			type->eventMethodMappings[method->GetName()] = method->controlMethod;
	}

	void MethodDefinition::WriteAttributes(std::ostream& output)
	{
		WriteAttribute(output, "updating", source->updating);

		RoutineDefinition::WriteAttributes(output);
		
		WriteAttribute(output, "mapping", source->invocation == Method::Invocation::Mapping);
		WriteAttribute(output, "lockReceiver", source->lockReceiver);
		WriteAttribute(output, "partitionMethod", source->partitionMethod);
		WriteAttribute(output, "subschemaFinal", source->subschemaFinal);
		WriteAttribute(output, "subschemaCopyFinal", source->subschemaCopyFinal);
		WriteAttribute(output, "final", source->final);
		WriteAttribute(output, "condition", source->condition);
		WriteAttribute(output, "conditionSafe", source->conditionSafe);
		WriteAttribute(output, "typeMethod", source->isTypeMethod());
		WriteAttribute(output, "unitTest", source->unitTestFlags == Method::UnitTestFlags::Test);
		WriteAttribute(output, "unitTestIgnore", source->unitTestFlags == Method::UnitTestFlags::Ignore);
		WriteAttribute(output, "unitTestBefore", source->unitTestFlags == Method::UnitTestFlags::Before);
		WriteAttribute(output, "unitTestBeforeClass", source->unitTestFlags == Method::UnitTestFlags::BeforeClass);
		WriteAttribute(output, "unitTestBeforeAll", source->unitTestFlags == Method::UnitTestFlags::BeforeAll);
		WriteAttribute(output, "unitTestAfter", source->unitTestFlags == Method::UnitTestFlags::After);
		WriteAttribute(output, "unitTestAfterClass", source->unitTestFlags == Method::UnitTestFlags::AfterClass);
		WriteAttribute(output, "unitTestAfterAll", source->unitTestFlags == Method::UnitTestFlags::AfterAll);
		WriteAttribute(output, "webService", webService);
		WriteAttribute(output, "wsdlName", source->wsdlName);
	}
}