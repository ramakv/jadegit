#include "MethodSource.h"
#include "TypeSource.h"
#include <jadegit/data/JadeWebServicesMethod.h>

namespace JadeGit::Build::Classic
{
	class JadeWebServicesMethodSource : public MethodSource
	{
	public:
		JadeWebServicesMethodSource(SchemaDefinition* schema, TypeSource* type, const Data::JadeWebServicesMethod* method) : MethodSource(schema, type, method)
		{
			type->webServicesMethodSources.push_back(this);
		}
	};

	static NodeRegistration<JadeWebServicesMethodSource, Data::JadeWebServicesMethod> registrar;
}