#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/ActiveXLibrary.h>
#include <jadegit/data/ActiveXClass.h>
#include <jadegit/data/ActiveXFeature.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/Method.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ActiveXClassMeta.h>
#include <jadegit/data/RootSchema/ActiveXFeatureMeta.h>
#include <jadegit/data/RootSchema/ActiveXInterfaceMeta.h>
#include <jadegit/data/RootSchema/ActiveXLibraryMeta.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class ActiveXFeatureData : public DataNode
	{
	public:
		ActiveXFeatureData(const ActiveXFeature* source) : DataNode(source) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const override {}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const override
		{
			// Include feature reference
			if (property == rootSchema.activeXFeature->feature)
				return true;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}
	};

	class ActiveXClassData : public DataNode
	{
	public:
		ActiveXClassData(const ActiveXClass* source) : DataNode(source) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const override {}

		void PrintData(XMLPrinter& printer) const override
		{
			DataNode::PrintData(printer);

			auto source = static_cast<const ActiveXClass*>(this->source);

			printer.OpenElement("activexFeatures");
			
			PrintFeatures(printer, source->baseClass->constants);
			PrintFeatures(printer, source->baseClass->properties);
			PrintFeatures(printer, source->baseClass->methods);

			printer.CloseElement();
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const override
		{
			// Exclude library reference
			if (property == rootSchema.activeXClass->activeXLibrary)
				return false;

			// Include baseClass reference
			if (property == rootSchema.activeXClass->baseClass)
				return true;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}
	
	private:
		template <class TFeatures>
		void PrintFeatures(XMLPrinter& printer, const TFeatures& features) const
		{
			for (auto& feature : features)
			{
				const ActiveXFeature* activeXFeature = feature.second->activeXFeature;
				if (activeXFeature)
					ActiveXFeatureData(activeXFeature).Print(printer);
			}
		}
	};

	class ActiveXLibraryData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		ActiveXLibraryData(SchemaDefinition* schema, const ActiveXLibrary* library) : DataNode(schema->data, library) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const override
		{
			auto source = static_cast<const ActiveXLibrary*>(this->source);

			printer.PushAttribute("guid", source->guid.ToString().c_str());
			printer.PushAttribute("activeXClass", source->name.c_str());
		}

		void PrintData(XMLPrinter& printer) const override
		{
			DataNode::PrintData(printer);
			
			auto source = static_cast<const ActiveXLibrary*>(this->source);

			ActiveXClass* activeXClass = nullptr;
			std::vector<ActiveXClass*> coClasses;
			std::vector<ActiveXClass*> dispInterfaces;

			const Class* activeXInterface = *(source->GetRootSchema().activeXInterface);

			for (auto const& [key, cls] : source->classes)
			{
				if (cls->baseClass->name == source->name)
				{
					assert(!activeXClass);
					activeXClass = cls;
				}
				else if (cls->baseClass->InheritsFrom(activeXInterface))
				{
					dispInterfaces.push_back(cls);
				}
				else
				{
					coClasses.push_back(cls);
				}
			}

			printer.OpenElement("activeXClass");
			if (activeXClass)
				ActiveXClassData(activeXClass).Print(printer);
			printer.CloseElement();

			PrintClasses(printer, dispInterfaces, "dispInterfaces");
			PrintClasses(printer, coClasses, "coClasses");
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const override
		{
			// Include schema reference
			if (property == rootSchema.activeXLibrary->_schema)
				return true;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}

	private:
		void PrintClasses(XMLPrinter& printer, const std::vector<ActiveXClass*>& classes, const char* name) const
		{
			printer.OpenElement(name);
			for (auto& cls : classes)
				ActiveXClassData(cls).Print(printer);
			printer.CloseElement();
		}
	};
	static NodeRegistration<ActiveXLibraryData, ActiveXLibrary> registrar;
}