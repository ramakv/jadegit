#include "SchemaDefinition.h"
#include "ClassDefinition.h"
#include "TypeDeclaration.h"
#include "TypeSource.h"
#include <jadegit/data/JadeImportedPackage.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class JadeImportedPackageDefinition : public SchemaEntityDefinition<JadeImportedPackage, false, true>
	{
	public:
		using parent_node = SchemaDefinition;

		DefinitionNodes importedClassDefinitions;
		DefinitionNodes importedInterfaceDefinitions;

		JadeImportedPackageDefinition(SchemaDefinition* schema, const JadeImportedPackage* package) : SchemaEntityDefinition(package),
			importedClassDefinitions("importedClassDefinitions"),
			importedInterfaceDefinitions("importedInterfaceDefinitions")
		{
			schema->importedPackageDefinitions.push_back(this);
		}

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			SchemaEntityDefinition::WriteEnter(output, indent);
			output << " is " << source->exportedPackage->GetQualifiedName();
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			SchemaEntityDefinition::WriteBody(output, indent);
			importedClassDefinitions.Write(output, indent + "\t");
			importedInterfaceDefinitions.Write(output, indent + "\t");
		}
	};

	class JadeImportedClassHeader : public TypeDeclaration
	{
	public:
		using parent_node = JadeImportedPackageDefinition;

		JadeImportedClassHeader(JadeImportedPackageDefinition* package, const JadeImportedClass* cls) : TypeDeclaration(cls)
		{
			package->importedClassDefinitions.push_back(this);
		}

		void WriteEnter(std::ostream& output, const std::string& indent) override
		{
			output << indent << source->GetName() << "\n";
		}

		void WriteBody(std::ostream& output, const std::string& indent) override
		{
			WriteModifiedTimeStamp(output);
		}
	};

	class JadeImportedInterfaceDefinition : public SchemaEntityDefinition<JadeImportedInterface, false, false, false>
	{
	public:
		using parent_node = JadeImportedPackageDefinition;

		JadeImportedInterfaceDefinition(JadeImportedPackageDefinition* package, const JadeImportedInterface* interface) : SchemaEntityDefinition(interface)
		{
			package->importedInterfaceDefinitions.push_back(this);
		}
	};

	static NodeRegistration<JadeImportedPackageDefinition, JadeImportedPackage> package;
	static NodeRegistration<JadeImportedClassHeader, JadeImportedClass> classHeader;
	static NodeRegistration<ClassDefinition, JadeImportedClass> classDefinition(true);
	static NodeRegistration<TypeSource, JadeImportedClass> classSource(true);
	static NodeRegistration<JadeImportedInterfaceDefinition, JadeImportedInterface> interface;
}