#include "RoutineSource.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Function.h>

namespace JadeGit::Build::Classic
{
	class FunctionSource : public RoutineSource
	{
	public:
		using parent_node = SchemaDefinition;

		FunctionSource(SchemaDefinition* schema, const Data::Function* function) : RoutineSource(schema, function)
		{
			schema->externalFunctionSources.push_back(this);
		}
	};

	static NodeRegistration<FunctionSource, Data::Function> registrar;
}