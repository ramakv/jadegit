#include "MethodDefinition.h"
#include "TypeDefinition.h"

namespace JadeGit::Build::Classic
{
	class ExternalMethodDefinition : public MethodDefinition
	{
	public:
		ExternalMethodDefinition(SchemaDefinition* schema, TypeDefinition* type, const Data::ExternalMethod* method) : MethodDefinition(schema, type, method)
		{
			type->externalMethodDefinitions.push_back(this);
		}
	};

	static NodeRegistration<ExternalMethodDefinition, Data::ExternalMethod> registrar;
}