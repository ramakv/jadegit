#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Development.h>
#include <jadegit/data/Reference.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/DevControlPropertiesMeta.h>
#include <xml/XMLPrinter.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class DevControlPropertyData : public DataNode
	{
	public:
		DevControlPropertyData(const Data::DevControlProperties* source) : DataNode(source, "PainterControlProperty") {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const
		{
			auto source = static_cast<const DevControlTypes*>(this->source);

			printer.PushAttribute("name", source->name.c_str());
		}

		bool PrintPropertyFilter(const Data::RootSchema& rootSchema, const Data::Property* property) const override
		{
			// Include parent reference
			if (property == rootSchema.devControlProperties->parent)
				return true;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}
	};

	class DevControlTypesData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		DevControlTypesData(SchemaDefinition* schema, const Data::DevControlTypes* development) : DataNode(schema->data, development, "PainterControlDefinition") {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const
		{
			auto source = static_cast<const DevControlTypes*>(this->source);

			printer.PushAttribute("name", source->name.c_str());
		}

		void PrintData(XMLPrinter& printer) const
		{
			DataNode::PrintData(printer);

			printer.OpenElement("PainterControlProperties");

			auto source = static_cast<const DevControlTypes*>(this->source);
			for (auto& prop : source->controlProps)
				DevControlPropertyData(prop).Print(printer);

			printer.CloseElement();
		}
	};
	static NodeRegistration<DevControlTypesData, DevControlTypes> registrar;
}