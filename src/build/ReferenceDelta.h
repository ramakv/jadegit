#pragma once
#include "PropertyDelta.h"
#include <jadegit/data/Reference.h>

namespace JadeGit::Build
{
	template<class TComponent>
	class ReferenceDelta : public PropertyDelta<TComponent>
	{
	public:
		using PropertyDelta<TComponent>::PropertyDelta;

		using PropertyDelta<TComponent>::previous;
		using PropertyDelta<TComponent>::latest;

	protected:
		using PropertyDelta<TComponent>::graph;
		using PropertyDelta<TComponent>::GetDefinition;
		using PropertyDelta<TComponent>::GetDeletion;

		bool AnalyzeEnter() override
		{
			/* Analyze previous constraint */
			const IDelta* previousConstraint = nullptr;
			if (previous)
				previousConstraint = graph.Analyze<IDelta>(previous->constraint);

			/* Basic analysis */
			if (!PropertyDelta<TComponent>::AnalyzeEnter())
			{
				/* Handle deletion before deleting previous constraint */
				if (previousConstraint)
					graph.AddEdge(GetDeletion(), previousConstraint->GetDeletion());

				return false;
			}

			/* Analyze latest constraint */
			const IDelta* latestConstraint = nullptr;
			latestConstraint = graph.Analyze<IDelta>(latest->constraint);

			/* Definition is dependent on latest constraint */
			if (latestConstraint)
				graph.AddEdge(latestConstraint->GetDefinition(), GetDefinition());

			/* Handle update before deleting previous constraint */
			if (previousConstraint && previousConstraint != latestConstraint)
				graph.AddEdge(GetDefinition(), previousConstraint->GetDeletion());

			return true;
		}
	};
}