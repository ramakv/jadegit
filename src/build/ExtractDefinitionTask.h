#pragma once
#include "ExtractTask.h"

namespace JadeGit::Build
{
	template<class TComponent>
	concept has_extract_definition_strategy = requires (ExtractStrategy & strategy, const TComponent * component, bool complete) {
		strategy.Define(component, complete);
	};

	template<class TComponent>
	class ExtractDefinitionTask : public ExtractTask
	{
	public:
		ExtractDefinitionTask(TaskGraph &graph, const TComponent* component, bool complete) : ExtractTask(graph, complete), component(component)
		{
			assert(component);
		};

		bool Execute(ExtractStrategy &strategy) const override
		{
			return strategy.Define(component, complete);
		}

		void Print(std::ostream& output) const override
		{
			output << "Define " << *component;
			if (complete)
				output << ", complete";
		}

	protected:
		const TComponent* component;
	};
}