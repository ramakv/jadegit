#pragma once
#include "CommandTask.h"

namespace JadeGit::Build
{
	class CommandMoveClassTask : public CommandTask
	{
	public:
		std::string qualifiedName;
		std::string newSuperClass;

		CommandMoveClassTask(TaskGraph &graph, std::string qualifiedName, std::string newSuperClass);

		void Execute(CommandStrategy &strategy) const override;
		void Print(std::ostream& output) const override;
	};
}