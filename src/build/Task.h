#pragma once
#include <jadegit/MemoryAllocated.h>
#include <ostream>

namespace JadeGit::Build
{
	class TaskGraph;
	class TaskVisitor;

	class Task : virtual public MemoryAllocated
	{
	public:
		Task(TaskGraph &graph, bool complete = false);
		virtual ~Task() = default;

		const bool complete;

		virtual bool Accept(TaskVisitor &v) const = 0;
		virtual void Print(std::ostream& output) const = 0;
		virtual int Priority() const { return 0; }
	};

	std::ostream& operator<< (std::ostream& stream, const Task& task);
}
