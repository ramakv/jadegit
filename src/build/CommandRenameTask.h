#pragma once
#include "CommandTask.h"

namespace JadeGit::Build
{
	class CommandRenameTask : public CommandTask
	{
	public:
		const char* entityType;
		std::string qualifiedName;
		std::string newName;

		CommandRenameTask(TaskGraph &graph, const char* entityType, std::string qualifiedName, std::string newName);

		void Execute(CommandStrategy &strategy) const override;
		void Print(std::ostream& output) const override;
	};
}