#include "Delta.h"
#include "TaskGraph.h"
#include <jadegit/Exception.h>

namespace JadeGit::Build
{
	void IDelta::Analyze()
	{
		/* Ignore repeated calls to analyze */
		if (analyzed)
			return;

		/* Analyze */
		AnalyzeEnter();
		AnalyzeExit();
	}

	bool IDelta::AnalyzeEnter()
	{
		/* Guard against recursive calls to re-analyze self */
		if (analyzing)
			throw jadegit_exception("Cyclic build command");

		/* Set flag indicating analysis is in progress */
		analyzing = true;

		return true;
	}

	void IDelta::AnalyzeExit()
	{
		/* Set flag indicating analysis has been completed */
		analyzed = true;

		/* Reset analysis in progress flag */
		analyzing = false;
	}

	bool IDelta::ParentCreated(Task* command)
	{
		if (!analyzed)
			return false;

		/* Parent must be established before child */
		if (Task* creation = GetCreation())
		{
			graph.AddEdge(command, creation);
			return false;
		}

		/* Notify children */
		return true;
	}

	bool IDelta::ParentDeleted(Task* command)
	{
		if (!analyzed)
			return false;

		/* Parent deletions must occur after explicit child deletions */
		if (Task* deletion = GetDeletion())
		{
			graph.AddEdge(deletion, command);
			return false;
		}

		/* Notify children */
		return true;
	}
}