#pragma once
#include "ExtractDefinitionTask.h"

namespace JadeGit::Build
{
	template<class TComponent>
	class ExtractDefinitionRenameTask : public ExtractDefinitionTask<TComponent>
	{
	public:
		ExtractDefinitionRenameTask(TaskGraph &graph, const TComponent* component, bool complete, const char* oldName) : ExtractDefinitionTask<TComponent>(graph, component, complete), oldName(oldName)
		{
		};

		bool Execute(ExtractStrategy &strategy) const override
		{
			return strategy.Define(this->component, this->complete, oldName);
		}

		void Print(std::ostream& output) const override
		{
			ExtractDefinitionTask<TComponent>::Print(output);
			if (oldName)
				output << ", previously " << oldName;
		}

	protected:
		const char* oldName = nullptr;
	};
}