#pragma once
#include "ClassDelta.h"
#include <jadegit/data/Form.h>

namespace JadeGit::Build
{
	class FormDelta : public Delta<Form>
	{
	public:
		using Delta::Delta;

	protected:
		bool AnalyzeEnter() override
		{
			/* Handle deletion */
			if (!Delta::AnalyzeEnter() || !latest)
			{
				// Forms are deleted implicitly when class or locale are deleted
				return false;
			}

			/* Dependent on form class */
			const IClassDelta* formClass = graph.Analyze<IClassDelta>(latest->GetClass());
			if (formClass)
				graph.AddEdge(formClass->GetDefinition(), GetDefinition());

			/* Dependent on super-form */
			const Form* superForm = latest->GetSuperForm();
			while (superForm)
			{
				if (const FormDelta * delta = graph.Analyze<FormDelta>(superForm))
				{
					graph.AddEdge(delta->GetDefinition(), GetDefinition());
					break;
				}

				superForm = superForm->GetSuperForm();
			}

			return true;
		}
	};
}