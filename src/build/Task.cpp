#include "Task.h"
#include "TaskGraph.h"
#include "BuildTask.h"
#include "CommandTask.h"
#include "ExtractTask.h"
#include "TaskVisitor.h"

namespace JadeGit::Build
{
	Task::Task(TaskGraph &graph, bool complete) : complete(complete)
	{
		graph.AddVertex(this);
		graph.commands.push_back(this);
	}

	std::ostream& operator<< (std::ostream& stream, const Task& task)
	{
		task.Print(stream);
		return stream;
	}

	bool BuildTask::Accept(TaskVisitor &v) const
	{
		return v.Visit(this);
	}

	bool CommandTask::Accept(TaskVisitor &v) const
	{
		return v.Visit(this);
	}

	bool ExtractTask::Accept(TaskVisitor &v) const
	{
		return v.Visit(this);
	}
}