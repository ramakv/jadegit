#pragma once
#include "SchemaComponentDelta.h"

namespace JadeGit::Build
{
	class IFeatureDelta : public ISchemaComponentDelta
	{
	public:
		using ISchemaComponentDelta::ISchemaComponentDelta;

		// No feature specific commands (as yet)
	};

	template<class TComponent, class TInterface = IFeatureDelta>
	class FeatureDelta : public SchemaComponentDelta<TComponent, TInterface>
	{
	public:
		using SchemaComponentDelta<TComponent, TInterface>::SchemaComponentDelta;
		using SchemaComponentDelta<TComponent, TInterface>::graph;
		using SchemaComponentDelta<TComponent, TInterface>::previous;
		using SchemaComponentDelta<TComponent, TInterface>::latest;
		using SchemaComponentDelta<TComponent, TInterface>::GetDefinition;

		// TODO: Moving classes is dependent on features first being renamed or deleted to prevent conflicts with features in hierarchy to which they're being moved 
	
		bool AnalyzeEnter() override
		{
			// Analyze latest schema type
			const ITypeDelta* latestSchemaType = nullptr;
			if (latest)
				latestSchemaType = graph.Analyze<ITypeDelta>(latest->schemaType);

			// Basic analysis
			if (!SchemaComponentDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				// Handle deleting subschema copy features before deleting root superschema type
				auto rootType = previous->schemaType ? &previous->schemaType->getRootType() : nullptr;
				if (previous->schemaType != rootType)
				{
					if (auto delta = graph.Analyze<IDelta>(rootType))
						graph.AddEdge(this->GetDeletion(), delta->GetDeletion());
				}

				return false;
			}

			// Feature definition is dependent on latest schema type being created/renamed
			if (latestSchemaType)
				graph.AddEdge(latestSchemaType->GetCreation(), GetDefinition());

			return true;
		}
	};
}