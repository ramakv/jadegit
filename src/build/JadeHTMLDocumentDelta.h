#pragma once
#include "ClassDelta.h"
#include "ExtractDefinitionRenameTask.h"
#include <jadegit/data/JadeHTMLDocument.h>

namespace JadeGit::Build
{
	class JadeHTMLDocumentDelta : public SchemaComponentDelta<JadeHTMLDocument>
	{
	public:
		JadeHTMLDocumentDelta(TaskGraph& graph) : SchemaComponentDelta(graph, "JadeHTMLDocument") {}

	protected:
		bool AnalyzeEnter() override
		{
			// Analyze HTML class
			htmlClass = graph.Analyze<IClassDelta>(latest ? latest->htmlClass : previous->htmlClass);

			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			return true;
		}

		Task* HandleDefinition(bool complete) override
		{
			if (previous && previous->name != latest->name)
				return new ExtractDefinitionRenameTask<JadeHTMLDocument>(graph, latest, complete, previous->name.c_str());

			return SchemaComponentDelta::HandleDefinition(complete);
		}

		Task* HandleDeletion() override
		{
			// Deletion implied by HTML class deletion
			if (htmlClass)
				return htmlClass->GetDeletion();

			return SchemaComponentDelta::HandleDeletion();
		}

		Task* HandleRename() override
		{
			// Rename handled implicitly by definition
			return GetDefinition();
		}

	private:
		IClassDelta* htmlClass = nullptr;
	};
}