#pragma once
#include <jadegit/data/Object.h>
#include <functional>
#include "ExtractDefinitionTask.h"
#include "TaskWaypoint.h"

namespace JadeGit::Build
{
	class Task;
	class TaskGraph;

	class IDelta
	{
	public:
		IDelta(TaskGraph& graph) : graph(graph) {}
		virtual ~IDelta() = default;

		bool analyzed = false;

		void Analyze();

		virtual Task* GetCreation() const = 0;
		virtual Task* GetDefinition() const = 0;
		virtual Task* GetDeletion() const = 0;
		virtual int Level() const = 0;

		bool ParentCreated(Task* command);
		bool ParentDeleted(Task* command);

	protected:
		TaskGraph& graph;

		virtual bool AnalyzeEnter();	/* Returns false for deletion */
		virtual void AnalyzeExit();

	private:
		bool analyzing = false;
	};

	template<class TEntity, class TInterface = IDelta>
	class Delta : virtual public MemoryAllocated, public TInterface
	{
	public:
		using TInterface::TInterface;
		using TInterface::graph;

		TEntity* previous = nullptr;
		TEntity* latest = nullptr;

		Task* GetCreation() const override
		{
			return previous ? nullptr : GetDefinition();
		}

		Task* GetDefinition() const override final
		{
			return definition;
		}

		Task* GetDeletion() const override
		{
			return deletion;
		}

		int Level() const override final
		{
			return (previous ? previous->getLevel() : latest->getLevel());
		}

	protected:
		bool implicitDeletion = false;

		bool AnalyzeEnter() override
		{
			if (!TInterface::AnalyzeEnter())
				return false;

			// Handle deletion
			if (!latest)
			{
				deletion = HandleDeletion();
				return false;
			}

			// Handle definition, subject to waypoints used to identify general prerequisites for major entities
			if (definition = HandleDefinition(!previous))
			{
				auto parent = TaskWaypoint::make(graph, latest->getParentEntity(), TaskWaypoint::Definition);

				if constexpr (Data::is_major_entity<TEntity>)
				{
					auto waypoint = TaskWaypoint::make(graph, latest, TaskWaypoint::Definition);
					graph.AddEdge(parent, waypoint);
					graph.AddEdge(waypoint, definition);
				}
				else
				{
					graph.AddEdge(parent, definition);
				}
			}

			return true;
		}

		void AnalyzeExit() override
		{
			if (Task* creation = GetCreation())
			{
				/* Parent must be established before child */
				if (const IDelta* parent = graph.Find<IDelta>(latest->getParentObject()))
					graph.AddEdge(parent->GetCreation(), creation);

				/* Notify latest children of parent creation */
				Cascade<IDelta>(latest, std::bind(&IDelta::ParentCreated, std::placeholders::_1, creation));
			}

			/* Notify previous children of parent deletion */
			if(Task* deletion = GetDeletion())
				Cascade<IDelta>(previous, std::bind(&IDelta::ParentDeleted, std::placeholders::_1, deletion));

			TInterface::AnalyzeExit();
		}

		template<class TDelta>
		void Cascade(const Data::Object* component, std::function<bool(TDelta*)> notification)
		{
			if (component != previous && component != latest)
			{
				TDelta* delta = graph.Find<TDelta>(component);
				if (delta && !notification(delta))
					return;
			}

			for (auto& child : component->children)
				Cascade(child, notification);
		}

		virtual Task* HandleDefinition(bool complete)
		{
			if constexpr (has_extract_definition_strategy<TEntity>)
				return new ExtractDefinitionTask<TEntity>(graph, latest, complete);

			return nullptr;
		}

		virtual Task* HandleDeletion()
		{
			auto parent = previous->getParentObject();
			while (parent)
			{
				/* Check if parent has been changed */
				if (const IDelta * delta = graph.Find<IDelta>(parent))
				{
					/* Check if parent change has been analyzed */
					if (delta->analyzed)
					{
						/* Check for parent deletion */
						if (Task * deletion = delta->GetDeletion())
						{
							implicitDeletion = true;
							return deletion;
						}

						/* Check for complete parent definition */
						Task* definition = delta->GetDefinition();
						if (definition && definition->complete)
						{
							implicitDeletion = true;
							return definition;
						}

						break;
					}
				}

				/* Check grandparent */
				parent = parent->getParentObject();
			}

			return nullptr;
		}

		// Determines if whether previous/latest versions of an entity appear the same
		template<class TEntity, class TDelta = IDelta>
		bool match(const TDelta* previousDelta, const TDelta* latestDelta, const TEntity* previous, const TEntity* latest) const
		{
			if (!previous || !latest)
				return !previous && !latest;

			return previousDelta ? previousDelta == latestDelta : previous->GetName() == latest->GetName();
		}

		template<class TEntity, class TDelta = IDelta>
		bool match(const TDelta* latestDelta, const TEntity* previous, const TEntity* latest) const
		{
			return match(graph.Find<TDelta>(previous), latestDelta, previous, latest);
		}

		template<class TEntity, class TDelta = IDelta>
		bool match(const TEntity* previous, const TEntity* latest) const
		{
			return match(graph.Find<TDelta>(latest), previous, latest);
		}

	private:
		Task* definition = nullptr;
		Task* deletion = nullptr;
	};
}