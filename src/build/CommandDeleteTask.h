#pragma once
#include "CommandTask.h"

namespace JadeGit::Build
{
	class CommandDeleteTask : public CommandTask
	{
	public:
		const char* entityType;
		std::string qualifiedName;

		CommandDeleteTask(TaskGraph &graph, const char* entityType, std::string qualifiedName);

		void Execute(CommandStrategy &strategy) const override;
		void Print(std::ostream& output) const override;
	};
}