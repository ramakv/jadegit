#pragma once
#include "Task.h"
#include <build/Builder.h>

namespace JadeGit::Build
{
	class BuildTask : public Task
	{
	public:
		BuildTask(TaskGraph &graph) : Task(graph) {}

		bool Accept(TaskVisitor &v) const override final;

		virtual bool Execute(Builder& builder) const = 0;
	};
}