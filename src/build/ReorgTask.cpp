#include "ReorgTask.h"
#include "TaskVisitor.h"
#include <build/Builder.h>

namespace JadeGit::Build
{
	bool ReorgTask::Accept(TaskVisitor& v) const
	{
		return v.Visit(this);
	}

	void ReorgTask::Print(std::ostream& output) const 
	{
		output << "<reorg>"; 
	}

	bool ReorgTask::Execute(Builder& builder) const
	{
		builder.Reorg();
		return true;
	}
}