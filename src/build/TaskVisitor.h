#pragma once

namespace JadeGit::Build
{
	class BuildTask;
	class CommandTask;
	class ExtractTask;
	class ReorgTask;

	class TaskVisitor
	{
	public:
		virtual bool Visit(const BuildTask* task) = 0;
		virtual bool Visit(const CommandTask* task) = 0;
		virtual bool Visit(const ExtractTask* task) = 0;
		virtual bool Visit(const ReorgTask* task) = 0;
	};
}