#include "TaskWaypoint.h"
#include "TaskGraph.h"

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	TaskWaypoint* TaskWaypoint::make(TaskGraph& graph, const Entity* entity, Type type)
	{
		if (!entity)
			return nullptr;

		auto key = make_pair(entity, type);

		if (TaskWaypoint* task = graph.waypoints[key])
			return task;

		return graph.waypoints[key] = new TaskWaypoint(graph);
	}
}