#pragma once
#include "FeatureDelta.h"
#include <jadegit/data/Class.h>

namespace JadeGit::Build
{
	template<class TComponent>
	class MethodDelta : public FeatureDelta<TComponent>
	{
	public:
		MethodDelta(TaskGraph& graph) : FeatureDelta<TComponent>(graph, "Method") {}

		using FeatureDelta<TComponent>::previous;
		using FeatureDelta<TComponent>::latest;

	protected:
		using FeatureDelta<TComponent>::graph;
		using FeatureDelta<TComponent>::GetDefinition;
		using FeatureDelta<TComponent>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!FeatureDelta<TComponent>::AnalyzeEnter())
			{
				// Handle deletion before event method
				if (previous->controlMethod != previous)
					if (auto eventMethod = graph.Analyze<ISchemaComponentDelta>(previous->controlMethod))
						graph.AddEdge(GetDeletion(), eventMethod->GetDeletion());

				return false;
			}

			// Defining event method mappings depend on event methods being created/renamed
			if (latest->controlMethod != latest)
				if (auto eventMethod = graph.Analyze<ISchemaComponentDelta>(latest->controlMethod))
					graph.AddEdge(eventMethod->GetCreation(), TaskWaypoint::make(graph, static_cast<Type*>(latest->schemaType), TaskWaypoint::Definition));

			return true;
		}

		Task* HandleRename() final
		{
			/*
				Mapping methods are implicitly renamed when properties are renamed.
				May also apply when super-methods are renamed (not supported by JADE yet).
			*/

			Class* previousClass = dynamic_cast<Class*>(static_cast<Type*>(previous->schemaType));
			Class* latestClass = dynamic_cast<Class*>(static_cast<Type*>(latest->schemaType));

			if (previousClass && latestClass)
			{
				// Resolve properties with the same name
				const Property* previousProperty = previousClass->GetProperty(previous->GetName());
				const Property* latestProperty = latestClass->GetProperty(latest->GetName());

				if (previousProperty && latestProperty)
				{
					// Analyze property changes
					const ISchemaComponentDelta* previousPropertyDelta = graph.Analyze<ISchemaComponentDelta>(previousProperty);
					const ISchemaComponentDelta* latestPropertyDelta = graph.Analyze<ISchemaComponentDelta>(latestProperty);

					if (previousPropertyDelta && previousPropertyDelta == latestPropertyDelta)
					{
						// Method will be implicitly renamed when property is renamed
						return latestPropertyDelta->GetRename();
					}
				}
			}

			// Default to renaming method explicitly
			return FeatureDelta<TComponent>::HandleRename();
		}
	};
}