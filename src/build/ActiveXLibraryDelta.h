#pragma once
#include "ClassDelta.h"
#include <jadegit/data/ActiveXLibrary.h>

namespace JadeGit::Build
{
	class ActiveXLibraryDelta : public SchemaComponentDelta<ActiveXLibrary>
	{
	public:
		ActiveXLibraryDelta(TaskGraph& graph) : SchemaComponentDelta(graph, "ActiveXLibrary") {}

	protected:
		bool AnalyzeEnter() final
		{
			// Analyze base class
			baseClass = graph.Analyze<IClassDelta>(latest ? latest->getBaseClass() : previous->getBaseClass());

			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			return true;
		}

		Task* HandleDeletion() final
		{
			// Deletion implied by base class deletion
			if (baseClass)
				return baseClass->GetDeletion();

			return SchemaComponentDelta::HandleDeletion();
		}

		Task* HandleRename() final
		{
			// Rename implied by base class rename
			if (baseClass)
				return baseClass->GetRename();

			return SchemaComponentDelta::HandleRename();
		}

	private:
		IClassDelta* baseClass = nullptr;
	};

	template<class TComponent>
	class ActiveXComponentDelta : public Delta<TComponent>
	{
	public:
		ActiveXComponentDelta(TaskGraph& graph) : Delta<TComponent>(graph) {}

		using Delta<TComponent>::previous;
		using Delta<TComponent>::latest;

	protected:
		using Delta<TComponent>::graph;
		using Delta<TComponent>::GetDefinition;
		using Delta<TComponent>::GetDeletion;

		Task* HandleDefinition(bool complete) final
		{
			// Defined by parent
			const IDelta* parent = graph.Analyze<IDelta>(latest->getParentObject());
			return parent ? parent->GetDefinition() : nullptr;
		}

		Task* HandleDeletion() final
		{
			// Deleted implicitly by related/original entity
			if (const IDelta* original = graph.Analyze<IDelta>(&previous->getOriginal()))
			{
				if (auto deletion = original->GetDeletion())
					return deletion;
			}

			// Deleted implicitly by parent otherwise
			const IDelta* parent = graph.Analyze<IDelta>(previous->getParentObject());
			if (!parent)
				throw jadegit_exception("External component entity missing previous parent delta");

			if (auto deletion = parent->GetDeletion())
				return deletion;

			return parent->GetDefinition();
		}

		bool AnalyzeEnter() final
		{
			// Returns false when deleted
			if (!Delta<TComponent>::AnalyzeEnter())
				return false;

			// Definition depends on related/original entities being created/renamed
			if (const IDelta* original = graph.Analyze<IDelta>(&latest->getOriginal()))
				graph.AddEdge(original->GetCreation(), GetDefinition());

			return true;
		}
	};
}