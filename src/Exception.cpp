#include <jadegit/Exception.h>
#include <jadegit/Version.h>

using namespace std;

namespace JadeGit
{
	jadegit_operation_aborted::jadegit_operation_aborted() : jadegit_exception("Operation aborted by user") {}

	jadegit_unimplemented_feature::jadegit_unimplemented_feature() : jadegit_unimplemented_feature("Feature") {}
	jadegit_unimplemented_feature::jadegit_unimplemented_feature(const string& feature) : jadegit_exception(feature + " is not available as support for this has not been implemented in this release") {}
	
	jadegit_unsupported_feature::jadegit_unsupported_feature() : jadegit_unsupported_feature("Feature") {}
	jadegit_unsupported_feature::jadegit_unsupported_feature(const string& feature) : jadegit_exception(feature + " is not supported") {}
	jadegit_unsupported_feature::jadegit_unsupported_feature(const string& feature, const AnnotatedVersion& requiredVersion) : jadegit_exception(format("{} is not supported (requires {})", feature, static_cast<string>(requiredVersion))) {}
}