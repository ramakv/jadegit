# jadegit
__Source control and deployment tools for JADE software using libgit2__

The goal of this project is to implement source control and deployment capabilities for [JADE](https://www.jadeworld.com/jade-platform) applications, using Git repositories (using the [libgit2](https://libgit2.org/) library).  For development environments, this is implemented on the premise of using JADE as the ODB backend to libgit2, so both systems are kept in sync atomically.

The current focus is to implement the core functionality needed to plug-in source control to the JADE IDE.  In addition to supporting common source control functions, the long term goals of this project include the following aspirations:

- Multi-developer & multi-branch support.
- Multi-repository support, allowing repositories to import/use schemas from others.
- jadegit command line interface.
- Deployment build server that's backed by a Git repository, independently of a JADE environment.
- Self-service capabilities for users/testers to select/download/install changes into local environments.
- JadeGitPackage which provides bindings to the libgit2 library for JADE applications to embed version control for application data, with the option of reusing entity framework defined for source code, in order to gain deployment capabilities integrated with code changes (useful for complex configuration data changes that must to be released with code changes).

## License
jadegit is licensed under the [GNU Lesser General Public License v3.0+](COPYING.LESSER). This allows you to link and use the jadegit libraries from any program, proprietary or open source; paid or gratis; without any obligation to open source your components/software.  However, any derived works based on, or modifications to, the jadegit libraries must be shared in keeping with the conditions of the standard [GPL](LICENSE.txt).

The intent of using this license is to fostor collaboration for the benefit of the JADE community, while also recognizing that most of us work in environments developing proprietary software.  To use the standard GPL would prevent most of us from linking/using jadegit functionality, which would prevent us reusing jadegit functionality from our proprietary applications (like JadeGitPackage referred to above in particular).

## Documentation
jadegit is [documented](https://jadelab.gitbook.io/jadegit/) using gitbook.  Check out the [Getting Started](https://jadelab.gitbook.io/jadegit/developers/getting-started) guide for initial installation & setup.

## Discussion
If something isn't working as you'd expect, or there's a feature you'd like to see supported for which you can't find an existing issue, please [raise an issue](https://gitlab.com/jadelab/jadegit/jadegit/issues/new).

[Join](https://join.slack.com/t/jadelabnz/shared_invite/zt-7ruxd3ad-bXvNo88SBWlzZqsicbHIkA) the group [slack workspace](https://jadelabnz.slack.com) to ask questions, discuss and see development progress in the #jadegit channel.