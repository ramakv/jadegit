#pragma once
#include "Constant.h"
#include "RootSchema/GlobalConstantMeta.h"

namespace JadeGit::Data
{
	class ConstantCategory;
	class Schema;

	class GlobalConstant : public Constant
	{
	public:
		GlobalConstant(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Schema* const, &GlobalConstantMeta::schema> schema;
		ObjectValue<ConstantCategory*, &GlobalConstantMeta::category> category;

		void Accept(EntityVisitor &v) override;
	};

	extern template ObjectValue<Schema* const, &GlobalConstantMeta::schema>;
	extern template ObjectValue<ConstantCategory*, &GlobalConstantMeta::category>;
}