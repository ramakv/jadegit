#pragma once
#include "Schema.h"

namespace JadeGit
{
	class Version;
}

namespace JadeGit::Data
{
	class Config;
	class ObjectFileStorage;

	class Assembly : public Component
	{
	public:
		Assembly(const FileSystem& fs);
		virtual ~Assembly();

		MemberKeyDictionary<Schema, &Schema::name> schemas;

		const Config& getConfig() const;
		const RootSchema& GetRootSchema() const { return *rootSchema; }
		const ObjectFileStorage& getStorage() const;

		// Saves any pending changes
		void save(bool unload = false);

		// Load entity from repository
		Entity* Load(std::filesystem::path path, bool shallow);

	protected:
		friend Schema;
		const RootSchema* rootSchema = nullptr;
		std::vector<std::unique_ptr<MetaSchema>> metaSchemas;

		Assembly(const FileSystem& fs, const Version& platformVersion);

		virtual void unload();

	private:
		std::unique_ptr<Config> config;
		std::unique_ptr<ObjectFileStorage> storage;
	};
}