#pragma once
#include "Entity.h"
#include "Enum.h"

namespace JadeGit::Data
{
	class SchemaEntity : public Entity
	{
	public:
		SchemaEntity(Object* parent, const Class* dataClass, const char* name);

		enum Access : char
		{
			Public = 0,
			ReadOnly = 1,
			Protected = 2,
			Hidden = 3,

			// Locale specific
			Locale_Local = 'L',
			Locale_Inherited = 'I'
		};

		Value<std::string> text;
		Value<bool> abstract = false;
		Value<Access> access = Public;
		Value<Access> subAccess = Public;
		Value<std::string> wsdlName;
	};

	extern template Value<SchemaEntity::Access>;
	extern template std::map<SchemaEntity::Access, const char*> EnumStrings<SchemaEntity::Access>::data;
}