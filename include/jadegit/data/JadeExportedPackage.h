#pragma once
#include "SchemaEntity.h"
#include "JadeExportedClass.h"
#include "JadeExportedInterface.h"
#include "RootSchema/JadeExportedPackageMeta.h"

namespace JadeGit::Data
{
	class Schema;

	DECLARE_OBJECT_CAST(Schema)

	class JadePackage : public MajorEntity<JadePackage, SchemaEntity>
	{
	public:
		static const std::filesystem::path subFolder;

		JadePackage(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Schema*, &JadePackageMeta::schema> schema;
	};
	
	extern template ObjectValue<Schema*, &JadePackageMeta::schema>;

	class Application;
	class JadeImportedPackage;

	DECLARE_OBJECT_CAST(Application)
	DECLARE_OBJECT_CAST(JadeImportedPackage)

	class JadeExportedPackage : public JadePackage
	{
	public:
		JadeExportedPackage(Schema* parent, const Class* dataClass, const char* name);

		ObjectValue<Application*, &JadeExportedPackageMeta::application> application;
		EntityDict<JadeExportedClass, &JadeExportedPackageMeta::classes> classes;
		EntityDict<JadeExportedInterface, &JadeExportedPackageMeta::interfaces> interfaces;
		ObjectValue<Set<JadeImportedPackage*>, &JadeExportedPackageMeta::importedPackageRefs> importedPackageRefs;

		void Accept(EntityVisitor& v) override;

		std::unique_ptr<QualifiedName> GetOriginalEntityName() const;
		JadeExportedType* getType(const std::string& name);
	};

	extern template ObjectValue<Application*, &JadeExportedPackageMeta::application>;
	extern template EntityDict<JadeExportedClass, &JadeExportedPackageMeta::classes>;
	extern template EntityDict<JadeExportedInterface, &JadeExportedPackageMeta::interfaces>;
	extern template ObjectValue<Set<JadeImportedPackage*>, &JadeExportedPackageMeta::importedPackageRefs>;
}