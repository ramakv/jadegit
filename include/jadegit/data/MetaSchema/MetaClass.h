#pragma once
#include "MetaType.h"

namespace JadeGit::Data
{
	class Type;
	class Class;
	class CollClass;
	class GUIClass;
	class CompAttribute;
	class PrimAttribute;
	class ExplicitInverseRef;
	class ImplicitInverseRef;

	template<class TType>
	class MetaClass : public MetaType<TType>
	{
	public:
		MetaClass(MetaSchema& parent, const char* name, Class* superclass);

	protected:
		using MetaType<TType>::GetRootSchema;
		using MetaType<TType>::versionCheck;

		template <class TProperty, typename... Args>
		TProperty* NewProperty(const char* name, Type* type, Args... args)
		{
			return new TProperty(this->subject, nullptr, name, type, args...);
		}

		template <class TProperty, typename... Args>
		TProperty* NewProperty(const Version& minVersion, const char* name, Type* type, Args... args)
		{
			return versionCheck(minVersion) ? NewProperty<TProperty>(name, type, args...) : nullptr;
		}

		template <class TReference>
		TReference* NewReference(const char* name, Type* type)
		{
			return NewProperty<TReference>(name, type);
		}

		template <class TReference>
		TReference* NewReference(const Version& minVersion, const char* name, Type* type)
		{
			return NewProperty<TReference>(minVersion, name, type);
		}

		// Primitives
		PrimAttribute* NewBoolean(const char* name);
		PrimAttribute* NewBoolean(const Version& minVersion, const char* name);
		PrimAttribute* NewBinary(const char* name, int length = -1);
		PrimAttribute* NewBinary(const Version& minVersion, const char* name, int length = -1);
		PrimAttribute* NewByte(const char* name);
		PrimAttribute* NewByte(const Version& minVersion, const char* name);
		PrimAttribute* NewCharacter(const char* name);
		PrimAttribute* NewCharacter(const Version& minVersion, const char* name);
		PrimAttribute* NewInteger(const char* name);
		PrimAttribute* NewInteger(const Version& minVersion, const char* name);
		PrimAttribute* NewReal(const char* name);
		PrimAttribute* NewReal(const Version& minVersion, const char* name);
		PrimAttribute* NewString(const char* name, int length = -1);
		PrimAttribute* NewString(const Version& minVersion, const char* name, int length = -1);
		PrimAttribute* NewStringUtf8(const char* name, int length = -1);
		PrimAttribute* NewStringUtf8(const Version& minVersion, const char* name, int length = -1);
		PrimAttribute* NewTimeStamp(const char* name);
		PrimAttribute* NewTimeStamp(const Version& minVersion, const char* name);
	};
}