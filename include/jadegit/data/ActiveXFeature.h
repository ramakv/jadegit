#pragma once
#include "Feature.h"
#include "RootSchema/ActiveXFeatureMeta.h"

namespace JadeGit::Data
{
	class ActiveXClass;

	class ActiveXFeature : public Entity
	{
	public:
		ActiveXFeature(ActiveXClass& parent, const Class* dataClass, const char* name);

		ObjectValue<Feature*, &ActiveXFeatureMeta::feature> feature;

		const Feature& getOriginal() const final;

	protected:
		const ActiveXClass& getParent() const;
	};

	extern template ObjectValue<Feature*, &ActiveXFeatureMeta::feature>;
}