#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ApplicationMeta : public RootClass<>
	{
	public:
		static const ApplicationMeta& get(const Object& object);
		
		ApplicationMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const aboutFormName;
		PrimAttribute* const defaultLocaleId;
		PrimAttribute* const internetPipeName;
		PrimAttribute* const numberOfPipes;
		PrimAttribute* const startupFormName;
		PrimAttribute* const webAppDirectory;
		PrimAttribute* const webBaseURL;
		PrimAttribute* const webDefaultTimeZone;
		PrimAttribute* const webDisplayMessages;
		PrimAttribute* const webDisplayPreference;
		ImplicitInverseRef* const webEventClasses;
		PrimAttribute* const webHomePage;
		PrimAttribute* const webMachineName;
		PrimAttribute* const webMaxHTMLSize;
		PrimAttribute* const webServiceClasses;
		PrimAttribute* const webSessionTimeout;
		PrimAttribute* const webShowModal;
		PrimAttribute* const webStatusLineDisplay;
		PrimAttribute* const webUseHTML32;
		PrimAttribute* const webVirtualDirectory;
		PrimAttribute* const appVersion;
		PrimAttribute* const applicationType;
		PrimAttribute* const controlSpacing;
		PrimAttribute* const defaultApp;
		PrimAttribute* const defaultMdi;
		ExplicitInverseRef* const exportedPackages;
		ImplicitInverseRef* const finalizeMethod;
		PrimAttribute* const fontBold;
		PrimAttribute* const fontName;
		PrimAttribute* const fontSize;
		PrimAttribute* const formMargin;
		PrimAttribute* const heightSingleLineControl;
		PrimAttribute* const helpFile;
		PrimAttribute* const icon;
		ImplicitInverseRef* const initializeMethod;
		PrimAttribute* const mdiStyle;
		PrimAttribute* const mdiWindowListOrder;
		PrimAttribute* const name;
		ExplicitInverseRef* const schema;
		ImplicitInverseRef* const threeDControls;
		PrimAttribute* const useBorderStyleOnly;
		PrimAttribute* const webMinimumResponseTime;
	};
};
