#pragma once
#include "JadeExportedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeExportedMethodMeta : public RootClass<>
	{
	public:
		static const JadeExportedMethodMeta& get(const Object& object);
		
		JadeExportedMethodMeta(RootSchema& parent, const JadeExportedFeatureMeta& superclass);
	
		ExplicitInverseRef* const importedMethodRefs;
		ExplicitInverseRef* const method;
	};
};
