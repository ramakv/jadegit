#pragma once
#include "RoutineMeta.h"

namespace JadeGit::Data
{
	class LockPolicyMeta : public RootClass<>
	{
	public:
		static const LockPolicyMeta& get(const Object& object);
		
		LockPolicyMeta(RootSchema& parent, const RoutineMeta& superclass);
	};
};
