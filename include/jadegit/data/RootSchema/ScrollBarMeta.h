#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class ScrollBarMeta : public RootClass<GUIClass>
	{
	public:
		static const ScrollBarMeta& get(const Object& object);
		
		ScrollBarMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const largeChange;
		PrimAttribute* const max_;
		PrimAttribute* const min_;
		PrimAttribute* const smallChange;
		PrimAttribute* const value;
	};
};
