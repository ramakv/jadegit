#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class ExternalClassMeta : public RootClass<>
	{
	public:
		ExternalClassMeta(RootSchema& parent, const ClassMeta& superclass);
	};
};
