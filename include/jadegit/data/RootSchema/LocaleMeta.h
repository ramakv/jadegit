#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class LocaleMeta : public RootClass<>
	{
	public:
		static const LocaleMeta& get(const Object& object);
		
		LocaleMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const cloneOf;
		ExplicitInverseRef* const forms;
		ExplicitInverseRef* const schema;
		ExplicitInverseRef* const translatableStrings;
	};
};
