#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class JadeHTMLDocumentMeta : public RootClass<>
	{
	public:
		static const JadeHTMLDocumentMeta& get(const Object& object);
		
		JadeHTMLDocumentMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const addCookieCode;
		PrimAttribute* const autoComplete;
		PrimAttribute* const bounceSeconds;
		PrimAttribute* const bounceURL;
		PrimAttribute* const description;
		PrimAttribute* const encodingType;
		PrimAttribute* const fileCreationTime;
		PrimAttribute* const fileModifiedTime;
		PrimAttribute* const fileName;
		PrimAttribute* const homePage;
		ExplicitInverseRef* const htmlClass;
		PrimAttribute* const htmlString;
		PrimAttribute* const htmlStringModifiedTime;
		PrimAttribute* const includeSessionData;
		ImplicitInverseRef* const jadeClass;
		PrimAttribute* const name;
		ExplicitInverseRef* const schema;
		PrimAttribute* const securePage;
		PrimAttribute* const targetWindow;
		PrimAttribute* const uuid;
	};
};
