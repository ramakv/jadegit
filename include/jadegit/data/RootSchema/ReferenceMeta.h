#pragma once
#include "PropertyMeta.h"

namespace JadeGit::Data
{
	class ReferenceMeta : public RootClass<>
	{
	public:
		static const ReferenceMeta& get(const Object& object);
		
		ReferenceMeta(RootSchema& parent, const PropertyMeta& superclass);
	
		ExplicitInverseRef* const constraint;
	};
};
