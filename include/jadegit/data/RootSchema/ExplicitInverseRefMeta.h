#pragma once
#include "ReferenceMeta.h"

namespace JadeGit::Data
{
	class ExplicitInverseRefMeta : public RootClass<>
	{
	public:
		static const ExplicitInverseRefMeta& get(const Object& object);
		
		ExplicitInverseRefMeta(RootSchema& parent, const ReferenceMeta& superclass);
	
		PrimAttribute* const transientToPersistentAllowed;
		PrimAttribute* const inverseNotRequired;
		PrimAttribute* const kind;
		PrimAttribute* const updateMode;
	};
};
