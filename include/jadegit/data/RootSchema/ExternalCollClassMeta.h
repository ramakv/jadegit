#pragma once
#include "CollClassMeta.h"

namespace JadeGit::Data
{
	class ExternalCollClassMeta : public RootClass<>
	{
	public:
		ExternalCollClassMeta(RootSchema& parent, const CollClassMeta& superclass);
	};
};
