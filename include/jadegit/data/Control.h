#pragma once
#include "Window.h"
#include "ObjectValue.h"
#include "RootSchema/ControlMeta.h"

namespace JadeGit::Data
{
	class Form;

	class Control : public Window
	{
	public:
		Control(Form* parent, const Class* dataClass, const char* name);

		ObjectValue<Form* const, &ControlMeta::form> form;

		void Accept(EntityVisitor &v) override;	
	};

	extern template ObjectValue<Form* const, &ControlMeta::form>;
}