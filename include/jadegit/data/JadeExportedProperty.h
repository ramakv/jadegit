#pragma once
#include "JadeExportedFeature.h"
#include "Property.h"
#include "RootSchema/JadeExportedPropertyMeta.h"

namespace JadeGit::Data
{
	class JadeImportedProperty;

	DECLARE_OBJECT_CAST(JadeImportedProperty)

	class JadeExportedProperty : public JadeExportedFeature
	{
	public:
		JadeExportedProperty(JadeExportedType* parent, const Class* dataClass, const char* name);

		ObjectValue<Set<JadeImportedProperty*>, &JadeExportedPropertyMeta::importedPropertyRefs> importedPropertyRefs;
		ObjectValue<Property*, &JadeExportedPropertyMeta::property> property;

		void Accept(EntityVisitor& v) override;

		const Property& getOriginal() const override;

	protected:
		void loaded(std::queue<std::future<void>>& tasks) final;
	};

	extern template ObjectValue<Set<JadeImportedProperty*>, &JadeExportedPropertyMeta::importedPropertyRefs>;
	extern template ObjectValue<Property*, &JadeExportedPropertyMeta::property>;
}