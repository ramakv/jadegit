#pragma once
#include "Feature.h"
#include "Set.h"
#include "RootSchema/PropertyMeta.h"
#include <future>
#include <queue>

namespace JadeGit::Data
{
	template<class T, typename U>
	std::ptrdiff_t member_offset(U T::* member)
	{
		return std::ptrdiff_t(std::addressof(reinterpret_cast<T const volatile*>(nullptr)->*member));
	}

	template <typename>
	struct is_object_value : public std::false_type {};

	template <typename T, auto prop>
	struct is_object_value<ObjectValue<T, prop>> : public std::true_type {};

	// Base mapping for a property to any explicit AnyValue pointer to member
	class AnyPropertyValue
	{
	public:
		virtual AnyValue& value(Object &object) const = 0;
		virtual const AnyValue& value(const Object &object) const = 0;
		virtual ~AnyPropertyValue() {};
	};

	// Mapping for a property to specific type of AnyValue pointer to member
	template <typename TValue>
	class PropertyValue : public AnyPropertyValue
	{
	public:
		PropertyValue(TValue Object::* member) : member(member) {}

		AnyValue& value(Object &object) const override
		{
			return object.*member;
		}

		const AnyValue& value(const Object &object) const override
		{
			return object.*member;
		}

	private:
		TValue Object::* member;
	};

	class JadeImportedPackage;
	class Schema;

	class Property : public Feature
	{
	public:
		Property(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);

		Value<bool> embedded = false;
		Value<bool> isHTMLProperty = false;
		Value<bool> required = false;
		Value<bool> virtual_ = false;
		Value<bool> webService = false;
		Value<bool> xmlAttribute = false;
		Value<bool> xmlNillable = false;

		AnyValue& getActualValue(const Object& object) const;
		AnyValue* getActualValue(const Object& object, bool instantiate) const;

		// Returns default value, creating if required
		const AnyValue* GetDefaultValue() const;

		virtual int GetLength() const;

		using Feature::GetValue;
		Any GetValue(const Object &object, bool instantiate = false) const;
		
		using Feature::resetValue;
		void resetValue(Object& object) const;

		template<typename V>
		void SetValue(Object &object, V v) const
		{
			AnyValue* value = member ? &(member->value(object)) : object.values[this];
			if (!value)
				object.values[this] = value = InstantiateValue();

			value->Set(v);

			// Flag object as being modified
			object.Modified();
		}

		// Bind property to TValue AnyValue pointer to TObject member
		template <typename TObject, typename TValue>
		inline Property& bind(TValue TObject::* ptr)
		{
			member = std::make_unique<PropertyValue<TValue>>(static_cast<TValue Object::*>(ptr));

			if constexpr (is_object_value<TValue>::value)
			{
				auto offset = member_offset(ptr);
				assert(TValue::offset == 0 || TValue::offset == offset);
				TValue::offset = offset;
			}

			return *this;
		}

		inline Property* SetAlias(const char* alias)
		{
			if (this)
				this->alias = alias;
			return this;
		}

		inline const char* GetAlias() const
		{
			return alias ? alias : name.c_str();
		}

		inline bool isStructural() const
		{
			return structural_;
		}

		inline Property& structural()
		{
			this->structural_ = true;
			return *this;
		}

		inline Property& unwritten()
		{
			this->unwritten_ = true;
			return *this;
		}

	protected:
		const char* alias = nullptr;
		std::unique_ptr<AnyValue> default_;
		bool structural_ = false;
		bool unwritten_ = false;

		AnyValue* InstantiateValue() const;
		virtual AnyValue* InstantiateValue(Object& object) const = 0;

		friend JadeImportedPackage;
		friend Object;
		friend Schema;
		friend class ObjectXMLFormat;
		void LoadFor(Object &object, const tinyxml2::XMLElement* source, bool initial, bool shallow, std::queue<std::future<void>>& tasks) const;
		void WriteFor(const Object &object, tinyxml2::XMLNode& parent) const;
		virtual bool WriteFilter() const { return !unwritten_; };

		// Group properties after constants
		int GetBracket() const override { return 2; }

	private:
		std::unique_ptr<AnyPropertyValue> member;
	};
}