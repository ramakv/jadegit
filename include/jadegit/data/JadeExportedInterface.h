#pragma once
#include "JadeExportedType.h"
#include "JadeInterface.h"
#include "RootSchema/JadeExportedInterfaceMeta.h"

namespace JadeGit::Data
{
	class JadeImportedInterface;

	DECLARE_OBJECT_CAST(JadeImportedInterface)

	class JadeExportedInterface : public JadeExportedType
	{
	public:
		JadeExportedInterface(JadeExportedPackage* parent, const Class* dataClass, const char* name);

		ObjectValue<Set<JadeImportedInterface*>, &JadeExportedInterfaceMeta::importedInterfaceRefs> importedInterfaceRefs;
		ObjectValue<JadeInterface*, &JadeExportedInterfaceMeta::originalInterface> originalInterface;

		void Accept(EntityVisitor& v) override;

		const JadeInterface& getOriginal() const override;

	protected:
		void loaded(std::queue<std::future<void>>& tasks) final;
	};

	extern template ObjectValue<Set<JadeImportedInterface*>, &JadeExportedInterfaceMeta::importedInterfaceRefs>;
	extern template ObjectValue<JadeInterface*, &JadeExportedInterfaceMeta::originalInterface>;
}