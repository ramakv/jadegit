#pragma once
#include "Entity.h"

namespace JadeGit::Data
{
	class Window : public Entity
	{
	public:
		Window(Object* parent, const Class* dataClass, const char* name);
	};
}