#pragma once
#include "Script.h"
#include "Array.h"
#include "Set.h"
#include "RootSchema/ConstantMeta.h"

namespace JadeGit::Data
{
	class Constant : public Script
	{
	public:
		Constant(Type* parent, const Class* dataClass, const char* name);

		ObjectValue<Set<Constant*>, &ConstantMeta::constantRefs> constantRefs;
		ObjectValue<Array<Constant*>, &ConstantMeta::constantUsages> constantUsages;

		void Accept(EntityVisitor &v) override;

	protected:
		Constant(Object* parent, const Class* dataClass, const char* name);

		/* Group constants after class mappings, or in the case of global constants, categories */
		int GetBracket() const override { return 1; }
	};

	extern template ObjectValue<Set<Constant*>, &ConstantMeta::constantRefs>;
	extern template ObjectValue<Array<Constant*>, &ConstantMeta::constantUsages>;
}