#pragma once
#include "Property.h"
#include "RootSchema/JadeDynamicCompAttributeMeta.h"
#include "RootSchema/JadeDynamicPrimAttributeMeta.h"

namespace JadeGit::Data
{
	class JadeDynamicPropertyCluster;

	DECLARE_OBJECT_CAST(JadeDynamicPropertyCluster)

	class Attribute : public Property
	{
	public:
		Attribute(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);

		Value<int> length = 0;

		int GetLength() const override;
	};

	class CompAttribute : public Attribute
	{
	public:
		CompAttribute(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr);

		void Accept(EntityVisitor &v) override;

	protected:
		AnyValue* InstantiateValue(Object& object) const final;
	};

	class JadeDynamicCompAttribute : public CompAttribute
	{
	public:
		JadeDynamicCompAttribute(Class* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeDynamicPropertyCluster*, &JadeDynamicCompAttributeMeta::dynamicPropertyCluster> dynamicPropertyCluster;
	};

	class PrimAttribute : public Attribute
	{
	public:
		Value<int> precision = 0;
		Value<int> scaleFactor = 0;
		Value<std::string> xmlType;

		PrimAttribute(Class* parent, const Class* dataClass, const char* name, Type* type = nullptr, int length = 0);

		void Accept(EntityVisitor &v) override;

		// Set default value
		inline Property* SetDefault(const AnyValue& v)
		{
			default_.reset(v.Clone());
			return this;
		}

	protected:
		AnyValue* InstantiateValue(Object& object) const final;
	};

	class JadeDynamicPrimAttribute : public PrimAttribute
	{
	public:
		JadeDynamicPrimAttribute(Class* parent, const Class* dataClass, const char* name);

		ObjectValue<JadeDynamicPropertyCluster*, &JadeDynamicPrimAttributeMeta::dynamicPropertyCluster> dynamicPropertyCluster;
	};
}