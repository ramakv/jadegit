#pragma once
#include "Value.h"
#include <chrono>
#include <optional>

namespace JadeGit::Data
{
	using namespace std;
	using namespace std::chrono;

	class Date : public optional<year_month_day>
	{
	public:
		using optional<year_month_day>::optional;
		constexpr Date() noexcept : optional<year_month_day>(day(24) / November / year(-4713)) {}
	};

	class Time : public optional<hh_mm_ss<milliseconds>>
	{
	public:
		using optional<hh_mm_ss<milliseconds>>::optional;
		constexpr Time() noexcept : optional<hh_mm_ss<milliseconds>>(milliseconds()) {}
	};

	class TimeStamp : public optional<sys_time<Time::value_type::precision>>
	{
	public:
		using optional<sys_time<Time::value_type::precision>>::optional;
		constexpr TimeStamp() noexcept : TimeStamp(Date(), Time()) {}
		constexpr TimeStamp(Date date, Time time) noexcept
		{
			if (date && time)
				emplace(sys_days(date.value()) + time.value().to_duration());
		}
	};

	constexpr bool operator== (const Date& lhs, const Date& rhs) noexcept
	{
		return static_cast<optional<Date::value_type>>(lhs) == rhs;
	}

	constexpr bool operator== (const Time& lhs, const Time& rhs) noexcept
	{
		if (lhs.has_value() && rhs.has_value())
			return lhs.value().to_duration() == rhs.value().to_duration();

		return !lhs.has_value() && !rhs.has_value();
	}

	constexpr bool operator== (const TimeStamp& lhs, const TimeStamp& rhs) noexcept
	{
		return static_cast<optional<TimeStamp::value_type>>(lhs) == rhs;
	}
	
	template<class CharT, class Traits>
	basic_istream<CharT, Traits>& operator>> (basic_istream<CharT, Traits>& stream, Date::value_type& date)
	{
		bool negative = false;
		if (stream.get() == '-')
			negative = true;
		else
			stream.unget();

		if (!(stream >> parse("%F", date)))
			throw jadegit_exception("Failed to parse date");

		if (negative)
			date = date.day() / date.month() / (static_cast<int>(date.year()) * -1);

		return stream;
	}

	template<class CharT, class Traits>
	basic_istream<CharT, Traits>& operator>> (basic_istream<CharT, Traits>& stream, Time::value_type& time)
	{
		milliseconds value;
		if (!(stream >> parse("%T", value)))
			throw jadegit_exception("Failed to parse time");

		time = Time::value_type(value);
		return stream;
	}

	template<class CharT, class Traits>
	basic_istream<CharT, Traits>& operator>> (basic_istream<CharT, Traits>& stream, TimeStamp::value_type& ts)
	{
		if (!(stream >> parse("%F %T", ts)))
			throw jadegit_exception("Failed to parse timestamp");

		return stream;
	}

	template<class CharT, class Traits>
	basic_ostream<CharT, Traits>& operator<< (basic_ostream<CharT, Traits>& stream, const TimeStamp::value_type& ts)
	{
		stream << ts;
		return stream;
	}
}