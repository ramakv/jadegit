#pragma once
#include "JadeExportedFeature.h"
#include "Method.h"
#include "RootSchema/JadeExportedMethodMeta.h"

namespace JadeGit::Data
{
	class JadeImportedMethod;

	DECLARE_OBJECT_CAST(JadeImportedMethod)

	class JadeExportedMethod : public JadeExportedFeature
	{
	public:
		JadeExportedMethod(JadeExportedType* parent, const Class* dataClass, const char* name);

		ObjectValue<Set<JadeImportedMethod*>, &JadeExportedMethodMeta::importedMethodRefs> importedMethodRefs;
		ObjectValue<Method*, &JadeExportedMethodMeta::method> method;

		void Accept(EntityVisitor& v) override;

		const Method& getOriginal() const override;

	protected:
		void loaded(std::queue<std::future<void>>& tasks) final;
	};

	extern template ObjectValue<Set<JadeImportedMethod*>, &JadeExportedMethodMeta::importedMethodRefs>;
	extern template ObjectValue<Method*, &JadeExportedMethodMeta::method>;
}