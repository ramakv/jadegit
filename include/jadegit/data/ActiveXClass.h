#pragma once
#include "ActiveXAttribute.h"
#include "ActiveXConstant.h"
#include "ActiveXMethod.h"
#include "Class.h"
#include "RootSchema/ActiveXClassMeta.h"

namespace JadeGit::Data
{
	class ActiveXLibrary;

	class ActiveXClass : public Entity
	{
	public:
		ActiveXClass(ActiveXLibrary& parent, const Class* dataClass, const char* name);

		ObjectValue<ActiveXLibrary* const, &ActiveXClassMeta::activeXLibrary> activeXLibrary;
		ObjectValue<Class*, &ActiveXClassMeta::baseClass> baseClass;

		void Accept(EntityVisitor& v) final;

		ActiveXAttribute* getAttribute(const std::string& name) const;
		ActiveXConstant* getConstant(const std::string& name) const;
		ActiveXMethod* getMethod(const std::string& name) const;

		const Class& getOriginal() const final;

	protected:
		void loaded(std::queue<std::future<void>>& tasks) final;
	};

	extern template ObjectValue<ActiveXLibrary* const, &ActiveXClassMeta::activeXLibrary>;
	extern template ObjectValue<Class*, &ActiveXClassMeta::baseClass>;
}