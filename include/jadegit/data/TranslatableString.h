#pragma once
#include "Constant.h"
#include "RootSchema/TranslatableStringMeta.h"

namespace JadeGit::Data
{
	class Locale;

	class TranslatableString : public Constant
	{
	public:
		TranslatableString(Locale* parent, const Class* dataClass, const char* name);

		ObjectValue<Locale* const, &TranslatableStringMeta::locale> locale;

		void Accept(EntityVisitor &v) override;

		const Entity* GetQualifiedParent() const override;
	};

	extern template ObjectValue<Locale* const, &TranslatableStringMeta::locale>;
}