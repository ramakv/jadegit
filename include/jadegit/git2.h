#pragma once
#include <jadegit/Exception.h>
#include <git2.h>
#include <memory>

/* std specializations */
namespace std
{
	template<>
	struct default_delete<git_annotated_commit> {
		void operator()(git_annotated_commit* ptr) { git_annotated_commit_free(ptr); }
	};

	template<>
	struct default_delete<git_blame> {
		void operator()(git_blame* ptr) { git_blame_free(ptr); }
	};

	template<>
	struct default_delete<git_blob> {
		void operator()(git_blob* ptr) { git_blob_free(ptr); }
	};

	template<>
	struct default_delete<git_branch_iterator> {
		void operator()(git_branch_iterator* ptr) { git_branch_iterator_free(ptr); }
	};

	template<>
	struct default_delete<git_buf> {
		void operator()(git_buf* ptr) { git_buf_dispose(ptr); delete ptr; }
	};

	template<>
	struct default_delete<git_commit> {
		void operator()(git_commit* ptr) { git_commit_free(ptr); }
	};

	template<>
	struct default_delete<git_config> {
		void operator()(git_config* ptr) { git_config_free(ptr); }
	};

	template<>
	struct default_delete<git_config_entry> {
		void operator()(git_config_entry* ptr) { git_config_entry_free(ptr); }
	};

	template<>
	struct default_delete<git_config_iterator> {
		void operator()(git_config_iterator* ptr) { git_config_iterator_free(ptr); }
	};

	template<>
	struct default_delete<git_diff> {
		void operator()(git_diff* ptr) { git_diff_free(ptr); }
	};

	template<>
	struct default_delete<git_filter_list> {
		void operator()(git_filter_list* ptr) { git_filter_list_free(ptr); }
	};

	template<>
	struct default_delete<git_index> {
		void operator()(git_index* ptr) { git_index_free(ptr); }
	};

	template<>
	struct default_delete<git_indexer> {
		void operator()(git_indexer* ptr) { git_indexer_free(ptr); }
	};

	template<>
	struct default_delete<git_index_iterator> {
		void operator()(git_index_iterator* ptr) { git_index_iterator_free(ptr); }
	};

	template<>
	struct default_delete<git_index_conflict_iterator> {
		void operator()(git_index_conflict_iterator* ptr) { git_index_conflict_iterator_free(ptr); }
	};

	template<>
	struct default_delete<git_merge_file_result> {
		void operator()(git_merge_file_result* ptr) { git_merge_file_result_free(ptr); delete ptr; }
	};

	template<>
	struct default_delete<git_object> {
		void operator()(git_object* ptr) { git_object_free(ptr); }
	};

	template<>
	struct default_delete<git_odb> {
		void operator()(git_odb* ptr) { git_odb_free(ptr); }
	};

	template<>
	struct default_delete<git_odb_object> {
		void operator()(git_odb_object* ptr) { git_odb_object_free(ptr); }
	};

	template<>
	struct default_delete<git_refdb> {
		void operator()(git_refdb* ptr) { git_refdb_free(ptr); }
	};

	template<>
	struct default_delete<git_reference> {
		void operator()(git_reference* ptr) { git_reference_free(ptr); }
	};

	template<>
	struct default_delete<git_reference_iterator> {
		void operator()(git_reference_iterator* ptr) { git_reference_iterator_free(ptr); }
	};

	template<>
	struct default_delete<git_remote> {
		void operator()(git_remote* ptr) { git_remote_free(ptr); }
	};

	template<>
	struct default_delete<git_repository> {
		void operator()(git_repository* ptr) { git_repository_free(ptr); }
	};

	template<>
	struct default_delete<git_revwalk> {
		void operator()(git_revwalk* ptr) { git_revwalk_free(ptr); }
	};

	template<>
	struct default_delete<git_signature> {
		void operator()(git_signature* ptr) { git_signature_free(ptr); }
	};

	template<>
	struct default_delete<git_strarray> {
		void operator()(git_strarray* ptr) { git_strarray_dispose(ptr); delete ptr; }
	};

	template<>
	struct default_delete<git_tree> {
		void operator()(git_tree* ptr) { git_tree_free(ptr); }
	};

	template<>
	struct default_delete<git_tree_entry> {
		void operator()(git_tree_entry* ptr) { git_tree_entry_free(ptr); }
	};

	template<>
	struct less<git_oid>
	{
		bool operator()(const git_oid& lhs, const git_oid& rhs) const
		{
			return git_oid_cmp(&lhs, &rhs) < 0;
		}
	};
}

namespace JadeGit
{
	// Smart ptr wrapper for git objects being constructed with out parameter
	template<typename T>
	class git_ptr
	{
	public:
		git_ptr(T& u) : u(u), p() {}

		operator typename T::element_type**() { return &p; }

		~git_ptr()
		{
			if constexpr (std::is_same_v<T, std::shared_ptr<typename T::element_type>>)
				u.reset(p, std::default_delete<typename T::element_type>());
			else
				u.reset(p);
		}

	private:
		T &u;
		typename T::element_type* p;
	};

	/* Runtime Exception */
	class git_exception : public jadegit_exception
	{
	public:
		git_exception(int code, const git_error* error) : jadegit_exception(code, error ? error->message : "Missing error message") { git_error_clear(); }
	};

	inline int git_throw(int result)
	{
		if (result < 0 && result != GIT_EUSER) throw git_exception(result, git_error_last());
		return result;
	}
}