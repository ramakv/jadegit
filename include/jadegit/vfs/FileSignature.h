#pragma once
#include <string>
#include <chrono>

namespace JadeGit
{
	using namespace std;
	using namespace std::chrono;

	class FileSignature
	{
	public:
		FileSignature() = default;
		FileSignature(FileSignature&&) = default;
		FileSignature(const FileSignature&) = delete;
		FileSignature(string author, system_clock::time_point modified = system_clock::now()) : author(move(author)), modified(modified) {}
		FileSignature(string author, int patch, system_clock::time_point modified = system_clock::now()) : author(move(author)), patch(patch), modified(modified) {}

		const string author;
		const int patch = 0;
		const system_clock::time_point modified;

		bool operator==(const FileSignature& rhs) const = default;

		inline bool empty() const
		{
			return author.empty() && !patch;
		}
	};
}